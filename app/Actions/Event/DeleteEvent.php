<?php

declare(strict_types=1);

namespace App\Actions\Event;

use App\Models\Event;
use Illuminate\Support\Facades\Storage;

class DeleteEvent
{
    /**
     * Delete the given event.
     */
    public function delete(Event $event): void
    {
        $profilePicturePath = 'public/images/events/profile_pictures/';
        $eventProfilePicturePath = $event->profile_picture_path;
        if ($eventProfilePicturePath != null) {
            $filename = basename($eventProfilePicturePath);
            if (Storage::exists($profilePicturePath.$filename)) {
                Storage::delete($profilePicturePath.$filename);

                // Delete the large image as well
                ['filename' => $img_filename, 'extension' => $img_extension] = pathinfo($filename);
                Storage::delete($profilePicturePath.$img_filename.'_large'.'.'.$img_extension);
            }
        }

        $bannerPath = 'public/images/events/banners/';
        $eventBannerPath = $event->banner_path;
        if ($eventBannerPath != null) {
            $filename = basename($event->banner_path);
            if (Storage::exists($bannerPath.$filename)) {
                Storage::delete($bannerPath.$filename);
            }
        }

        $event->delete();
    }
}
