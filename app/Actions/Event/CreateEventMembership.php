<?php

declare(strict_types=1);

namespace App\Actions\Event;

use App\Models\Event;
use App\Models\EventMembership;
use App\Models\EventRole;
use App\Models\User;
use Exception;

class CreateEventMembership
{
    /**
     * Validate and create an event membership.
     */
    public function create(User $user, Event $event, EventRole $event_role): EventMembership
    {
        if ($user->event_memberships()->where([
            ['event_id', $event->id],
            ['event_role_id', $event_role->id],
        ])->first() != null) {
            throw new Exception('The user already has this role in the event', 400);
        }

        return EventMembership::create([
            'event_id' => $event->id,
            'user_id' => $user->id,
            'event_role_id' => $event_role->id,
        ]);
    }
}
