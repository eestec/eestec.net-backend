<?php

declare(strict_types=1);

namespace App\Actions\Event;

use App\Actions\File\Image;
use App\Models\Branch;
use App\Models\Event;
use App\Models\EventType;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CreateNewEvent
{
    /**
     * Validate the given event's information
     */
    public function validate(array $input): array
    {
        return Validator::make($input, [
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique(Event::class),
            ],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'application_deadline' => ['required', 'date'],
            'branch_id' => ['required', 'integer'],
            'max_participants' => ['required', 'integer'],
            'participation_fee' => ['required', 'integer'],
            'description' => ['required', 'string', 'max:3000'],
            'location' => ['required', 'string', 'max:255'],
            'event_type_id' => ['required', 'integer'],
            'profile_picture_path' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'banner_path' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'branch_id' => ['required', 'integer'],
        ])->validateWithBag('createEvent');
    }

    /**
     * Validate and create the given event.
     */
    public function create(array $input): Event
    {
        $validated = $this->validate($input);

        $branch = Branch::firstWhere('id', $validated['branch_id']);
        if ($branch == null) {
            abort(404, 'Specified branch does not exist');
        }

        $event_type = EventType::firstWhere('id', $validated['event_type_id']);
        if ($event_type == null) {
            abort(404, 'Specified event type does not exist');
        }

        $slug = SlugService::createSlug(Event::class, 'slug', $validated['name']);

        $now = time();
        $bannerName = $slug.'-'.$now.'.'.$validated['banner_path']->extension();
        $profilePictureName = $slug.'-'.$now.'.'.$validated['profile_picture_path']->extension();

        $largeProfilePictureName = $slug.'-'.$now.'_large.'.$validated['profile_picture_path']->extension();

        $bannerPath = 'public/images/events/banners/';
        $profilePicturePath = 'public/images/events/profile_pictures/';

        $validated['banner_path']->storeAs($bannerPath, $bannerName);
        $validated['profile_picture_path']->storeAs($profilePicturePath, $largeProfilePictureName);

        // Add the resized images
        Image::resize($profilePicturePath.$largeProfilePictureName, $profilePicturePath.$profilePictureName, 500, 2);

        $event = Event::create([
            'name' => trim($validated['name']),
            'start_date' => $validated['start_date'],
            'end_date' => $validated['end_date'],
            'application_deadline' => $validated['application_deadline'],
            'max_participants' => $validated['max_participants'],
            'participation_fee' => $validated['participation_fee'],
            'location' => $validated['location'],
            'description' => $validated['description'],
            'event_type_id' => $event_type->id,
            'profile_picture_path' => $profilePicturePath.$profilePictureName,
            'banner_path' => $bannerPath.$bannerName,
        ]);

        $event->branches()->attach($branch->id);

        return $event;
    }
}
