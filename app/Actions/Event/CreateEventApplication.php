<?php

declare(strict_types=1);

namespace App\Actions\Event;

use App\Models\Event;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class CreateEventApplication
{
    /**
     * Validate and create an event application.
     */
    public function create(User $user, Event $event, array $input): void
    {
        $validated = Validator::make($input, [
            'motivational_letter' => 'string',
        ]);
        $event->applicants()->attach($user->id, [
            'acceptance' => null,
            'motivational_letter' => $validated['motivational_letter'],
            'created_at' => now(),
        ]);
    }
}
