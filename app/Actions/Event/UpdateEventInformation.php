<?php

declare(strict_types=1);

namespace App\Actions\Event;

use App\Actions\File\Image;
use App\Models\Event;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Storage;

class UpdateEventInformation
{
    /**
     * Validate the given event's information
     */
    public function validate(Event $event, array $input): array
    {
        return Validator::make($input, [
            'name' => [
                'string',
                'max:255',
                Rule::unique(Event::class)->ignore($event->id),
            ],
            'start_date' => ['date'],
            'end_date' => ['date'],
            'application_deadline' => ['date'],
            'max_participants' => ['integer'],
            'participation_fee' => ['integer'],
            'description' => ['string', 'max:3000'],
            'location' => ['string', 'max:255'],
            'profile_picture_path' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'banner_path' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ])->validateWithBag('updateEventInformation');
    }

    /**
     * Validate and update the given branch's information.
     */
    public function update(Event $event, array $input): Event
    {
        $validated = $this->validate($event, $input);

        if (Arr::exists($validated, 'profile_picture_path')) {
            $profilePicturePath = 'public/images/events/profile_pictures/';
            $eventProfilePicturePath = $event->profile_picture_path;
            if ($eventProfilePicturePath != null) {
                $filename = basename($eventProfilePicturePath);
                if (Storage::exists($profilePicturePath.$filename)) {
                    Storage::delete($profilePicturePath.$filename);
                }
            }

            // Consistent timestamps
            $now = time();
            $profilePictureName = $event->slug.'-'.$now.'.'.$validated['profile_picture_path']->extension();
            $largeProfilePictureName = $event->slug.'-'.$now.'.'.$validated['profile_picture_path']->extension();

            $validated['profile_picture_path']->storeAs($profilePicturePath, $largeProfilePictureName);
            Image::resize($profilePicturePath.$largeProfilePictureName, $profilePicturePath.$profilePictureName);
            $event->profile_picture_path = $profilePicturePath.$profilePictureName;
        }

        if (Arr::exists($validated, 'banner_path')) {
            $bannerPath = 'public/images/events/banners/';
            $eventBannerPath = $event->banner_path;
            if ($eventBannerPath != null) {
                $filename = basename($eventBannerPath);
                if (Storage::exists($bannerPath.$filename)) {
                    Storage::delete($bannerPath.$filename);
                }
            }

            $bannerName = $event->slug.'-'.time().'.'.$validated['banner_path']->extension();
            $validated['banner_path']->storeAs($bannerPath, $bannerName);
            $event->banner_path = $bannerPath.$bannerName;
        }

        // Handle name separately so we remove whitespace
        if (Arr::exists($validated, 'name')) {
            $event->fill([
                'name' => trim($validated['name']),
            ]);
        }

        // Implicit assignment is a bad idea in general, but with so many optional fields
        // it's the only clean option. I hope we have enough validation to be clean.
        $fillable_fields = Arr::except($validated, ['profile_picture_path', 'banner_path', 'name']);
        $event->fill($fillable_fields)->save();

        return $event;
    }
}
