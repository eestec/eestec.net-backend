<?php

declare(strict_types=1);

namespace App\Actions\Partner;

use App\Models\Partner;
use Illuminate\Support\Facades\Storage;

class DeletePartner
{
    /**
     * Delete a partner
     */
    public function delete(Partner $partner): void
    {
        $picturePath = 'public/images/partners/';

        // We should probably find a better way to figure
        // the type of the image (or constraint the upload
        // only to PNGs)
        $pictureName = $partner->slug.'.png';
        $picture = $picturePath.$pictureName;
        if (Storage::exists($picture)) {
            Storage::delete($picture);
        }
        $partner->delete();
    }
}
