<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\Filters\Filter;

class CustomStringFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        // Ensure the value is an array with 'type' and 'value'
        if (! is_array($value) || ! isset($value['type'])) {
            return;
        }

        $type = $value['type'];

        if ($type === 'isEmpty') {
            $query->whereNull($property)->orWhere($property, '=', '');

            return;
        }
        if ($type === 'isNotEmpty') {
            $query->whereNotNull($property)->where($property, '<>', '');

            return;
        }

        if (! isset($value['value'])) {
            return;
        }

        $filterValue = $value['value'];

        switch ($type) {
            case 'contains':
                $query->where($property, 'ILIKE', '%'.$filterValue.'%');
                break;

            case 'equals':
                $query->whereRaw("LOWER($property) = ?", [strtolower($filterValue)]);
                break;

            case 'startsWith':
                $query->where($property, 'ILIKE', $filterValue.'%');
                break;

            case 'endsWith':
                $query->where($property, 'ILIKE', '%'.$filterValue);
                break;

            case 'isAnyOf':
                if (! is_array($filterValue)) {
                    $filterValue = [$filterValue];
                }

                if (is_array($filterValue)) {
                    $lowerFilterValues = array_map('strtolower', $filterValue);
                    $query->whereIn(DB::raw("LOWER($property)"), $lowerFilterValues);
                }
                break;

            default:
                // Unknown filter type, do nothing or handle accordingly
                break;
        }
    }
}
