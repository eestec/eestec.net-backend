<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersEventSearch implements Filter
{
    public function __invoke(Builder $query, $value, string $property): void
    {
        $query_val = strtolower($value);
        $query->where(function (Builder $query) use ($query_val) {
            $query
                ->whereHas('branches', function (Builder $query) use ($query_val) {
                    $query
                        ->whereRaw('LOWER(name) LIKE ?', '%'.$query_val.'%')
                        ->orWhere('slug', 'like', '%'.Str::replace(' ', '-', $query_val).'%')
                        ->orWhereRaw('LOWER(country) LIKE ?', '%'.$query_val.'%');
                })
                ->orWhereRaw('LOWER(name) LIKE ?', '%'.$query_val.'%')
                ->orWhere('slug', 'like', '%'.Str::replace(' ', '-', $query_val).'%')
                ->orWhereRaw('LOWER(location) LIKE ?', '%'.$query_val.'%');
        });
    }
}
