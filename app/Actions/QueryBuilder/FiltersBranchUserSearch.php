<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersBranchUserSearch implements Filter
{
    public function __invoke(Builder $query, $value, string $property): void
    {
        $query_val = strtolower($value);
        $query->where(function (Builder $query) use ($query_val) {
            $query
                ->whereRaw("concat(LOWER(users.first_name), ' ', LOWER(users.last_name)) LIKE ?", '%'.$query_val.'%')
                ->orWhere('users.slug', 'like', '%'.Str::replace(' ', '-', $query_val).'%');
        });
    }
}
