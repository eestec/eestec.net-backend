<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class CustomNumericFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        // Ensure the value is an array with 'type' and 'value'
        if (! is_array($value) || ! isset($value['type'])) {
            return;
        }

        $type = $value['type'];

        if ($type === 'isEmpty') {
            $query->whereNull($property);

            return;
        }

        if ($type === 'isNotEmpty') {
            $query->whereNotNull($property);

            return;
        }

        if (! isset($value['value'])) {
            return;
        }
        $filterValue = $value['value'];

        if (! is_array($filterValue)) {
            $filterValue = $filterValue + 0;
        }

        // Handle the comparison operators dynamically
        if (in_array($type, ['=', '!=', '>', '>=', '<', '<='])) {
            $query->where($property, $type, $filterValue);

            return;
        }

        if ($type === 'isAnyOf') {
            if (! is_array($filterValue)) {
                $filterValue = [$filterValue];
            }

            if (is_array($filterValue)) {
                $query->whereIn($property, $filterValue);
            }

            return;
        }
    }
}
