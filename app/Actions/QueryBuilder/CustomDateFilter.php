<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Spatie\QueryBuilder\Filters\Filter;

class CustomDateFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property): void
    {

        if (! is_array($value) || ! isset($value['type'])) {
            return;
        }

        $type = $value['type'];

        if ($type === 'isEmpty') {
            $query->whereNull($property);

            return;
        }
        if ($type === 'isNotEmpty') {
            $query->whereNotNull($property);

            return;
        }

        if (! isset($value['value']) || ! $value['value']) {
            return;
        }
        $dateValue = Carbon::parse($value['value']);

        switch ($type) {
            case 'is':
                $query->whereDate($property, $dateValue->format('Y-m-d'));
                break;

            case 'isNot':
                $query->whereDate($property, '<>', $dateValue->format('Y-m-d'));
                break;

            case 'isAfter':
                $query->whereDate($property, '>', $dateValue->format('Y-m-d'));
                break;

            case 'isOnOrAfter':
                $query->whereDate($property, '>=', $dateValue->format('Y-m-d'));
                break;

            case 'isBefore':
                $query->whereDate($property, '<', $dateValue->format('Y-m-d'));
                break;

            case 'isOnOrBefore':
                $query->whereDate($property, '<=', $dateValue->format('Y-m-d'));
                break;

            default:
                break;
        }
    }
}
