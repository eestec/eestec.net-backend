<?php

declare(strict_types=1);

namespace App\Actions\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class CustomInclusionFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        // Ensure the value is an array with 'type' and 'value'
        if (! is_array($value) || ! isset($value['type'], $value['value'])) {
            return;
        }

        $type = $value['type'];
        $filterValue = $value['value'];

        switch ($type) {
            case 'is':
                $query->where($property, '=', $filterValue);
                break;

            case 'isNot':
                $query->where($property, '!=', $filterValue);
                break;

            case 'isAnyOf':
                if (! is_array($filterValue)) {
                    $filterValue = [$filterValue];
                }

                if (is_array($filterValue)) {
                    $query->whereIn($property, $filterValue);
                }
                break;

            default:
                // Handle unknown types or throw an exception if needed
                break;
        }
    }
}
