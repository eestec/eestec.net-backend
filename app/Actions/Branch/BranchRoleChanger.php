<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Support\Collection;
use InvalidArgumentException;

class BranchRoleChanger
{
    public function one(Branch $branch, User $user, BranchRole $role): void
    {
        $user_membership = BranchMembership::firstWhere([
            ['user_id', '=', $user->id],
            ['branch_id', '=', $branch->id],
        ]);

        if ($user_membership == null) {
            throw new InvalidArgumentException('User does not belong in this branch');
        }

        $user_membership->branch_role_id = $role->id;
        $user_membership->save();
    }

    /**
     * @param  Collection<User>  $users
     */
    public function many(Branch $branch, Collection $users, BranchRole $role): void
    {
        $user_ids = $users->map(fn (User $u) => $u->id);

        $memberships = BranchMembership::where('branch_id', $branch->id)
            ->whereIn('user_id', $user_ids)
            ->get();

        if (count($memberships) !== count($user_ids)) {
            throw new InvalidArgumentException('A user in the list is not a member of this branch');
        }

        BranchMembership::where('branch_id', $branch->id)
            ->whereIn('user_id', $user_ids)
            ->update(['branch_role_id' => $role->id]);
    }
}
