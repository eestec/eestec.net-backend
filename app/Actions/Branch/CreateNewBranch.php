<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Actions\File\Image;
use App\Models\Branch;
use App\Models\BranchType;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CreateNewBranch
{
    /**
     * Validate and create a branch.
     */
    public function create(array $input): Branch
    {
        // It is very important to use the validated array from now on.
        $validated = Validator::make($input, [
            'name' => ['required', 'string', 'max:255', Rule::unique(Branch::class)],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(Branch::class),
            ],
            'founded_in' => ['integer'],
            'website' => ['string', 'max:255'],
            'profile_picture_path' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'description' => ['string', 'max:3000'],
            'country' => ['required', 'string', 'max:255'],
            'region' => ['required', 'integer', 'between:1,5'],
            'facebook' => ['url', 'max:255'],
            'instagram' => ['url', 'max:255'],
            'linkedin' => ['url', 'max:255'],
            'address' => ['string', 'max:255'],
            'branch_type_id' => ['required', 'integer', Rule::exists('branch_types', 'id')],
        ])->validate();

        $other_fields = Arr::except($validated, ['name', 'email', 'country', 'region', 'branch_type_id', 'profile_picture_path']);

        // Image stuff
        if (Arr::exists($validated, 'profile_picture_path')) {
            $profilePicturePath = 'public/images/branches/profile_pictures/';
            $slug = SlugService::createSlug(Branch::class, 'slug', $validated['name']);

            $now = time();
            $profilePictureName = $slug.'-'.$now.'.'.$validated['profile_picture_path']->extension();
            $largeProfilePictureName = $slug.'-'.$now.'_large.'.$validated['profile_picture_path']->extension();

            $validated['profile_picture_path']->storeAs($profilePicturePath, $profilePictureName);

            // Add the resized images
            Image::resize($profilePicturePath.$profilePictureName, $profilePicturePath.$largeProfilePictureName, 2000, 2);
            Image::resize($profilePicturePath.$profilePictureName, $profilePicturePath.$profilePictureName, 500, 2);

            // Add the profile_picture_path to the fields we will spread fill
            $other_fields = Arr::add($other_fields, 'profile_picture_path', $profilePicturePath.$profilePictureName);
            $other_fields = Arr::add($other_fields, 'large_profile_picture_path', $profilePicturePath.$largeProfilePictureName);
        }

        // Implicit assignment is a bad idea in general, but with so many optional fields
        // it's the only clean option. I hope we have enough validation to be clean.
        $branch = Branch::create([
            ...$other_fields,
            'name' => $validated['name'],
            'email' => $validated['email'],
            'country' => $validated['country'],
            'region' => $validated['region'],
        ]);
        $type = BranchType::find($validated['branch_type_id']);

        $branch->branch_type()->associate($type);
        $branch->save();

        return $branch;
    }
}
