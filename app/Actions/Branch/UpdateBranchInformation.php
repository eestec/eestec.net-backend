<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Actions\File\Image;
use App\Models\Branch;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use InvalidArgumentException;

class UpdateBranchInformation
{
    /**
     * Validate the given branch's information
     */
    public function validate(Branch $branch, array $input): array
    {
        return Validator::make($input, [
            'name' => ['string', 'max:255'],
            'email' => [
                'string',
                'email',
                'max:255',
                Rule::unique(Branch::class)->ignore($branch->id),
            ],
            'description' => ['string', 'max:3000'],
            'founded_in' => ['integer'],
            'website' => ['string', 'nullable', 'max:255'],
            'profile_picture_path' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'country' => ['string', 'max:255'],
            'region' => ['string', 'max:255'],
            'facebook' => ['url', 'nullable', 'max:255'],
            'instagram' => ['url', 'nullable', 'max:255'],
            'linkedin' => ['url', 'nullable', 'max:255'],
            'address' => ['string', 'nullable', 'max:255'],
        ])->validateWithBag('updateBranchInformation');
    }

    /**
     * Validate and update the given branch's information.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function update($branch, array $input)
    {
        $validated = $this->validate($branch, $input);

        // Check if the user has the 'Administrator' role for certain fields
        $restrictedFields = ['region', 'country', 'name'];

        foreach ($restrictedFields as $field) {
            if (Arr::exists($validated, $field) && ! Auth::user()->hasRole('Administrator')) {
                throw new InvalidArgumentException('You are not authorized to edit '.$field);
            }
        }

        if (Arr::exists($validated, 'profile_picture_path')) {
            $now = time();
            $profilePicturePath = 'public/images/branches/profile_pictures/';

            if ($branch->profile_picture_path != null) {
                $filename = basename($branch->profile_picture_path);

                $exists = ! empty($branch->profile_picture_path)
                    && Storage::exists($profilePicturePath.$filename);

                if ($exists) {
                    Storage::delete($profilePicturePath.$filename);

                    // We need to create the right path to delete the large image
                    ['filename' => $img_filename, 'extension' => $img_extension] = pathinfo($filename);
                    Storage::delete($profilePicturePath.$img_filename.'_large.'.$img_extension);
                }
            }

            $profilePictureName = $branch->slug.'-'.$now.'.'.$validated['profile_picture_path']->extension();
            $largeProfilePictureName = $branch->slug.'-'.$now.'_large.'.$validated['profile_picture_path']->extension();

            $validated['profile_picture_path']->storeAs($profilePicturePath, $profilePictureName);

            // Add the resized images
            Image::resize($profilePicturePath.$profilePictureName, $profilePicturePath.$largeProfilePictureName, 2000, 2);
            Image::resize($profilePicturePath.$profilePictureName, $profilePicturePath.$profilePictureName, 500, 2);

            $branch->profile_picture_path = $profilePicturePath.$profilePictureName;
            $branch->large_profile_picture_path = $profilePicturePath.$largeProfilePictureName;
        }

        // Implicit assignment is a bad idea in general, but with so many optional fields
        // it's the only clean option. I hope we have enough validation to be clean.
        $fillable_fields = Arr::except($validated, ['profile_picture_path']);
        $branch->fill($fillable_fields)->save();
    }
}
