<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Mail\BranchApplicationApproval;
use App\Models\Branch;
use App\Models\BranchApplication;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class BranchAcceptance
{
    /**
     * Validate and update the given branch application's status.
     */
    public function one(BranchApplication $application, User $user, Branch $branch, array $input): void
    {
        $validated = Validator::make($input, [
            'acceptance' => ['required', 'bool'],
        ])->validate();

        $application->fill([
            'acceptance' => $validated['acceptance'],
        ])->save();

        if ($application->acceptance) {
            BranchMembership::create([
                'user_id' => $user->id,
                'branch_id' => $branch->id,
                'branch_role_id' => BranchRole::memberId(),
            ]);
            $user->branch_verified_at = now();
            $user->save();
        }

        $acceptance = $application->acceptance;
        $application->delete();

        Mail::to($user->email)->queue(new BranchApplicationApproval($user, $branch, $acceptance > 0));
    }

    /**
     * @param  Collection<BranchApplication>  $applications
     */
    public function many(Collection $applications, Branch $branch, bool $acceptance): void
    {
        $users = User::whereIn('id', $applications
            ->map(fn (BranchApplication $app) => $app->user_id))
            ->get();

        if (count($applications) !== count($users)) {
            throw new InvalidArgumentException('A user mentioned in the application list does not exist');
        }

        foreach ($users as $user) {
            if ($user->belongsToBranch($branch->id)) {
                throw new InvalidArgumentException('User '.$user->slug.'is already a member of that branch.');
            }
        }

        $application_ids = $applications->map(fn (BranchApplication $app) => $app->id);
        BranchApplication::whereIn('id', $application_ids)->update(['acceptance' => $acceptance]);

        if ($acceptance) {
            $member_role = BranchRole::memberId();
            $memberships = [];
            for ($i = 0; $i < count($applications); $i++) {
                $memberships[] = [
                    'user_id' => $users->get($i)->id,
                    'branch_id' => $branch->id,
                    'branch_role_id' => $member_role,
                ];
            }
            BranchMembership::insert($memberships);
        }
        BranchApplication::destroy($application_ids);
        foreach ($users as $user) {
            Mail::to($user->email)->queue(new BranchApplicationApproval($user, $branch, $acceptance));
        }
    }
}
