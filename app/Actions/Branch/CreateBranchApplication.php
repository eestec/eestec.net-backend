<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Events\BranchApplicationEvent;
use App\Models\Branch;
use App\Models\User;

class CreateBranchApplication
{
    /**
     * Creates the given branch application.
     */
    public function create(Branch $branch, User $user): void
    {
        $user->branch_applications()->attach($branch->id, [
            'acceptance' => null,
        ]);

        // Dispatch a branch application event.
        BranchApplicationEvent::dispatch($branch, $user);
    }
}
