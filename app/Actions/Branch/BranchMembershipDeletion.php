<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\User;
use Illuminate\Support\Collection;
use InvalidArgumentException;

class BranchMembershipDeletion
{
    public function one(Branch $branch, User $user): void
    {
        $membership = BranchMembership::firstWhere([
            ['branch_id', '=', $branch->id],
            ['user_id', '=', $user->id],
        ]);
        if ($membership == null) {
            throw new InvalidArgumentException('The user is not a member of this branch');
        }
        $membership->delete();
    }

    /**
     * @param  Collection<User>  $users
     */
    public function many(Branch $branch, Collection $users): void
    {
        $memberships = [];
        foreach ($users as $user) {
            $membership = BranchMembership::firstWhere([
                ['branch_id', '=', $branch->id],
                ['user_id', '=', $user->id],
            ]);
            if ($membership == null) {
                throw new InvalidArgumentException('The user '.$user->slug.' with id '.$user->id.' is not a member of this branch');
            }

            $memberships[] = $membership;
        }
        BranchMembership::destroy(collect($memberships)->map(fn (BranchMembership $m) => $m->id));
    }
}
