<?php

declare(strict_types=1);

namespace App\Actions\Branch;

use App\Models\Branch;
use App\Models\BranchType;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UpdateBranchType
{
    /**
     * Validate the given branch's information
     */
    public function validate(Branch $branch, array $input): array
    {
        return Validator::make($input, [
            'branch_type' => ['string', Rule::exists('branch_types', 'name')],
        ])->validateWithBag('updateBranchInformation');
    }

    /**
     * Validate and update the given branch's information.
     */
    public function update(Branch $branch, array $input): void
    {
        $validated = $this->validate($branch, $input);

        $type_id = BranchType::firstWhere('name', $validated['branch_type'])->id;
        $branch->forceFill(['branch_type_id' => $type_id])->save();
    }
}
