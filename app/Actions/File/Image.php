<?php

declare(strict_types=1);

namespace App\Actions\File;

use Illuminate\Support\Facades\Storage;
use Imagick;
use InvalidArgumentException;

class Image
{
    /**
     * Save image to file location.
     */
    private static function saveImage(Imagick $image, string $path): bool
    {
        $format = $image->getImageFormat();

        if ($format == 'GIF') {
            $newImage = $image->deconstructImages();

            return $newImage->writeImages($path, true);
        }

        return $image->writeImage($path);
    }

    /**
     * Resizes image, while keeping original aspect ratio. Supports selecting the target dimension for which
     * the size is set or resizing based on the largest one. The images must be located in the default storage
     * location of the laravel application, because Imagick requires full paths.
     */
    public static function resize(string $original_filename, string $target_filename, int $dim_size = 300, int $target_dimension = 0): bool
    {
        $original_path = Storage::path($original_filename);
        $target_path = Storage::path($target_filename);

        $image = new Imagick($original_path);

        $format = $image->getImageFormat();
        ['width' => $width, 'height' => $height] = $image->getImageGeometry();

        if ($format == 'GIF') {
            $image = $image->coalesceImages();
        }

        do {
            if ($target_dimension === 0 && $width < $height) {
                $image->resizeImage($dim_size, 0, Imagick::FILTER_CATROM, 1);
            } elseif ($target_dimension === 0) {
                $image->resizeImage(0, $dim_size, Imagick::FILTER_CATROM, 1);
            } elseif ($target_dimension === 1) {
                $image->resizeImage(0, $dim_size, Imagick::FILTER_CATROM, 1);
            } elseif ($target_dimension === 2) {
                $image->resizeImage($dim_size, 0, Imagick::FILTER_CATROM, 1);
            } else {
                throw new InvalidArgumentException('Target dimension must have a value between 0 and 2');
            }
        } while ($image->nextImage());

        return self::saveImage($image, $target_path);
    }

    /**
     * Converts an image from one type to another.
     */
    public static function convert(string $original_filename, string $target_filename): bool
    {
        $original_path = Storage::path($original_filename);
        $target_path = Storage::path($target_filename);

        $image = new Imagick($original_path);

        return self::saveImage($image, $target_path);
    }

    public static function url(?string $path): ?string
    {
        return $path === null ? null : Storage::url($path);
    }
}
