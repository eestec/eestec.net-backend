<?php

declare(strict_types=1);

namespace App\Actions\File;

class CSV
{
    public static function process(string $filename, callable $process): void
    {
        if ($csv = fopen($filename, 'r')) {
            while (($data = fgetcsv($csv))) {
                $process($data);
            }
        }
        fclose($csv);
    }
}
