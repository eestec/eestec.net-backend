<?php

declare(strict_types=1);

namespace App\Actions\Fortify;

use App\Actions\File\Image;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate the given user's profile information
     */
    public function validate(User $user, array $input): array
    {
        return Validator::make($input, [
            'first_name' => ['string', 'max:255'],
            'last_name' => ['string', 'max:255'],
            'email' => [
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'description' => ['string', 'nullable', 'max:3000'],
            'profile_photo_path' => [
                'image',
                'mimes:jpeg,png,jpg,gif,svg',
                'max:2048',
            ],
            'banner_path' => [
                'image',
                'mimes:jpeg,png,jpg,gif,svg',
                'max:2048',
            ],
        ])->validateWithBag('updateProfileInformation');
    }

    /**
     * Validate and update the given user's profile information.
     */
    public function update(User $user, array $input): void
    {
        // It is super important to use the validated array from here on.
        $validated = $this->validate($user, $input);

        if (
            Arr::exists($validated, 'email') &&
            $input['email'] !== $user->email &&
            $user instanceof MustVerifyEmail
        ) {
            $this->updateVerifiedUser($user, $validated);
        } else {
            $this->updateCommonUserParts($user, $validated);
            $user->save();
        }
    }

    /**
     * Update the common parts that are the same regardless of user state
     * The input is expected to be validated, so only valid fields remain.
     *
     * @param  mixed  $user
     * @return void
     */
    protected function updateCommonUserParts($user, array $input)
    {
        $fillable_fields = Arr::except($input, ['profile_photo_path', 'banner_path']);
        $user->fill($fillable_fields);

        if (Arr::exists($input, 'profile_photo_path')) {
            $profilePicturePath = 'public/images/users/profile_photos/';
            if ($user->profile_photo_path != null) {
                $filename = basename($user->profile_photo_path);

                $exists = ! empty($user->profile_photo_path)
                    && Storage::exists($profilePicturePath.$filename);

                if ($exists) {
                    Storage::delete($profilePicturePath.$filename);

                    // Delete the large image as well
                    ['filename' => $img_filename, 'extension' => $img_extension] = pathinfo($filename);
                    Storage::delete($profilePicturePath.$img_filename.'_large'.'.'.$img_extension);
                }
            }

            // Make sure all names have the same timestamp.
            $now = time();
            $profilePictureName = $user->slug.'-'.$now.'.'.$input['profile_photo_path']->extension();
            $largeProfilePictureName = $user->slug.'-'.$now.'_large'.'.'.$input['profile_photo_path']->extension();
            $input['profile_photo_path']->storeAs($profilePicturePath, $largeProfilePictureName);

            Image::resize($profilePicturePath.$largeProfilePictureName, $profilePicturePath.$profilePictureName);
            $user->profile_photo_path = $profilePicturePath.$profilePictureName;
        }

        if (Arr::exists($input, 'banner_path')) {
            $bannerPath = 'public/images/users/banners/';

            if ($user->banner_path != null) {
                $filename = basename($user->banner_path);

                $exists = ! empty($user->banner_path)
                    && Storage::exists($bannerPath.$filename);

                if ($exists) {
                    Storage::delete($bannerPath.$filename);
                }
            }

            $bannerName = $user->slug.'-'.time().'.'.$input['banner_path']->extension();
            $input['banner_path']->storeAs($bannerPath, $bannerName);
            $user->banner_path = $bannerPath.$bannerName;
        }
        $user->save();
    }

    /**
     * Update the given verified user's profile information.
     * Used when the user changes their email.
     *
     * @param  mixed  $user
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $this->updateCommonUserParts($user, $input);
        $user->forceFill([
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
