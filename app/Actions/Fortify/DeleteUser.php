<?php

declare(strict_types=1);

namespace App\Actions\Fortify;

use App\Contracts\Actions\DeletesUser;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class DeleteUser implements DeletesUser
{
    /**
     * Delete the given user.
     */
    public function delete(User $user): void
    {
        $profilePicturePath = 'public/images/users/profile_photos/';
        if (! empty($user->profile_photo_path)) {
            $filename = basename($user->profile_photo_path);

            $exists = Storage::exists($profilePicturePath.$filename);

            if ($exists) {
                Storage::delete($profilePicturePath.$filename);

                // Delete the large image as well
                ['filename' => $img_filename, 'extension' => $img_extension] = pathinfo($filename);
                Storage::delete($profilePicturePath.$img_filename.'_large'.'.'.$img_extension);
            }
        }
        $bannerPath = 'public/images/users/banners/';
        if (! empty($user->banner_path)) {
            $filename = basename($user->banner_path);

            $exists = Storage::exists($bannerPath.$filename);

            if ($exists) {
                Storage::delete($bannerPath.$filename);
            }
        }
        foreach ($user->tokens as $token) {
            $token->delete();
        }
        $user->delete();
    }
}
