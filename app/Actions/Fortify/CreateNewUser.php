<?php

declare(strict_types=1);

namespace App\Actions\Fortify;

use App\Actions\Branch\CreateBranchApplication;
use App\Models\Branch;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     */
    public function create(array $input): User
    {
        $validated = Validator::make($input, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
            'terms' => ['required', 'accepted'],
            'branch' => ['required', Rule::exists(Branch::class, 'name')],
        ])->validate();

        $branch = Branch::firstWhere('name', $validated['branch']);

        if ($branch == null) {
            abort(404, 'Specified branch does not exist');
        }

        $user = User::create([
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'email' => $validated['email'],
            'password' => Hash::make($validated['password']),
            'role_id' => Role::where('name', 'User')->first()->id,
        ]);

        $user->forceFill([
            'last_password_update' => now(),
        ])->save();

        $creator = new CreateBranchApplication;
        $creator->create($branch, $user);

        return $user;
    }
}
