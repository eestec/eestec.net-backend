<?php

declare(strict_types=1);

namespace App\Actions\BoardPosition;

use App\Models\BoardPosition;
use Illuminate\Support\Facades\Validator;

class UpdateBoardPosition
{
    /**
     * Validate the given board member's information
     */
    public function validate(array $input): array
    {
        return Validator::make($input, [
            'description' => ['string'],
            'name' => ['string'],
        ])->validateWithBag('updateBoardPosition');
    }

    /**
     * Validate and update the given board member's information.
     */
    public function update(BoardPosition $position, array $input): BoardPosition
    {
        $validated = $this->validate($input);

        $position->fill($validated)->save();

        return $position;
    }
}
