<?php

declare(strict_types=1);

namespace App\Actions\BoardMember;

use App\Models\BoardMember;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UpdateBoardMembership
{
    /**
     * Validate the given board member's information
     */
    public function validate(array $input): array
    {
        return Validator::make($input, [
            'mandate_start' => ['date'],
            'mandate_end' => ['date'],
            'user_id' => [Rule::exists('users', 'id')],
            'board_position_id' => [Rule::exists('board_positions', 'id')],
            'order' => ['integer'],
        ])->validateWithBag('updateBoardMember');
    }

    /**
     * Validate and update the given board member's information.
     */
    public function update(BoardMember $membership, array $input): BoardMember
    {
        $validated = $this->validate($input);

        $membership->fill($validated)->save();

        return $membership;
    }
}
