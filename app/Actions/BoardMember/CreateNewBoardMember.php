<?php

declare(strict_types=1);

namespace App\Actions\BoardMember;

use App\Models\BoardMember;
use App\Models\BoardPosition;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use InvalidArgumentException;

class CreateNewBoardMember
{
    /**
     * Validate the given event's information
     */
    public function validate(array $input): array
    {
        return Validator::make($input, [
            'mandate_start' => ['date'],
            'mandate_end' => ['date'],
            'mandate' => ['string'],
            'user_id' => ['required', Rule::exists('users', 'id')],
            'board_position_id' => ['required', Rule::exists('board_positions', 'id')],
            'order' => ['required', 'integer'],
        ])->validateWithBag('createBoardMember');
    }

    /**
     * Validate and create the given event.
     */
    public function create(array $input): BoardMember
    {
        $validated = $this->validate($input);

        if ((! Arr::exists($validated, 'mandate_start') && Arr::exists($validated, 'mandate_end')) ||
        Arr::exists($validated, 'mandate_start') && ! Arr::exists($validated, 'mandate_end')) {
            throw new InvalidArgumentException('Both a mandate_start and a mandate_end need to be set!');
        }

        if (! Arr::exists($validated, 'mandate_start') && ! Arr::exists($validated, 'mandate')) {
            throw new InvalidArgumentException('There needs to be a mandate or mandate duration specified.');
        }

        if (Arr::exists($validated, 'mandate') && ! Arr::exists($validated, 'mandate_start')) {
            $year = (int) explode('/', $validated['mandate'])[0];

            $validated['mandate_start'] = Carbon::createFromDate($year, 8, 1);

            $validated['mandate_end'] = Carbon::createFromDate($year + 1, 7, 31);
        }

        if (Arr::exists($validated, 'mandate_start') && ! Arr::exists($validated, 'mandate')) {
            $date = new DateTime($validated['mandate_start']);
            $year = $date->format('Y');
            $validated['mandate'] = $year.'/'.(substr($year + 1, -2));
        }

        $user = User::find($validated['user_id']);
        $board_position = BoardPosition::find($validated['board_position_id']);

        $membership = new BoardMember;
        $membership->mandate_start = $validated['mandate_start'];
        $membership->mandate_end = $validated['mandate_end'];
        $membership->mandate = $validated['mandate'];
        $membership->order = $validated['order'];

        $membership->user_id = $user->id;
        $membership->board_position_id = $board_position->id;
        $membership->save();

        // Make the user a super-admin
        $role = Role::firstWhere('name', 'Administrator');
        $user->role()->associate($role);
        $user->save();

        return $membership;
    }
}
