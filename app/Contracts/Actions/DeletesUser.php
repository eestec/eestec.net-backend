<?php

declare(strict_types=1);

namespace App\Contracts\Actions;

use App\Models\User;

interface DeletesUser
{
    public function delete(User $user);
}
