<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AlwaysAcceptJsonResponses
{
    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (\Illuminate\Http\Response|RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $next(tap($request, function ($request) {
            $request->headers->set('Accept', 'application/json');
        }));
    }
}
