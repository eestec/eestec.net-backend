<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Response;

class EnsureBranchIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  Closure(Request): (JsonResponse|RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next, ?string $redirectToRoute = null): JsonResponse|RedirectResponse
    {
        if (! $request->user() || ! $request->user()->hasBranchVerified()) {
            return $request->expectsJson()
                ? Response::json(['message' => 'You are currently not a member of any branch.'], 403)
                : Redirect::guest(URL::route($redirectToRoute ?: 'verification.notice'));
        }

        return $next($request);
    }
}
