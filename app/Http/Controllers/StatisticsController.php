<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class StatisticsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): JsonResponse
    {
        $cities = Cache::get('branches');
        $users = Cache::get('users');
        $countries = Cache::get('countries');

        return response()->json([
            'countries' => $countries,
            'universities' => $cities,
            'users' => $users,
        ]);
    }
}
