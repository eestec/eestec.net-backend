<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(RoleResource::collection(Role::get()));
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role): JsonResponse
    {
        return response()->json(new RoleResource($role));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role): JsonResponse
    {
        if (Auth::user()->hasRole('Administrator')) {
            if ($role->name == 'Administrator' || $role->name == 'User') {
                return response()->json('Administrator and User roles cannot be deleted', 403);
            }
            $role->delete();

            return response()->json();
        }

        return response()->json('Unauthorized', 403);
    }

    /**
     * Promote an account to administrator of the website
     */
    public function changeUserRole(Request $request, User $user): JsonResponse
    {
        if (Auth::user()->hasRole('Administrator')) {
            $validated = $request->validate([
                'role_name' => ['required', Rule::exists('roles', 'name')],
            ]);
            $role = Role::firstWhere('name', $validated['role_name']);
            $user->role()->associate($role);
            $user->save();

            return response()->json(new UserResource($user));
        }

        return response()->json('Unauthorized', 403);
    }
}
