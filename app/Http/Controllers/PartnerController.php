<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Partner\DeletePartner;
use App\Http\Resources\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(PartnerResource::collection(Partner::get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Partner $partner, DeletePartner $deleter): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator')) {
            return response()->json(['error' => 'You do not have access to delete partners'], 403);
        }

        $deleter->delete($partner);

        return response()->json(null, 204);
    }
}
