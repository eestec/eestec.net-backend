<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    /**
     * Create a mail for the specific .
     */
    public function __invoke(Request $request, Contact $contact): void
    {
        $validated = $request->validate([
            'content' => ['required', 'string'],
            'sender' => ['required', 'string', 'email', 'max:255'],
        ]);

        Mail::to($contact->to)
            ->cc($contact->cc)
            ->queue(new ContactMail($contact, $validated['sender'], $validated['content']));
    }
}
