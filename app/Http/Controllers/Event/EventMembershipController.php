<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Actions\Event\CreateEventMembership;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventMembershipResource;
use App\Models\BranchRole;
use App\Models\Event;
use App\Models\EventMembership;
use App\Models\EventRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Throwable;

class EventMembershipController extends Controller
{
    /**
     * Delete eventMembership of an event
     */
    public function destroy(Event $event, User $user): JsonResponse
    {
        $branches = $event->branches->toArray();
        $boardieBranch = Auth::user()->branch_memberships()->firstWhere('branch_role_id', 1);
        $isUserBranchInOrganizingBranches = in_array($boardieBranch, $branches);

        if (! Auth::user()->hasEventRole($event->id, 1) && ! Auth::user()->hasRole('Administrator') && is_null($boardieBranch) && ! $isUserBranchInOrganizingBranches) {
            abort(403, 'You are not authorized to delete this user as an event member.');
        }
        $eventMembership = EventMembership::firstWhere([
            ['user_id', $user->id],
            ['event_id', $event->id],
        ]);

        if ($eventMembership == null) {
            abort(404, 'There is no user associated with this event');
        }

        $eventMembership->delete();

        return response()->json(null, 204);
    }

    /**
     * Create eventMembership for an event
     */
    public function addUser(Event $event, User $user, Request $request, CreateEventMembership $creator): JsonResponse
    {
        $branches = $event->branches->toArray();
        $boardieBranch = Auth::user()->branch_memberships()->firstWhere('branch_role_id', BranchRole::boardId());
        $isUserBranchInOrganizingBranches = in_array($boardieBranch, $branches);

        if (
            ! Auth::user()->hasEventRole($event->id, EventRole::organizerId()) &&
            ! Auth::user()->hasRole('Administrator') &&
            is_null($boardieBranch) &&
            ! $isUserBranchInOrganizingBranches
        ) {
            return response()->json('You are not authorized to add users to this event.', 403);
        }

        $validated = $request->validate([
            'event_role_id' => ['integer', Rule::exists('event_roles', 'id')],
        ]);

        $event_role = EventRole::find($validated['event_role_id']);

        try {
            $eventMembership = $creator->create($user, $event, $event_role);
        } catch (Throwable $e) {
            return response()->json($e->getMessage(), $e->getCode());
        }

        return response()->json(new EventMembershipResource($eventMembership), 201);
    }
}
