<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Actions\Event\CreateEventApplication;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventApplication;
use App\Models\EventMembership;
use App\Models\EventRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EventApplicationController extends Controller
{
    /**
     * Return all event applications for a given event
     */
    public function index(Event $event): JsonResponse
    {
        if (
            ! Auth::user()->hasRole('Administrator') &&
            ! Auth::user()->hasEventRole($event->id, EventRole::organizerId())
        ) {
            abort(403, 'You are not authorized to view event applications for this event.');
        }

        $eventApplications = EventApplication::where('event_id', $event->id)->with('user')->paginate(12);

        return response()->json($eventApplications);
    }

    /**
     * Return an event application by Id
     */
    public function show(Event $event, EventApplication $eventApplication): JsonResponse
    {
        if (
            ! Auth::user()->hasRole('Administrator') &&
            ! Auth::user()->hasEventRole($event->id, EventRole::organizerId()) &&
            Auth::id() != $eventApplication->user_id
        ) {
            abort(403, 'You are not authorized to view this event application.');
        }

        return response()->json($eventApplication);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Event $event, Request $request, CreateEventApplication $creator): JsonResponse
    {
        $user = Auth::user();
        $creator->create($user, $event, $request->all());

        return response()->json(['success' => true], 201);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, EventApplication $eventApplication): JsonResponse
    {
        if (Auth::id() != $eventApplication->user_id) {
            response()->json('You are not authorized to edit this event application.', 403);
        }
        $request->validate([
            'motivational_letter' => 'string',
        ]);
        $eventApplication->motivational_letter = $request->motivational_letter;
        $eventApplication->save();

        return response()->json($eventApplication);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Event $event, EventApplication $eventApplication): JsonResponse
    {
        if (
            ! Auth::user()->hasRole('Administrator') &&
            Auth::id() != $eventApplication->user_id
        ) {
            abort(403, 'You are not authorized to delete this event application.');
        }
        $eventApplication->delete();

        return response()->json(null, 204);
    }

    /**
     * Create an event application for the user specified in the request
     */
    public function addEventApplication(Request $request, Event $event, CreateEventApplication $creator): JsonResponse
    {
        $validated = $request->validate([
            'user_id' => ['integer', Rule::exists('users', 'id')],
        ]);

        $user = User::find($validated['user_id']);

        if (! Auth::user()->hasRole('Administrator') || Auth::user() != $user) {
            return response()->json('You cannot create an event application for another user', 403);
        }
        $creator->create($user, $event, $request->all());

        return response()->json(['success' => true], 201);
    }

    /**
     * Change accepted status of an event application by request
     */
    public function approval(Request $request, EventApplication $eventApplication): JsonResponse
    {
        if (! Auth::user()->hasEventRole($eventApplication->event_id, 1)) {
            abort(403, 'You are not authorized to approve this event applications.');
        }

        $request->validate([
            'acceptance' => 'required|string|boolean',
        ]);
        $isAccepted = $request->acceptance;

        if ($isAccepted) {
            $eventMembership = new EventMembership;
            $eventMembership->event_id = $eventApplication->event_id;
            $eventMembership->user_id = $eventApplication->user_id;
            $eventMembership->event_role_id = 3;
            $eventMembership->save();
        }

        $eventApplication->acceptance = $request->acceptance;
        $eventApplication->save();

        return response()->json($eventApplication, 200);
    }
}
