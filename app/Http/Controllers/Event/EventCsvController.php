<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Actions\Event\CreateEventMembership;
use App\Actions\File\CSV;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventRole;
use App\Models\User;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\File;
use Throwable;

class EventCsvController extends Controller
{
    /**
     * Upload a CSV file with the participants of the event
     */
    public function upload(Event $event, Request $request, CreateEventMembership $creator): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator')) {
            return response()->json('You are not authorized to upload participants\' lists', 401);
        }

        $validated = $request->validate([
            'file' => [
                'required',
                File::types(['csv']),
            ],
        ]);

        $role = EventRole::firstWhere('name', 'Participant');
        $info = [];
        CSV::process((string) $validated['file'], function ($data) use ($creator, $event, $role, &$info) {
            $slug = SlugService::createSlug(User::class, 'slug', $data[0], ['unique' => false]);
            $user = User::firstWhere('slug', $slug);

            if ($user == null) {
                $info = Arr::add($info, $data[0], 'User not found. Slug: '.$slug);

                return;
            }

            try {
                $creator->create($user, $event, $role);
            } catch (Throwable $e) {
                $info = Arr::add($info, $data[0], $e->getMessage());

                return;
            }

            $info = Arr::add($info, $data[0], 'User added to event');
        });

        return response()->json($info);
    }
}
