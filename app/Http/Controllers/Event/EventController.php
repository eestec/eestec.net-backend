<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Actions\Event\CreateNewEvent;
use App\Actions\Event\DeleteEvent;
use App\Actions\Event\UpdateEventInformation;
use App\Actions\QueryBuilder\AlphanumericNameSort;
use App\Actions\QueryBuilder\FiltersEventSearch;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Http\Resources\EventResource;
use App\Http\Resources\UserResource;
use App\Models\BranchRole;
use App\Models\Event;
use App\Models\EventRole;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): EventCollection
    {
        $event_kind = $request->query('kind');

        $events = QueryBuilder::for(Event::class)
            ->defaultSort('-start_date')
            ->allowedSorts([
                AllowedSort::custom('name', new AlphanumericNameSort, 'name'),
                'start_date',
            ])
            ->allowedFilters([
                AllowedFilter::exact('event_type_id'),
                AllowedFilter::custom('search', new FiltersEventSearch),
            ])
            ->with(['event_type', 'branches', 'entities']);

        switch ($event_kind) {
            case 'active':
                $events = $events->whereDate('end_date', '>=', date('Y-m-d'));
                break;
            case 'past':
                $events = $events->whereDate('end_date', '<', date('Y-m-d'));
                break;
            default:
                break;
        }

        return new EventCollection($events->paginate(12));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, CreateNewEvent $creator): JsonResponse
    {
        $creator->validate($request->all());

        $branchMembership = Auth::user()->branch_memberships()->firstWhere('branch_id', $request->branch_id);

        if (! Auth::user()->hasRole('Administrator') && is_null($branchMembership)) {
            abort(403, 'You are not authorized to create events for this branch.');
        }

        $event = $creator->create($request->all());

        return response()->json(new EventResource($event), 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Event $event): JsonResponse
    {
        $can_edit = false;

        if (Auth::user() != null) {
            // Get the branches that are organizing the event
            $branch_ids = $event->branches->map(function ($branch) {
                return $branch->id;
            });

            $userBranchInOrganizingBranches =
                Auth::user()->branch_memberships()->whereIn('branch_id', $branch_ids)->where('branch_role_id', BranchRole::boardId())->first();

            $userInEventOrganizers =
                Auth::user()->hasEventRole($event->id, EventRole::organizerId());

            $can_edit = Auth::user()->hasRole('Administrator') ||
                ! is_null($userBranchInOrganizingBranches) ||
                $userInEventOrganizers;
        }
        $response = [
            'can_edit' => $can_edit,
            'event' => new EventResource($event->with('branches', 'entities')->find($event->id)),
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Event $event, UpdateEventInformation $updater): EventResource
    {
        // Get the branches that are organizing the event
        $branch_ids = $event->branches->map(function ($branch) {
            return $branch->id;
        });

        $userBranchInOrganizingBranches =
            Auth::user()->branch_memberships()->whereIn('branch_id', $branch_ids)->where('branch_role_id', BranchRole::boardId())->first();

        $userInEventOrganizers =
            Auth::user()->hasEventRole($event->id, EventRole::organizerId());

        if (
            ! Auth::user()->hasRole('Administrator') &&
            is_null($userBranchInOrganizingBranches &&
                ! $userInEventOrganizers)
        ) {
            abort(403, 'You are not authorized to edit this event.');
        }

        $event = $updater->update($event, $request->all());

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Event $event, DeleteEvent $remover): JsonResponse
    {
        // Get the branches that are organizing the event
        $branch_ids = $event->branches->map(function ($branch) {
            return $branch->id;
        });

        $userBranchInOrganizingBranches =
            Auth::user()->branch_memberships()->whereIn('branch_id', $branch_ids)->where('branch_role_id', BranchRole::boardId())->first();

        if (! Auth::user()->hasRole('Administrator') && is_null($userBranchInOrganizingBranches)) {
            abort(403, 'You are not authorized to delete this event.');
        }

        $remover->delete($event);

        return response()->json(null, 204);
    }

    /**
     * Return all Event participants
     */
    public function participants(Event $event): JsonResource
    {
        $participants = $event
            ->participants()
            ->orderBy('slug', 'ASC')
            ->paginate(12);

        return UserResource::collection($participants);
    }

    /**
     * Return all Event organizers
     */
    public function organizers(Event $event): JsonResource
    {
        $organizers = $event
            ->organizers()
            ->orderBy('slug', 'ASC')
            ->paginate(12);

        return UserResource::collection($organizers);
    }

    /**
     * Return all Event helpers
     */
    public function helpers(Event $event): JsonResource
    {
        $helpers = $event
            ->helpers()
            ->orderBy('slug', 'ASC')
            ->paginate(12);

        return UserResource::collection($helpers);
    }
}
