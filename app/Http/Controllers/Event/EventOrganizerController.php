<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventOrganizerController extends Controller
{
    /**
     * Add branch as eventOrganizer for the event
     */
    public function addOrganizingBranch(Request $request, Event $event): JsonResponse
    {
        $request->validate([
            'branch_id' => ['required', 'integer'],
        ]);

        $branches = $event->branches->toArray();
        $boardieBranch = Auth::user()->branch_memberships()->where('branch_role_id', 1)->first();
        $isUserBranchInOrganizingBranches = in_array($boardieBranch, $branches);

        if (! Auth::user()->hasEventRole($event->id, 1) && ! Auth::user()->hasRole('Administrator') && is_null($boardieBranch) && ! $isUserBranchInOrganizingBranches) {
            abort(403, 'You are not authorized to add an organizing Branch.');
        }

        $branch = Branch::find($request->branch_id);
        if ($branch == null) {
            abort(404, 'The specified branch does not exist');
        }

        $event->branches()->attach($branch->id);

        return response()->json(null, 204);
    }
}
