<?php

declare(strict_types=1);

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventRoleResource;
use App\Models\BranchRole;
use App\Models\Event;
use App\Models\EventMembership;
use App\Models\EventRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class EventRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): AnonymousResourceCollection
    {
        return EventRoleResource::collection(EventRole::all());
    }

    /**
     * Change an eventMembership eventRole
     *
     * @return \Illuminate\Http\Response
     */
    public function changeEventRole(Request $request, User $user, Event $event): JsonResponse
    {
        // Get the branches that are organizing the event
        $branch_ids = $event->branches->map(function ($branch) {
            return $branch->id;
        });
        $userBranchInOrganizingBranches =
            Auth::user()->branch_memberships()->whereIn('branch_id', $branch_ids)->where('branch_role_id', BranchRole::boardId())->first();

        if (! Auth::user()->hasEventRole($event->id, EventRole::organizerId()) && ! Auth::user()->hasRole('Administrator') && is_null($userBranchInOrganizingBranches)) {
            abort(403, 'You are not authorized to change this persons event role.');
        }

        $request->validate([
            'event_role_id' => ['required', 'integer'],
        ]);

        $eventMembership = EventMembership::firstWhere([
            ['user_id', $user->id],
            ['event_id', $event->id],
        ]);

        if ($eventMembership == null) {
            abort(404, 'The user does not have a role in this event');
        }
        $eventMembership->event_role_id = $request->event_role_id;
        $eventMembership->save();

        return response()->json(null, 204);
    }
}
