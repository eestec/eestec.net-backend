<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\BoardMember\CreateNewBoardMember;
use App\Actions\BoardMember\UpdateBoardMembership;
use App\Http\Resources\BoardMemberResource;
use App\Models\BoardMember;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResource
    {
        $mandate = $request->query('mandate');

        if ($mandate === 'all') {
            return BoardMemberResource::collection(BoardMember::paginate(12));
        }

        if (is_null($mandate)) {
            $current_year = date('Y');
            $mandate_start = Carbon::createFromDate($current_year, 8, 1);

            if ($mandate_start > Carbon::now()) {
                $mandate_start = Carbon::createFromDate($current_year - 1, 8, 1);
            }

            $members = BoardMember::whereDate('mandate_start', $mandate_start)->orderBy('order', 'ASC')->get();

            return BoardMemberResource::collection($members);
        }

        return BoardMemberResource::collection(BoardMember::where('mandate', $mandate)->orderBy('order', 'ASC')->get());
    }

    public function update(Request $request, BoardMember $position, UpdateBoardMembership $updater)
    {
        if (! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to update board members.');
        }

        $position = $updater->update($position, $request->all());

        return new BoardMemberResource($position);

    }

    public function store(Request $request, CreateNewBoardMember $creator): JsonResource|JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to create board members.');
        }
        try {
            $membership = $creator->create($request->all());
        } catch (InvalidArgumentException $e) {
            return response()->json($e->getMessage(), 400);
        }

        return new BoardMemberResource($membership);

    }

    public function destroy(BoardMember $position): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to delete board members.');
        }
        $position->delete();

        return response()->json(null, 204);
    }
}
