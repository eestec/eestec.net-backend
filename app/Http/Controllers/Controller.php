<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use OpenApi\Attributes as OA;

#[
    OA\Info(
        version: '0.1',
        description: 'Swagger OpenAPI description',
        title: 'EESTEC OpenAPI documentation',
    ),
]
abstract class Controller
{
    //
}
