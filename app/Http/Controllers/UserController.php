<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Actions\QueryBuilder\FiltersUserSearch;
use App\Contracts\Actions\DeletesUser;
use App\Http\Resources\EventCollection;
use App\Http\Resources\UserResource;
use App\Models\Branch;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use OpenApi\Attributes as OA;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): AnonymousResourceCollection
    {
        $users = QueryBuilder::for(User::class)
            ->defaultSort('last_name', 'first_name')
            ->allowedSorts(['id', 'slug'])
            ->allowedFilters([
                AllowedFilter::custom('search', new FiltersUserSearch),
            ])
            ->with(['branch_memberships']);

        return UserResource::collection($users->paginate());
    }

    #[OA\Get(
        path: '/api/users/{user-slug}',
        description: 'Return user based on slug',
        parameters: [
            new OA\Parameter(
                name: 'user-slug',
                description: 'the slug of the user',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'string')
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'Successful operation',
                content: new OA\JsonContent(ref: '#/components/schemas/profile'),
            ),
            new OA\Response(response: 409, description: 'Unauthenticated'),
        ]
    )]
    /**
     * Display the specified resource.
     */
    public function show(User $user): JsonResponse
    {
        if (Auth::id() == $user->id || Auth::user()->hasRole('Administrator')) {
            return response()->json(new UserResource($user->with(['branch_memberships', 'branch_applications'])->find($user->id)));
        }

        if (Auth::user()->email_verified_at == null || Auth::user()->branch_verified_at == null) {
            return response()->json('Unauthorized', 403);
        }

        return response()->json(new UserResource($user->with('branch_memberships')->find($user->id)));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user, UpdateUserProfileInformation $updater): JsonResource
    {
        if (Auth::user() == null || (! Auth::user()->hasRole('Administrator') && Auth::id() != $user->id)) {
            abort(403, 'You are not authorized to edit this user.');
        }

        $updater->update($user, $request->all());

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user, DeletesUser $deleteUser): JsonResponse
    {
        if (Auth::user()->hasRole('Administrator')) {
            $deleteUser->delete($user);

            return response()->json();
        }

        return response()->json('Unauthorized', 403);
    }

    /**
     * Return user's attended events
     */
    public function attendedEvents(Request $request, User $user): JsonResource
    {
        $event_kind = $request->query('kind');

        switch ($event_kind) {
            case 'active':
                $active_events = $user
                    ->attendedEvents()
                    ->where('end_date', '>=', date('Y-m-d'))
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->get();

                return new EventCollection($active_events);
            case 'past':
                $past_events = $user
                    ->attendedEvents()
                    ->where('end_date', '<', date('Y-m-d'))
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->paginate(12);

                return new EventCollection($past_events);

            default:
                $events = $user->attendedEvents()
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->paginate(12);

                return new EventCollection($events);
        }
    }

    /**
     * Return user's organized events
     */
    public function organizedEvents(Request $request, User $user): JsonResource
    {

        $event_kind = $request->query('kind');

        switch ($event_kind) {
            case 'active':
                $active_events = $user
                    ->organizedEvents()
                    ->where('end_date', '>=', date('Y-m-d'))
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->get();

                return new EventCollection($active_events);
            case 'past':
                $past_events = $user
                    ->organizedEvents()
                    ->where('end_date', '<', date('Y-m-d'))
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->paginate(12);

                return new EventCollection($past_events);

            default:
                $events = $user->organizedEvents()
                    ->with(['branches', 'entities', 'event_type'])
                    ->orderBy('end_date', 'DESC')
                    ->paginate(12);

                return new EventCollection($events);
        }
    }

    /**
     * Return the privileges of the user
     */
    public function admin(): JsonResponse
    {
        $user = Auth::user();
        if ($user->hasRole('Administrator')) {
            $entities = [
                'cities' => Branch::all()->map(function ($branch) {
                    return [
                        'id' => $branch->slug,
                        'name' => $branch->name,
                    ];
                })->sortBy('name')->values()->toArray(), // TODO: Add teams
            ];

            $result = [
                'entities' => $entities,
                'isSuper' => true,
            ];

            return response()->json($result);
        }

        $managed_branches = $user
            ->branch_memberships()
            ->wherePivot('branch_role_id', BranchRole::boardId())
            ->get()
            ->map(function ($branch) {
                return [
                    'id' => $branch->slug,
                    'name' => $branch->name,
                ];
            })->sortBy('name')->values()->toArray();

        $entities = [
            'cities' => $managed_branches, // TODO: Add teams
        ];

        $result = [
            'entities' => $entities,
            'isSuper' => false,
        ];

        return response()->json($result);
    }
}
