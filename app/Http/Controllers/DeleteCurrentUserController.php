<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Contracts\Actions\DeletesUser;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Actions\ConfirmPassword;

class DeleteCurrentUserController extends Controller
{
    /**
     * Delete the current user.
     *
     * @throws ValidationException
     */
    public function __invoke(Request $request, DeletesUser $deleteUser, StatefulGuard $guard): void
    {

        $confirmed = (new ConfirmPassword)(
            $guard,
            $request->user(),
            $request->password
        );

        if (! $confirmed) {
            throw ValidationException::withMessages([
                'password' => __('The password is incorrect.'),
            ]);
        }

        $deleteUser->delete($request->user()->fresh());

        $guard->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
}
