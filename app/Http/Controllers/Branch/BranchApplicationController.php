<?php

declare(strict_types=1);

namespace App\Http\Controllers\Branch;

use App\Actions\Branch\BranchAcceptance;
use App\Actions\Branch\CreateBranchApplication;
use App\Actions\QueryBuilder\AlphanumericNameSort;
use App\Actions\QueryBuilder\CustomDateFilter;
use App\Actions\QueryBuilder\CustomStringFilter;
use App\Actions\QueryBuilder\FiltersBranchApplicationSearch;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchApplicationCollection;
use App\Models\Branch;
use App\Models\BranchApplication;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Validator;

class BranchApplicationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Branch $branch, User $user, CreateBranchApplication $creator): JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator'))) {
            return response()->json('You are not authorized to add users to branch.', 403);
        }

        if ($user->belongsToBranch($branch->id)) {
            return response()->json('The user is already a member of the branch', 400);
        }

        if ($user->branch_applications()->find($branch->id) != null) {
            return response()->json('There is an already pending application from the user to this branch', 400);
        }

        $creator->create($branch, $user);

        return response()->json();
    }

    /**
     * Return all applications for a specific branch.
     */
    public function getApplications(Branch $branch): JsonResource|JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator') || Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()))) {
            return response()->json('You are not authorized to view pending branch applications.', 403);
        }

        // Build the query with QueryBuilder
        $applications = QueryBuilder::for(BranchApplication::class)
            ->join('users', 'branch_applications.user_id', '=', 'users.id')
            ->with('user')
            ->where('branch_id', $branch->id)
            ->select(['branch_applications.*', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.slug', 'users.profile_photo_path'])
            ->defaultSort('branch_applications.created_at')
            ->allowedSorts([
                'branch_applications.created_at',
                AllowedSort::custom('first_name', new AlphanumericNameSort, 'users.first_name'),
                AllowedSort::custom('last_name', new AlphanumericNameSort, 'users.last_name'),
            ])
            ->allowedFilters([
                AllowedFilter::custom('first_name', new CustomStringFilter, 'users.first_name'),
                AllowedFilter::custom('last_name', new CustomStringFilter, 'users.last_name'),
                AllowedFilter::custom('created_at', new CustomDateFilter, 'branch_applications.created_at'),
                AllowedFilter::custom('search', new FiltersBranchApplicationSearch),
            ]);

        return new BranchApplicationCollection($applications->paginate(12));
    }

    /**
     * Approve a branch application.
     */
    public function approval(Request $request, Branch $branch, BranchApplication $application, BranchAcceptance $updater): JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator') || Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()))) {
            return response()->json('You are not authorized to accept or reject branch applications.', 403);
        }

        if ($branch->id != $application->branch_id) {
            return response()->json('This application is not for that branch', 400);
        }

        $user = User::find($application->user_id);

        if ($user->belongsToBranch($branch->id)) {
            return response()->json('You cannot accept a user to a branch when he is already a member of that branch.', 400);
        }
        $updater->one($application, $user, $branch, $request->all());

        return response()->json();
    }

    public function massApproval(Request $request, Branch $branch, BranchAcceptance $updater): JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator') || Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()))) {
            return response()->json('You are not authorized to accept or reject branch applications.', 403);
        }

        $validated = Validator::make($request->all(), [
            'acceptance' => ['required', 'boolean'],
            'applications.*' => Rule::forEach(function (?string $value, string $attribute) {
                return [
                    Rule::exists(BranchApplication::class, 'id'),
                ];
            }),
        ])->validate();

        $applications = BranchApplication::where('branch_id', $branch->id)
            ->whereIn('id', $validated['applications'])
            ->get();
        if (count($applications) !== count($validated['applications'])) {
            response()->json('one of the applications is not for this branch', 400);
        }

        try {
            $updater->many($applications, $branch, $validated['acceptance']);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }

        return response()->json();
    }

    /**
     * Apply as the current user to the branch.
     */
    public function apply(Branch $branch, CreateBranchApplication $creator): JsonResponse
    {
        if (Auth::user()->belongsToBranch($branch->id)) {
            return response()->json('The user is already a member of the branch', 400);
        }

        if (Auth::user()->branch_applications()->find($branch->id) != null) {
            return response()->json('There is an already pending application from the user to this branch', 400);
        }

        $creator->create($branch, Auth::user());

        return response()->json(['success' => true]);
    }
}
