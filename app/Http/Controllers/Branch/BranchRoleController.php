<?php

declare(strict_types=1);

namespace App\Http\Controllers\Branch;

use App\Actions\Branch\BranchRoleChanger;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchRoleResource;
use App\Models\Branch;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Validator;

class BranchRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): AnonymousResourceCollection
    {
        return BranchRoleResource::collection(BranchRole::all());
    }

    /**
     * Change the role of a user in a branch.
     *
     * @throws ValidationException
     */
    public function changeRole(Request $request, Branch $branch, User $user, BranchRoleChanger $changer): JsonResponse
    {
        $validated = Validator::make($request->all(), [
            'branch_role_id' => ['required', 'integer'],
        ])->validate();

        if (! (Auth::user()->hasRole('Administrator') || Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()))) {
            return response()->json('You are not authorized to change user roles for this branch.', 403);
        }

        $role = BranchRole::findOrFail($validated['branch_role_id']);
        try {
            $changer->one($branch, $user, $role);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }

        return response()->json(['success' => true]);
    }

    public function massChangeRole(Request $request, Branch $branch, BranchRoleChanger $changer): JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator') || Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()))) {
            return response()->json('You are not authorized to change user roles for this branch.', 403);
        }

        $validated = Validator::make($request->all(), [
            'branch_role_id' => ['required', 'integer'],
            'users.*' => Rule::forEach(function (?string $value, string $attribute) {
                return [
                    Rule::exists(User::class, 'slug'),
                ];
            }),
        ])->validate();
        $users = User::whereIn('slug', $validated['users'])->get();
        $role = BranchRole::findOrFail($validated['branch_role_id']);

        try {
            $changer->many($branch, $users, $role);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }

        return response()->json([], 200);
    }
}
