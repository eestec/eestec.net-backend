<?php

declare(strict_types=1);

namespace App\Http\Controllers\Branch;

use App\Actions\Branch\CreateNewBranch;
use App\Actions\Branch\UpdateBranchInformation;
use App\Actions\QueryBuilder\AlphanumericNameSort;
use App\Actions\QueryBuilder\CustomDateFilter;
use App\Actions\QueryBuilder\CustomInclusionFilter;
use App\Actions\QueryBuilder\CustomNumericFilter;
use App\Actions\QueryBuilder\CustomStringFilter;
use App\Actions\QueryBuilder\FiltersBranchSearch;
use App\Actions\QueryBuilder\FiltersBranchUserSearch;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchCollection;
use App\Http\Resources\BranchMembershipCollection;
use App\Http\Resources\BranchResource;
use App\Http\Resources\EventCollection;
use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\BranchType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;
use OpenApi\Attributes as OA;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class BranchController extends Controller
{
    #[OA\Get(
        path: '/api/branches',
        description: 'Return list of branches',
        responses: [
            new OA\Response(response: 200, description: 'Successful operation'),
        ]
    )]
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): BranchCollection
    {
        $is_admin = Auth::user() != null && Auth::user()->hasRole('Administrator');
        $return_all = $request->query('all') !== null;

        $type_ids = $is_admin
            ? BranchType::pluck('id')->toArray()
            : BranchType::where('name', '<>', 'Terminated')->pluck('id')->toArray();

        $branches = QueryBuilder::for(Branch::class)
            ->defaultSort('name')
            ->allowedSorts([
                AllowedSort::custom('name', new AlphanumericNameSort, 'name'),
                AllowedSort::custom('country', new AlphanumericNameSort, 'country'),
                AllowedSort::custom('slug', new AlphanumericNameSort, 'slug'),
                'region',
                'branch_type_id',
            ])
            ->allowedFilters([
                AllowedFilter::custom('search', new FiltersBranchSearch),
                AllowedFilter::custom('name', new CustomStringFilter),
                AllowedFilter::custom('country', new CustomStringFilter),
                AllowedFilter::custom('slug', new CustomStringFilter),
                AllowedFilter::custom('region', new CustomNumericFilter),
                AllowedFilter::custom('branch_type_id', new CustomInclusionFilter),
            ])
            ->with('branch_type')
            ->withCount('activeMembers')
            ->whereIn('branch_type_id', $type_ids);

        if ($return_all) {
            return new BranchCollection($branches->get());
        }

        return new BranchCollection($branches->paginate(12));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, CreateNewBranch $creator): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to create branches.');
        }

        $branch = $creator->create($request->all());

        return (new BranchResource($branch))
            ->response()
            ->setStatusCode(201);
    }

    #[OA\Get(
        path: '/api/branches/{branch-slug}',
        description: 'Return branch based on slug',
        parameters: [
            new OA\Parameter(
                name: 'branch-slug',
                description: 'the slug of the branch',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'string')
            ),
        ],
        responses: [
            new OA\Response(response: 200, description: 'Successful operation'),
        ]
    )]
    /**
     * Display the specified resource.
     */
    public function show(Branch $branch): JsonResponse
    {
        $can_edit = Auth::user() != null && (Auth::user()->hasBranchRole($branch->id, BranchRole::boardId()) ||
            Auth::user()->hasRole('Administrator')
        );
        $response = [
            'branch' => new BranchResource($branch->withCount(['activeMembers'])->find($branch->id)),
            'can_edit' => $can_edit,
        ];

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Branch $branch, UpdateBranchInformation $updater): JsonResource
    {
        if (! Auth::user()->hasRole('Administrator') && ! Auth::user()->hasBranchRole($branch->id, BranchRole::boardId())) {
            abort(403, 'You are not authorized to edit this branch.');
        }

        try {
            $updater->update($branch, $request->all());
        } catch (InvalidArgumentException $e) {
            abort(403, $e->getMessage());
        }

        return new BranchResource($branch);
    }

    /**
     * Get the members for the specified branch.
     */
    public function members(Request $request, Branch $branch): JsonResource
    {
        $members_kind = $request->query('kind');

        $members = QueryBuilder::for(BranchMembership::class)
            ->join('users', 'branch_memberships.user_id', '=', 'users.id')
            ->with('user')
            ->where('branch_id', $branch->id)
            ->defaultSort('users.slug')
            ->allowedSorts([
                AllowedSort::custom('first_name', new AlphanumericNameSort, 'users.first_name'),
                AllowedSort::custom('slug', new AlphanumericNameSort, 'users.slug'),
                AllowedSort::custom('last_name', new AlphanumericNameSort, 'users.last_name'),
                AllowedSort::field('created_at', 'branch_memberships.created_at'),
                AllowedSort::field('updated_at', 'branch_memberships.updated_at'),
                AllowedSort::field('branch_role_id', 'branch_memberships.branch_role_id'),
            ])
            ->allowedFilters([
                AllowedFilter::custom('search', new FiltersBranchUserSearch),
                AllowedFilter::custom('first_name', new CustomStringFilter, 'users.first_name'),
                AllowedFilter::custom('last_name', new CustomStringFilter, 'users.last_name'),
                AllowedFilter::custom('slug', new CustomStringFilter, 'users.slug'),
                AllowedFilter::custom('created_at', new CustomDateFilter, 'branch_memberships.created_at'),
                AllowedFilter::custom('updated_at', new CustomDateFilter, 'branch_memberships.updated_at'),
                AllowedFilter::custom('branch_role_id', new CustomInclusionFilter, 'branch_memberships.branch_role_id'),
            ]);

        switch ($members_kind) {
            case 'board':
                $members = $members->where('branch_role_id', BranchRole::boardId());
                break;
            case 'active':
                $members = $members->whereIn('branch_role_id', [BranchRole::boardId(), BranchRole::memberId()]);
                break;
            case 'alumni':
                $members = $members->where('branch_role_id', BranchRole::alumniId());
                break;
            default:
                break;
        }

        return new BranchMembershipCollection($members->paginate(12));
    }

    /**
     * Get the board members for the specified branch.
     */
    public function boardMembers(Branch $branch): JsonResource
    {
        return $this->members(new Request(['kind' => 'board']), $branch);
    }

    /**
     * Return Branch events.
     */
    public function events(Request $request, Branch $branch): EventCollection
    {
        $event_kind = $request->query('kind');

        $events = $branch
            ->events()
            ->with(['event_type', 'branches', 'entities'])
            ->orderBy('end_date', 'DESC');

        switch ($event_kind) {
            case 'active':
                $events = $events->where('end_date', '>=', date('Y-m-d'));
                break;
            case 'past':
                $events = $events->where('end_date', '<', date('Y-m-d'));
                break;
            default:
                break;
        }

        return new EventCollection($events->paginate(12));
    }
}
