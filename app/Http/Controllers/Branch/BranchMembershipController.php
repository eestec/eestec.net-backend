<?php

declare(strict_types=1);

namespace App\Http\Controllers\Branch;

use App\Actions\Branch\BranchMembershipDeletion;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchMembershipResource;
use App\Mail\BranchApplicationApproval;
use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Str;

class BranchMembershipController extends Controller
{
    /**
     * Create a new branch membership in storage (can only be done directly by an admin).
     */
    public function store(Request $request, Branch $branch, User $user): JsonResponse
    {
        if (! (Auth::user()->hasRole('Administrator'))) {
            return response()->json('You are not authorized to add users to branches.', 403);
        }

        $validated = $request->validate([
            'branch_role_id' => ['required', 'integer', Rule::exists('branch_roles', 'id')],
        ]);

        $membership = BranchMembership::create([
            'user_id' => $user->id,
            'branch_id' => $branch->id,
            'branch_role_id' => $validated['branch_role_id'],
        ]);

        $user->branch_verified_at = now();
        $user->save();

        Mail::to($user->email)->queue(new BranchApplicationApproval($user, $branch, true));

        return response()->json(new BranchMembershipResource($membership));
    }

    /**
     * Remove user from a branch
     */
    public function destroy(Branch $branch, User $user, BranchMembershipDeletion $deleter): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator') && ! Auth::user()->hasBranchRole($branch->id, BranchRole::boardId())) {
            return response()->json('You are not authorized to remove users from this branch.', 403);
        }

        try {
            $deleter->one($branch, $user);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }

        return response()->json(['success' => true]);
    }

    public function destroyMany(Request $request, Branch $branch, BranchMembershipDeletion $deleter): JsonResponse
    {
        if (! Auth::user()->hasRole('Administrator') && ! Auth::user()->hasBranchRole($branch->id, BranchRole::boardId())) {
            return response()->json('You are not authorized to remove users from this branch.', 403);
        }

        // Split slugs into an array based on commas
        $slugs = Str::of($request->query('slugs'))->split('/,+/');

        $users = User::whereIn('slug', $slugs)->get();

        // If they are equal we have as many users as ids.
        if (count($users) !== count($slugs)) {
            return response('Not all users were found', 400)->json();
        }

        try {
            $deleter->many($branch, $users);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }

        return response()->json([], 200);

    }
}
