<?php

declare(strict_types=1);

namespace App\Http\Controllers\Branch;

use App\Actions\Branch\UpdateBranchType;
use App\Http\Controllers\Controller;
use App\Http\Resources\BranchTypeResource;
use App\Models\Branch;
use App\Models\BranchType;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BranchTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): AnonymousResourceCollection
    {
        return BranchTypeResource::collection(BranchType::get());
    }

    /**
     * Change the type of a branch.
     */
    public function changeType(Request $request, Branch $branch, UpdateBranchType $updater): JsonResponse
    {
        if (Auth::user() == null || ! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to change branch types.');
        }
        $updater->update($branch, $request->all());

        return response()->json();
    }
}
