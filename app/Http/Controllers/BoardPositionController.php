<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\BoardPosition\UpdateBoardPosition;
use App\Http\Resources\BoardPositionResource;
use App\Models\BoardPosition;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class BoardPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResource
    {
        return BoardPositionResource::collection(
            BoardPosition::orderBy('id', 'asc')->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(BoardPosition $position): JsonResource
    {
        return new BoardPositionResource($position);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BoardPosition $position, UpdateBoardPosition $updater)
    {
        if (! Auth::user()->hasRole('Administrator')) {
            abort(403, 'You are not authorized to update board positions.');
        }

        $position = $updater->update($position, $request->all());

        return new BoardPositionResource($position);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
