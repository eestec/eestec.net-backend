<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Illuminate\Http\Request;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;
use Laravel\Fortify\Fortify;
use Symfony\Component\HttpFoundation\Response;

class LoginResponse implements LoginResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     * We have added the verification of the user email on the
     * login info, so that it is easier for the frontend to know
     * what page to display.
     *
     * @param  Request  $request
     */
    public function toResponse($request): Response
    {
        $attributes = auth()->user()->getAttributes();
        $email_verified = isset($attributes['email_verified_at']);
        $branch_verified = isset($attributes['branch_verified_at']);

        return $request->wantsJson()
            ? response()->json([
                'two_factor' => false,
                'email_verified' => $email_verified,
                'branch_verified' => $branch_verified,
            ])
            : redirect()->intended(Fortify::redirects('login'));
    }
}
