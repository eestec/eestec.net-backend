<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Actions\File\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'profile',
    title: 'User',
    description: 'The profile of the user',
    required: [
        'first_name',
        'last_name',
        'slug',
        'email',
        'description',
        'profile_photo_url',
    ],
    properties: [
        'id' => new OA\Property(property: 'id'),
        'email_verified_at' => new OA\Property(property: 'email_verified_at'),
        'branch_verified_at' => new OA\Property(property: 'branch_verified_at'),
        'role_id' => new OA\Property(property: 'role_id'),
        'created_at' => new OA\Property(property: 'created_at'),
        'updated_at' => new OA\Property(property: 'updated_at'),
        'first_name' => new OA\Property(property: 'first_name', description: 'The first name of the user'),
        'last_name' => new OA\Property(property: 'last_name', description: 'The last name of the user'),
        'slug' => new OA\Property(property: 'slug'),
        'email' => new OA\Property(property: 'email'),
        'description' => new OA\Property(property: 'description'),
        'profile_photo_path' => new OA\Property(property: 'profile_photo_url'),
    ],
    type: 'object'
)]
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'slug' => $this->slug,
            'profile_photo_path' => Image::url($this->profile_photo_path),
            'role' => new RoleResource($this->whenLoaded('role')),
            $this->mergeWhen(Str::startsWith($request->path(), 'api/user') || Str::startsWith($request->path(), 'api/users/'), [
                'email_verified_at' => $this->email_verified_at,
                'branch_verified_at' => $this->branch_verified_at,
                'description' => $this->description,
                'banner_path' => Image::url($this->banner_path),
                $this->mergeWhen(Auth::user() != null && (Auth::id() == $this->id || Auth::user()->hasRole('Administrator')), [
                    'email' => $this->email,
                    'id' => $this->id,
                    'role_id' => $this->role_id,
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                    'branch_applications' => $this->whenLoaded(
                        'branch_applications',
                        function () {
                            return $this->branch_applications->map(function ($branch) {
                                return new BranchResource($branch);
                            });
                        }),
                ]),
            ]),

            'branches' => $this->whenLoaded(
                'branch_memberships',
                function () {
                    return $this->branch_memberships->map(function ($branch) {
                        return new BranchResource($branch);
                    });
                }
            ),
        ];
    }
}
