<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Actions\File\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'email' => $this->email,
            'founded_in' => $this->founded_in,
            'website' => $this->website,
            'country' => $this->country,
            'region' => $this->region,
            'facebook' => $this->facebook,
            'instagram' => $this->instagram,
            'linkedin' => $this->linkedin,
            'address' => $this->address,
            'member_no' => $this->whenCounted('activeMembers'),
            'description' => $this->description,
            'profile_picture_path' => Image::url($this->profile_picture_path),
            'large_profile_picture_path' => Image::url($this->large_profile_picture_path),
            'branch_type' => new BranchTypeResource($this->branch_type),
        ];
    }
}
