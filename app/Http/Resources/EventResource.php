<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Actions\File\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'application_deadline' => $this->application_deadline,
            'max_participants' => $this->max_participants,
            'participation_fee' => $this->participation_fee,
            'location' => $this->location,
            'description' => $this->description,
            'profile_picture' => Image::url($this->profile_picture_path),
            'banner_path' => Image::url($this->banner_path),
            'event_type' => new EventTypeResource($this->event_type),
            'branches' => $this->whenLoaded(
                'branches',
                function () {
                    return $this->branches->map(function ($branch) {
                        return new BranchResource($branch);
                    });
                }
            ),
            'entities' => $this->whenLoaded(
                'entities',
                function () {
                    return $this->entities->map(function ($entity) {
                        return new EntityResource($entity);
                    });
                }
            ),
        ];
    }
}
