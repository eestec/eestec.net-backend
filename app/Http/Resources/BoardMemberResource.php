<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BoardMemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'position' => $this->board_position,
            'order' => $this->order,
            'mandate_start' => $this->mandate_start,
            'mandate_end' => $this->mandate_end,
            'mandate' => $this->mandate,
            'user' => $this->whenLoaded('user', new UserResource($this->user)),
        ];
    }
}
