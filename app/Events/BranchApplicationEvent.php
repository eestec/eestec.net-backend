<?php

declare(strict_types=1);

namespace App\Events;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BranchApplicationEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The branch instance
     */
    public Branch $branch;

    /**
     * The branch instance
     */
    public User $user;

    /**
     * Create a new branch application instance
     */
    public function __construct(Branch $branch, User $user)
    {
        $this->user = $user;
        $this->branch = $branch;
    }

    /**
     * Get the channels the event should broadcast on.
     */
    public function broadcastOn(): Channel|array
    {
        return new PrivateChannel('branch-applications');
    }
}
