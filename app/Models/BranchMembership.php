<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $branch_role_id
 * @property int $branch_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Branch $branch
 * @property-read \App\Models\BranchRole|null $role
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\BranchMembershipFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereBranchRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchMembership whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperBranchMembership
 */
class BranchMembership extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_memberships';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'branch_role_id',
        'branch_id',
    ];

    /**
     * Return the role of this membership.
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(BranchRole::class);
    }

    /** Return the user of this membership.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the branch of this membership.
     */
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
}
