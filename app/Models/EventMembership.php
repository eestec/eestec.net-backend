<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $event_role_id
 * @property int $event_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @property-read \App\Models\EventRole|null $role
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\EventMembershipFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereEventRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventMembership whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEventMembership
 */
class EventMembership extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_memberships';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'event_role_id',
        'event_id',
    ];

    /**
     * Return the role of this membership.
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(EventRole::class);
    }

    /** Return the user of this membership.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the event of this membership.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
