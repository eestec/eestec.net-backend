<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property string $motivational_letter
 * @property bool|null $acceptance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property int $entity_id
 * @property-read \App\Models\Branch $entity
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\EntityApplicationFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereAcceptance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereMotivationalLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityApplication whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEntityApplication
 */
class EntityApplication extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entity_applications';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'motivational_letter',
        'acceptance',
    ];

    /** Return the user of this application.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the entity of this application.
     */
    public function entity(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
}
