<?php

declare(strict_types=1);

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OpenApi\Attributes as OA;

/**
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string $to
 * @property string|null $cc
 * @property string $text
 * @property string $to_text
 * @property string $link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Contact findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact query()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereCc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereToText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperContact
 */
#[OA\Schema(
    type: 'object',
    properties: [
        'slug' => new OA\Property(property: 'slug', type: 'string', description: 'The slug of the contact'),
        'text' => new OA\Property(property: 'text', type: 'string', description: 'The text displayed to the user'),
        'link' => new OA\Property(property: 'link', type: 'string', description: 'The text displayed on the link'),
        'to-text' => new OA\Property(property: 'to-text', type: 'string', description: 'The text displayed on the To section'),
    ],
)]
class Contact extends Model
{
    use HasFactory;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'to',
        'cc',
        'text',
        'link',
        'to_text',
    ];

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
