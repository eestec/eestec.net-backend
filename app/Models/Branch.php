<?php

declare(strict_types=1);

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string $email
 * @property int|null $founded_in
 * @property string|null $website
 * @property string|null $country
 * @property int|null $region
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $linkedin
 * @property string|null $address
 * @property string|null $description
 * @property string|null $profile_picture_path
 * @property string|null $large_profile_picture_path
 * @property int|null $branch_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $applicants
 * @property-read int|null $applicants_count
 * @property-read \App\Models\BranchType|null $branch_type
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Event> $events
 * @property-read int|null $events_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $members
 * @property-read int|null $members_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\BranchRole> $roles
 * @property-read int|null $roles_count
 *
 * @method static \Database\Factories\BranchFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Branch findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch query()
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereBranchTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereFoundedIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereLargeProfilePicturePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereLinkedin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereProfilePicturePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Branch withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperBranch
 */
class Branch extends Model
{
    use HasFactory, Sluggable;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['branch_type'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'founded_in',
        'website',
        'description',
        'profile_picture_path',
        'large_profile_picture_path',
        'country',
        'region',
        'facebook',
        'instagram',
        'branch_type_id',
        'linkedin',
        'address',
        'member_no',
    ];

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     * More documentation here:
     * https://github.com/cviebrock/eloquent-sluggable/blob/master/ROUTE-MODEL-BINDING.md
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Return the branch's type.
     */
    public function branch_type(): BelongsTo
    {
        return $this->belongsTo(BranchType::class);
    }

    /**
     * Return the events the branch is an organizer for.
     */
    public function events(): MorphToMany
    {
        return $this->morphToMany(Event::class, 'event_organizer')
            ->withTimestamps()
            ->using(EventOrganizer::class);
    }

    /**
     * Return the applicants for this branch.
     */
    public function applicants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_applications')
            ->withPivot('acceptance')
            ->withTimestamps()
            ->using(BranchApplication::class);
    }

    /**
     * Return the members for this branch.
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->withTimestamps()
            ->withPivot('branch_role_id');
    }

    /**
     * Return the roles for this branch.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(BranchRole::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->withTimestamps()
            ->withPivot('user_id');
    }

    /**
     * Returns board members.
     *
     * This is hardcoded with the branch_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function boardMembers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->wherePivot('branch_role_id', BranchRole::boardId())
            ->withTimestamps()
            ->withPivot('branch_role_id');
    }

    /**
     * Returns active members.
     *
     * This is hardcoded with the branch_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function activeMembers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->wherePivotIn('branch_role_id', [BranchRole::boardId(), BranchRole::memberId()])
            ->withTimestamps()
            ->withPivot('branch_role_id');
    }

    /**
     * Returns alumni members.
     *
     * This is hardcoded with the branch_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function alumniMembers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->wherePivot('branch_role_id', BranchRole::alumniId())
            ->withTimestamps()
            ->withPivot('branch_role_id');
    }
}
