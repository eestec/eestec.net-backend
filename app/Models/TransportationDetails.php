<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $event_id
 * @property string $arrival
 * @property string $departure
 * @property string $transportation_type
 * @property string $comment
 * @property string $arrival_location
 * @property string $departure_location
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\TransportationDetailsFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereArrival($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereArrivalLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereDeparture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereDepartureLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereTransportationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransportationDetails whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperTransportationDetails
 */
class TransportationDetails extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transportation_details';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'event_id',
        'arrival',
        'departure',
        'transportation_type',
        'comment',
        'arrival_location',
        'departure_location',
    ];

    /** Return the user of this transportation detail.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the event of this transportation detail.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
