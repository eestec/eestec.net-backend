<?php

declare(strict_types=1);

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string|null $description
 * @property string|null $profile_picture_path
 * @property int|null $entity_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $applicants
 * @property-read int|null $applicants_count
 * @property-read \App\Models\EntityType|null $entity_type
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Event> $events
 * @property-read int|null $events_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $members
 * @property-read int|null $members_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\EntityRole> $roles
 * @property-read int|null $roles_count
 *
 * @method static \Database\Factories\EntityFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Entity findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Entity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Entity query()
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereEntityTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereProfilePicturePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Entity withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEntity
 */
class Entity extends Model
{
    use HasFactory;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'profile_picture',
    ];

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     * More documentation here:
     * https://github.com/cviebrock/eloquent-sluggable/blob/master/ROUTE-MODEL-BINDING.md
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Return the entity's type.
     */
    public function entity_type(): BelongsTo
    {
        return $this->belongsTo(EntityType::class);
    }

    /**
     * Return the events the entity is an organizer for.
     */
    public function events(): MorphToMany
    {
        return $this->morphToMany(Event::class, 'event_organizer')
            ->using(EventOrganizer::class);
    }

    /**
     * Return the applicants for this entity.
     */
    public function applicants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'entity_applications')
            ->withPivot('motivational_letter', 'acceptance')
            ->using(EntityApplication::class);
    }

    /**
     * Return the members for this entity.
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'entity_memberships')
            ->using(EntityMembership::class)
            ->withPivot('entity_role_id');
    }

    /**
     * Return the roles for this entity.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(EntityRole::class, 'entity_memberships')
            ->using(EntityMembership::class)
            ->withPivot('user_id');
    }
}
