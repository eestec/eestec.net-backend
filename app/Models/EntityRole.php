<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $permissions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\EntityMembership> $memberships
 * @property-read int|null $memberships_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityRole whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEntityRole
 */
class EntityRole extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'permissions',
    ];

    /**
     * Return the applicants for this entity.
     */
    public function memberships(): HasMany
    {
        return $this->hasMany(EntityMembership::class);
    }
}
