<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $event_id
 * @property string $motivational_letter
 * @property int|null $acceptance
 * @property int|null $priority
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\EventApplicationFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereAcceptance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereMotivationalLetter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventApplication whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEventApplication
 */
class EventApplication extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_applications';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'event_id',
        'motivational_letter',
        'acceptance',
        'priority',
    ];

    /** Return the user of this application.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the event of this application.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
}
