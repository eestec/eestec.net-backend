<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Branch> $branches
 * @property-read int|null $branches_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchType whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperBranchType
 */
class BranchType extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Returns the branches of this branch type.
     */
    public function branches(): HasMany
    {
        return $this->hasMany(Branch::class);
    }

    public static function nameToId(string $name): int
    {
        return self::firstWhere('name', $name)->id;
    }
}
