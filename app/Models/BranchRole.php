<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $permissions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\BranchMembership> $memberships
 * @property-read int|null $memberships_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchRole whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperBranchRole
 */
class BranchRole extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'permissions',
    ];

    /**
     * Return the memberships of this role.
     */
    public function memberships(): HasMany
    {
        return $this->hasMany(BranchMembership::class);
    }

    /**
     * Return branch role id with name 'Board'
     */
    public static function boardId(): int
    {
        return self::firstWhere('name', 'Board')->id;
    }

    /**
     * Return branch role id with name 'Member'
     */
    public static function memberId(): int
    {
        return self::firstWhere('name', 'Member')->id;
    }

    /**
     * Return branch role id with name 'Alumni'
     */
    public static function alumniId(): int
    {
        return self::firstWhere('name', 'Alumni')->id;
    }
}
