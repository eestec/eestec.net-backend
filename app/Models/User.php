<?php

declare(strict_types=1);

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $slug
 * @property string $email
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $branch_verified_at
 * @property \Illuminate\Support\Carbon|null $last_password_update
 * @property mixed $password
 * @property string|null $remember_token
 * @property string|null $profile_photo_path
 * @property string|null $banner_path
 * @property int|null $role_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Branch> $branch_applications
 * @property-read int|null $branch_applications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Branch> $branch_memberships
 * @property-read int|null $branch_memberships_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Entity> $entity_applications
 * @property-read int|null $entity_applications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Event> $event_applications
 * @property-read int|null $event_applications_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Event> $event_memberships
 * @property-read int|null $event_memberships_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Role|null $role
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Sanctum\PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Event> $transportation_details
 * @property-read int|null $transportation_details_count
 *
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|User findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBannerPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBranchVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastPasswordUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'slug',
        'email',
        'role_id',
        'description',
        'password',
        'profile_photo_path',
        'banner_path',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'branch_verified_at' => 'datetime',
            'last_password_update' => 'datetime',
            'password' => 'hashed',
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['first_name', 'last_name'],
            ],
        ];
    }

    /**
     * Get the route key for the model.
     * More documentation here:
     * https://github.com/cviebrock/eloquent-sluggable/blob/master/ROUTE-MODEL-BINDING.md
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Determine if the user has a branch verified.
     */
    public function hasBranchVerified(): bool
    {
        return $this->branch_verified_at !== null;
    }

    /**
     * Return the user's role.
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Returns boolean value if the user has the parameter role
     */
    public function hasRole(string $role_name): bool
    {
        return $this->role->name == $role_name;
    }

    /**
     * Return the entities the user has applied to.
     */
    public function entity_applications(): BelongsToMany
    {
        return $this->belongsToMany(Entity::class, 'entity_applications')
            ->withPivot('motivational_letter', 'acceptance')
            ->withTimestamps()
            ->using(EntityApplication::class);
    }

    /**
     * Return the branches the user has applied to.
     */
    public function branch_applications(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class, 'branch_applications')
            ->withPivot('acceptance')
            ->withTimestamps()
            ->using(BranchApplication::class);
    }

    /**
     * Return the events the user has applied to.
     */
    public function event_applications(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'event_applications')
            ->withPivot('motivational_letter', 'acceptance')
            ->withTimestamps()
            ->using(EventApplication::class);
    }

    /**
     * Return the transportation details for all the events the user has attended.
     */
    public function transportation_details(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'transportation_details')
            ->withTimestamps()
            ->using(TransportationDetails::class);
    }

    /**
     * Return eventMembership the member is part of
     */
    public function event_memberships(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'event_memberships')
            ->withTimestamps()
            ->using(EventMembership::class);
    }

    /**
     * Return the events the user been accepted to.
     */
    public function attendedEvents(): BelongsToMany
    {
        return $this->event_memberships()
            ->wherePivot('event_role_id', EventRole::participantId());
    }

    /**
     * Return the events the user has organized.
     */
    public function organizedEvents(): BelongsToMany
    {
        return $this->event_memberships()
            ->wherePivot('event_role_id', EventRole::organizerId());
    }

    /**
     * Return bool value if the user has an event role in the event membership with the given event id.
     */
    public function hasEventRole(int $event_id, int $role_id): bool
    {
        return $this->event_memberships()
            ->where([
                ['event_id', $event_id],
                ['event_role_id', $role_id],
            ])
            ->exists();
    }

    /**
     * Return the branch memberships of the user.
     */
    public function branch_memberships(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class, 'branch_memberships')
            ->using(BranchMembership::class)
            ->withTimestamps()
            ->withPivot('branch_role_id');
    }

    /**
     * Return bool value if the user has a branch role name in the branch membership with the given branch id.
     */
    public function hasBranchRole(int $branch_id, int $role_id): bool
    {
        return $this->branch_memberships()
            ->where('branch_id', $branch_id)
            ->where('branch_role_id', $role_id)
            ->exists();
    }

    /**
     * Return bool if a user belongs to a branch
     */
    public function belongsToBranch(int $branch_id): bool
    {
        return $this->branch_memberships()
            ->where('branch_id', $branch_id)
            ->exists();
    }

    public function appliedToBranch(int $branch_id): bool
    {
        return $this->branch_applications()
            ->where('branch_id', $branch_id)
            ->exists();
    }
}
