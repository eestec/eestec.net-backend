<?php

declare(strict_types=1);

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string $start_date
 * @property string $end_date
 * @property string $application_deadline
 * @property int $max_participants
 * @property int $participation_fee
 * @property string|null $location
 * @property string $description
 * @property int|null $event_type_id
 * @property string|null $profile_picture_path
 * @property string|null $banner_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $applicants
 * @property-read int|null $applicants_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Branch> $branches
 * @property-read int|null $branches_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Entity> $entities
 * @property-read int|null $entities_count
 * @property-read \App\Models\EventType|null $event_type
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $members
 * @property-read int|null $members_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $transportation_details
 * @property-read int|null $transportation_details_count
 *
 * @method static \Database\Factories\EventFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Event findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereApplicationDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereBannerPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEventTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereMaxParticipants($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereParticipationFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereProfilePicturePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEvent
 */
class Event extends Model
{
    use HasFactory;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'start_date',
        'end_date',
        'application_deadline',
        'max_participants',
        'participation_fee',
        'location',
        'description',
        'event_type_id',
        'profile_picture_path',
        'banner_path',
    ];

    /**
     * Return the sluggable configuration array for this model.
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     * More documentation here:
     * https://github.com/cviebrock/eloquent-sluggable/blob/master/ROUTE-MODEL-BINDING.md
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Return the event's type.
     */
    public function event_type(): BelongsTo
    {
        return $this->belongsTo(EventType::class);
    }

    /**
     * Return the branches that are organizers of the event.
     */
    public function branches(): MorphToMany
    {
        return $this->morphedByMany(Branch::class, 'event_organizer', 'event_organizers')
            ->withTimestamps()
            ->using(EventOrganizer::class);
    }

    /**
     * Return the entities that are organizers of the event.
     */
    public function entities(): MorphToMany
    {
        return $this->morphedByMany(Entity::class, 'event_organizer', 'event_organizers')
            ->withTimestamps()
            ->using(EventOrganizer::class);
    }

    /**
     * Return the applicants to this event.
     */
    public function applicants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_applications')
            ->withTimestamps()
            ->using(EventApplication::class);
    }

    /**
     * Get the transportation details for all participants.
     */
    public function transportation_details(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'transportation_details')
            ->withTimestamps()
            ->using(TransportationDetails::class);
    }

    /**
     * Return the members for this event.
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_memberships')
            ->using(EventMembership::class)
            ->withTimestamps()
            ->withPivot('event_role_id');
    }

    /**
     * Returns event participants.
     *
     * This is hardcoded with the event_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_memberships')
            ->using(EventMembership::class)
            ->wherePivot('event_role_id', EventRole::participantId())
            ->withTimestamps()
            ->withPivot('event_role_id');
    }

    /**
     * Returns event organizers.
     *
     * This is hardcoded with the event_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function organizers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_memberships')
            ->using(EventMembership::class)
            ->wherePivot('event_role_id', EventRole::organizerId())
            ->withTimestamps()
            ->withPivot('event_role_id');
    }

    /**
     * Returns event helpers.
     *
     * This is hardcoded with the event_role_id, so changing this tables
     * might break things. TODO: Extend this with more branch roles.
     */
    public function helpers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'event_memberships')
            ->using(EventMembership::class)
            ->wherePivot('event_role_id', EventRole::helperId())
            ->withTimestamps()
            ->withPivot('event_role_id');
    }
}
