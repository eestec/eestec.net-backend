<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphPivot;

/**
 * @property int $id
 * @property int $event_id
 * @property string $event_organizer_type
 * @property int $event_organizer_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereEventOrganizerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereEventOrganizerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventOrganizer whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEventOrganizer
 */
class EventOrganizer extends MorphPivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_organizers';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'event_id',
        'event_organizer_id',
        'event_organizer_type',
    ];
}
