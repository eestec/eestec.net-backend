<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $branch_id
 * @property int|null $acceptance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Branch $branch
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\BranchApplicationFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereAcceptance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchApplication whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperBranchApplication
 */
class BranchApplication extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_applications';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'branch_id',
        'motivational_letter',
        'acceptance',
    ];

    /** Return the user of this application.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the branch of this application.
     */
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }
}
