<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Entity> $entities
 * @property-read int|null $entities_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType query()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityType whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEntityType
 */
class EntityType extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Returns the entities of this entity type.
     */
    public function entities(): HasMany
    {
        return $this->hasMany(Entity::class);
    }
}
