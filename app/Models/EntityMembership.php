<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $entity_id
 * @property int $entity_role_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity|null $branch
 * @property-read \App\Models\EntityRole|null $role
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\EntityMembershipFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership query()
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereEntityRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntityMembership whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEntityMembership
 */
class EntityMembership extends Pivot
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entity_memberships';

    /**
     * Indicates if the IDs are auto-incrementing.
     * The default in pivot tables is false.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * Return the role of this membership.
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(EntityRole::class);
    }

    /** Return the user of this membership.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** Return the entity of this membership.
     */
    public function branch(): BelongsTo
    {
        return $this->belongsTo(Entity::class);
    }
}
