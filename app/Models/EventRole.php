<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $permissions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\EventMembership> $memberships
 * @property-read int|null $memberships_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EventRole whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEventRole
 */
class EventRole extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'permissions',
    ];

    /**
     * Return the memberships of this role.
     */
    public function memberships(): HasMany
    {
        return $this->hasMany(EventMembership::class);
    }

    /**
     * Return event role id with name 'Organizer'
     */
    public static function organizerId(): int
    {
        return self::firstWhere('name', 'Organizer')->id;
    }

    /**
     * Return branch role id with name 'Participant'
     */
    public static function participantId(): int
    {
        return self::firstWhere('name', 'Participant')->id;
    }

    /**
     * Return branch role id with name 'Helper'
     */
    public static function helperId(): int
    {
        return self::firstWhere('name', 'Helper')->id;
    }
}
