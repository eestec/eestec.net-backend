<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\BranchApplicationEvent;
use App\Mail\NewMemberApplicationToBranch;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToCP implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(BranchApplicationEvent $event): void
    {
        Mail::to($event->branch->email)->queue(new NewMemberApplicationToBranch($event->user, $event->branch));
    }
}
