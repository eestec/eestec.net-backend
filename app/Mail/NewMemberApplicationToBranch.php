<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewMemberApplicationToBranch extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user connected with the application
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * The branch connected with the application
     *
     * @var \App\Models\Branch
     */
    public $branch;

    /**
     * The branch's URL
     *
     * @var string
     */
    public $view_url;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Branch $branch)
    {
        $this->user = $user;
        $this->branch = $branch;
        $this->view_url = config('app.spa_url').'/cities/'.$branch->slug;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this
            ->subject('Incoming member application from '.$this->user->first_name.' '.$this->user->last_name)
            ->markdown('emails.applications.branch_application');
    }
}
