<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\Branch;
use App\Models\BranchType;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BranchApplicationApproval extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The subject of the email.
     *
     * @var string
     */
    public $subject;

    /**
     * The user related to this message.
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * The acceptance of the membership related to the message.
     *
     * @var bool
     */
    public $acceptance;

    /**
     * The branch related to this message.
     *
     * @var \App\Models\Branch
     */
    public $branch;

    /**
     * The branch type of the related branch.
     *
     * @var string
     */
    public $branch_type_name;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Branch $branch, bool $acceptance)
    {
        $this->branch = $branch;
        $this->user = $user;
        $this->acceptance = $acceptance;
        $this->branch_type_name = BranchType::find($branch->branch_type_id)->name;

        if ($this->acceptance) {
            $this->subject = 'Branch Application for '.$this->branch->name.' Approved';
        } else {
            $this->subject = 'Branch Application for '.$this->branch->name.'  Rejected';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->markdown('emails.applications.branch_application_approval');
    }
}
