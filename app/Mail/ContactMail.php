<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The original author of the mail.
     *
     * @var string
     */
    public $sender;

    /**
     * The contents of the message.
     *
     * @var string
     */
    public $content;

    /**
     * The contents of the message.
     *
     * @var \App\Models\Contact
     */
    public $contact;

    /**
     * Create a new message instance.
     */
    public function __construct(Contact $contact, string $sender, string $content)
    {
        $this->sender = $sender;
        $this->content = $content;
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->markdown('emails.contact.new_contact');
    }
}
