<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UpdateStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eestecnet:update-statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update statistics used in the front page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        // Branches
        $type_ids = DB::table('branch_types')->whereNot(function ($query) {
            $query->where('name', 'Terminated');
        })->get()->map(function ($type) {
            return $type->id;
        });

        $branches_query = Branch::whereIn('branch_type_id', $type_ids);
        $branches = $branches_query->count();
        Cache::forever('branches', $branches);

        $countries = $branches_query->distinct()->count('country');
        Cache::forever('countries', $countries);

        $role_ids = DB::table('branch_roles')->whereNot(function ($query) {
            $query->where('name', 'Alumni');
        })->get()->map(function ($role) {
            return $role->id;
        });

        $users = User::with('branch_memberships')->get();
        $active_users = $users->filter(function ($value) use ($role_ids) {
            // User may have more than one branches, and we don't want to double count
            return $value->branch_memberships->contains(function ($value) use ($role_ids) {
                return $role_ids->contains($value->pivot->branch_role_id);
            });
        })->count();
        Cache::forever('users', $active_users);

        return 0;
    }
}
