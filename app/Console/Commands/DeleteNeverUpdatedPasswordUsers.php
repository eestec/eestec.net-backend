<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Fortify\DeleteUser;
use App\Models\User;
use Illuminate\Console\Command;

class DeleteNeverUpdatedPasswordUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eestecnet:delete-not-password-reset-users {days=90}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete users who did not update their password (default 90 days)';

    /**
     * The action that deletes users.
     */
    protected DeleteUser $remover;

    /**
     * Create a new command instance.
     */
    public function __construct(DeleteUser $remover)
    {
        parent::__construct();
        $this->remover = $remover;
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $users = User::with('tokens')
            ->whereNull('last_password_update')
            ->where('updated_at', '<', now()->subDays(intval($this->argument('days'))))
            ->get();

        $count = count($users);
        foreach ($users as $user) {
            $this->remover->delete($user);
        }
        $this->info("Deleted {$count} not password updated users.");

        return 0;
    }
}
