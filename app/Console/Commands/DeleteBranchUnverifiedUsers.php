<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Fortify\DeleteUser;
use App\Models\User;
use Illuminate\Console\Command;

class DeleteBranchUnverifiedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eestecnet:delete-branch-unverified-users {days=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete users whose branch was never confirmed (default 30 days)';

    /**
     * The action that deletes users.
     */
    protected DeleteUser $remover;

    /**
     * Create a new command instance.
     */
    public function __construct(DeleteUser $remover)
    {
        parent::__construct();
        $this->remover = $remover;
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $users = User::with('tokens')
            ->whereNull('branch_verified_at')
            ->where('updated_at', '<', now()->subDays(intval($this->argument('days'))))
            ->get();

        $count = count($users);

        foreach ($users as $user) {
            $this->remover->delete($user);
        }

        $this->info("Deleted {$count} branch unverified users.");

        return 0;
    }
}
