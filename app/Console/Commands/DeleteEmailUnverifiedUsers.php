<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Fortify\DeleteUser;
use App\Models\User;
use Illuminate\Console\Command;

class DeleteEmailUnverifiedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eestecnet:delete-email-unverified-users {days=14}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete users who did not confirm their email (default 14 days)';

    /**
     * The action that deletes users.
     */
    protected DeleteUser $remover;

    /**
     * Create a new command instance.
     */
    public function __construct(DeleteUser $remover)
    {
        parent::__construct();
        $this->remover = $remover;
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $users = User::with('tokens')
            ->whereNull('email_verified_at')
            ->where('created_at', '<', now()->subDays(intval($this->argument('days'))))
            ->get();

        $count = count($users);
        foreach ($users as $user) {
            $this->remover->delete($user);
        }

        $this->info("Deleted {$count} email unverified users.");

        return 0;
    }
}
