<?php

declare(strict_types=1);

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        ResetPassword::createUrlUsing(function ($user, string $token) {
            return config('app.spa_url').'/reset-password?token='.$token.'&email='.$user->email;
        });

        VerifyEmail::createUrlUsing(function ($user) {
            $url = URL::temporarySignedRoute(
                'verification.verify',
                Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
                [
                    'id' => $user->getKey(),
                    'hash' => sha1($user->getEmailForVerification()),
                ]
            );

            return config('app.spa_url').'/verify-email/'.sha1($user->getEmailForVerification()).'?'.explode('?', $url)[1];
        });

        // Change how the url sent to the user looks like.
        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            $message = (new MailMessage)
                ->subject(Lang::get('Verify Email Address'));

            // This is not the fist verify email the user receives
            if ($notifiable->updated_at != $notifiable->created_at) {
                return $message
                    ->line(Lang::get('An email change was requested.'))
                    ->line(Lang::get('In order for it to be finalized, please click the button below.'))
                    ->action(Lang::get('Verify Email Address'), $url)
                    ->line(Lang::get('If you have not requested this change, or the account does not belong to you, no further action is required.'));
            }

            // Send this line on the mail only when a user is created
            return $message
                ->line(Lang::get('Please click the button below to verify your email address.'))
                ->action(Lang::get('Verify Email Address'), $url)
                ->line(Lang::get('If you did not create an account, no further action is required.'));
        });
    }
}
