<?php

declare(strict_types=1);

namespace App\Providers;

use App\Actions\Fortify\DeleteUser;
use App\Contracts\Actions\DeletesUser;
use Illuminate\Support\ServiceProvider;

class ActionServiceProvider extends ServiceProvider
{
    public array $bindings = [
        DeletesUser::class => DeleteUser::class,
    ];
}
