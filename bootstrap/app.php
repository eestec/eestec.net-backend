<?php

declare(strict_types=1);

use App\Http\Middleware\AlwaysAcceptJsonResponses;
use App\Http\Middleware\EnsureBranchIsVerified;
use App\Http\Middleware\ReturnIfAuthenticated;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
        then: function () {
            Route::middleware('api')
                ->prefix(config('sanctum.prefix', 'sanctum'))
                ->group(base_path('routes/sanctum.php'));
        },
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->statefulApi();
        //        $middleware->throttleApi();

        $middleware->prepend(AlwaysAcceptJsonResponses::class);

        $middleware->alias([
            'email.verified' => EnsureEmailIsVerified::class,
            'branch.verified' => EnsureBranchIsVerified::class,
            'guest' => ReturnIfAuthenticated::class,

        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
