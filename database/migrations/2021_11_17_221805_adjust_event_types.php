<?php

declare(strict_types=1);

use App\actions\File\CSV;
use App\Models\Event;
use App\Models\EventType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            CSV::process('assets/events/events_to_be_changed.csv', function ($data) {
                $event = Event::firstWhere('slug', $data[0]);
                $event_type = EventType::firstWhere('name', $data[1]);
                $event->event_type()->associate($event_type);
                $event->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
