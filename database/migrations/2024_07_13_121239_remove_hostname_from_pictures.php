<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\Event;
use App\Models\Partner;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $branches = Branch::all();
        foreach ($branches as $b) {
            if ($b->profile_picture_path != null) {
                $b->profile_picture_path = explode('/', parse_url($b->profile_picture_path, PHP_URL_PATH), 3)[2];
                $b->large_profile_picture_path = explode('/', parse_url($b->large_profile_picture_path, PHP_URL_PATH), 3)[2];
                $b->save();
            }
        }

        $users = User::all();
        foreach ($users as $u) {
            if ($u->profile_photo_path != null) {
                $u->profile_photo_path = explode('/', parse_url($u->profile_photo_path, PHP_URL_PATH), 3)[2];
            }

            if ($u->banner_path != null) {
                $u->banner_path = explode('/', parse_url($u->banner_path, PHP_URL_PATH), 3)[2];
            }

            $u->save();
        }

        $events = Event::all();
        foreach ($events as $e) {
            if ($e->profile_picture_path != null) {
                $e->profile_picture_path = explode('/', parse_url($e->profile_picture_path, PHP_URL_PATH), 3)[2];
            }

            if ($e->banner_path != null) {
                $e->banner_path = explode('/', parse_url($e->banner_path, PHP_URL_PATH), 3)[2];
            }

            $e->save();
        }

        $partners = Partner::all();
        foreach ($partners as $p) {
            $p->logo = explode('/', parse_url($p->logo, PHP_URL_PATH), 3)[2];
            $p->save();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $branches = Branch::all();
        foreach ($branches as $b) {
            if ($b->profile_picture_path != null) {
                $b->profile_picture_path = url($b->profile_picture_path);
                $b->large_profile_picture_path = url($b->large_profile_picture_path);
                $b->save();
            }
        }

        $users = User::all();
        foreach ($users as $u) {
            if ($u->profile_photo_path != null) {
                $u->profile_photo_path = url($u->profile_photo_path);
            }

            if ($u->banner_path != null) {
                $u->banner_path = url($u->banner_path);
            }

            $u->save();
        }

        $events = Event::all();
        foreach ($events as $e) {
            if ($e->profile_picture_path != null) {
                $e->profile_picture_path = url($e->profile_picture_path);
            }

            if ($e->banner_path != null) {
                $e->banner_path = url($e->banner_path);
            }

            $e->save();
        }

        $partners = Partner::all();
        foreach ($partners as $p) {
            $p->logo = url($p->logo);
            $p->save();
        }
    }
};
