<?php

declare(strict_types=1);

use App\Actions\File\Image;
use App\Models\User;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            $users = DB::table('account_eestecer')->get();

            $output = new ConsoleOutput;
            $progress = new ProgressBar($output, count($users));
            $progress->start();

            // Create a temporary directory to clean up photos
            Storage::makeDirectory('public/images/profile_photos');

            foreach ($users as $user) {
                if ($user->is_superuser) {
                    $role = DB::table('roles')->where('name', 'Administrator')->first();
                } else {
                    $role = DB::table('roles')->where('name', 'User')->first();
                }

                $user_slug = SlugService::createSlug(User::class, 'slug', $user->first_name.' '.$user->last_name);

                // Make sure that all time calls have the same value
                $now = time();

                // If the images are on HEIC format, change them to JPEG
                $extension = File::extension($user->thumbnail);
                if (File::extension($user->thumbnail) == 'HEIC') {
                    $extension = 'jpg';
                }
                $new_img_location = 'public/images/users/profile_photos/'.$user_slug.'-'.$now.'.'.$extension;
                $tmp_img_location = 'public/images/profile_photos/'.$user_slug.'-'.$now.'.'.$extension;
                $tmp_img_large_location = 'public/images/profile_photos/'.$user_slug.'-'.$now.'_large'.'.'.$extension;

                $exists = $user->thumbnail != '' && $user->thumbnail != null && Storage::exists('public/images/'.$user->thumbnail);

                if ($exists) {
                    // Store both the original and a resized image
                    Storage::copy('public/images/'.$user->thumbnail, $tmp_img_large_location);
                    if (File::extension($user->thumbnail) == 'HEIC') {
                        Image::convert('public/images/'.$user->thumbnail, $tmp_img_large_location);
                    }
                    try {
                        Image::resize('public/images/'.$user->thumbnail, $tmp_img_location);
                    } catch (Exception $e) {
                        dd('Error copying image '.$user->thumbnail.' with exception '.$e);
                    }
                }

                // Merge first and middle name
                $user_name = ucfirst($user->first_name);
                if ($user->middle_name != null) {
                    $user_name = $user_name.' '.ucfirst($user->middle_name);
                }

                DB::table('users')->insert([
                    'first_name' => $user_name,
                    'last_name' => ucfirst($user->last_name),
                    'slug' => $user_slug,
                    'email' => $user->email,
                    'gender' => $user->gender,
                    'passport_number' => $user->passport_number,
                    'phone_number' => $user->mobile,
                    'allergies' => $user->allergies,
                    't_shirt_size' => $user->tshirt_size,
                    'dietary_preferences' => $user->food_preferences,
                    'email_verified_at' => Carbon\Carbon::now(),
                    'branch_verified_at' => null,
                    'password' => Hash::make(Str::random(8)),
                    'profile_photo_path' => $exists ? Storage::url($new_img_location) : null,
                    'role_id' => $role->id,
                    'created_at' => $user->date_joined,
                    'updated_at' => Carbon\Carbon::now(),
                ]);
                $progress->advance();
            }
            $progress->finish();

            // Delete the old directory and rename the new one
            Storage::deleteDirectory('public/images/users');
            Storage::makeDirectory('public/images/users');
            Storage::makeDirectory('public/images/users/banners');
            if (config('filesystems.default') == 'production') {
                rename('data/public/images/profile_photos', 'data/public/images/users/profile_photos');
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
