<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            // User achievements
            Schema::dropIfExists('account_achievement');

            // Connect permissions with user
            Schema::dropIfExists('account_eestecer_user_permissions');
            Schema::dropIfExists('auth_group_permissions');
            Schema::dropIfExists('account_position');

            // Permissions for different actions
            Schema::dropIfExists('auth_permission');

            // Celery
            Schema::dropIfExists('celery_taskmeta');
            Schema::dropIfExists('celery_tasksetmeta');

            // CORS related
            Schema::dropIfExists('corsheaders_corsmodel');

            // Reversion
            Schema::dropIfExists('reversion_version');
            Schema::dropIfExists('reversion_revision');

            // Django
            Schema::dropIfExists('django_admin_log');
            Schema::dropIfExists('django_content_type');
            Schema::dropIfExists('django_migrations');
            Schema::dropIfExists('django_session');

            // Django Celery
            Schema::dropIfExists('djcelery_periodictask');
            Schema::dropIfExists('djcelery_intervalschedule');
            Schema::dropIfExists('djcelery_crontabschedule');
            Schema::dropIfExists('djcelery_periodictasks');
            Schema::dropIfExists('djcelery_taskstate');
            Schema::dropIfExists('djcelery_workerstate');

            // Django kombu?
            Schema::dropIfExists('djkombu_message');
            Schema::dropIfExists('djkombu_queue');

            // Feedback
            Schema::table('events_participation', function (Blueprint $table) {
                $table->dropColumn(['feedback_id']);
            });

            Schema::table('events_application', function (Blueprint $table) {
                $table->dropColumn(['questionaire_id']);
            });

            Schema::dropIfExists('feedback_answer');
            Schema::dropIfExists('feedback_answerset');
            Schema::dropIfExists('feedback_question');

            Schema::dropIfExists('feedback_questionset_parents');

            Schema::table('events_event', function (Blueprint $table) {
                $table->dropColumn(['feedbacksheet_id', 'questionaire_id']);
            });
            Schema::dropIfExists('feedback_questionset');

            // Mailqueue
            Schema::dropIfExists('mailqueue_attachment');
            Schema::dropIfExists('mailqueue_mailermessage');

            // News
            Schema::dropIfExists('news_entry_author');
            Schema::dropIfExists('news_entry');

            // Pages?
            Schema::dropIfExists('pages_page');
            Schema::dropIfExists('pages_stub');
            Schema::dropIfExists('pages_websitefeedbackimage');
            Schema::dropIfExists('pages_websitefeedback');

            // Wiki
            Schema::dropIfExists('wiki_externallink');
            Schema::dropIfExists('wiki_reference');
            Schema::dropIfExists('wiki_wikipage');

            // Achievements
            Schema::dropIfExists('account_achievements');

            // Thumbnails
            Schema::dropIfExists('thumbnail_kvstore');

            // TODO: What kind of information does this hold?
            Schema::dropIfExists('account_eestecer_groups');
            Schema::dropIfExists('auth_group');

            // TODO: Does this hold membership information?
            Schema::dropIfExists('news_membership');

            // TODO: Team's board - what's that?
            Schema::dropIfExists('teams_board');

            Schema::dropIfExists('events_eventimage');
            Schema::dropIfExists('teams_memberimage');

            // Rest
            Schema::dropIfExists('events_event_organizing_committee');
            Schema::dropIfExists('teams_team');
            Schema::dropIfExists('events_participation');
            Schema::dropIfExists('events_event_organizing_committee');
            Schema::dropIfExists('events_transportation');
            Schema::dropIfExists('events_application');
            Schema::dropIfExists('events_event_organizers');
            Schema::dropIfExists('events_event');
            Schema::dropIfExists('account_eestecer');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
