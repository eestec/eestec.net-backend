<?php

declare(strict_types=1);

use App\Models\BranchType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('branch_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        if (App::environment('production')) {
            BranchType::create([
                'name' => 'Observer',
            ]);
            BranchType::create([
                'name' => 'Junior Local Committee',
            ]);
            BranchType::create([
                'name' => 'Local Committee',
            ]);
            BranchType::create([
                'name' => 'Terminated',
            ]);
            BranchType::create([
                'name' => 'Suspended',
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('branch_types');
    }
};
