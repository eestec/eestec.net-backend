<?php

declare(strict_types=1);

use App\Models\BranchType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('slug')->nullable();
            $table->string('email')->unique();
            $table->integer('founded_in')->nullable();
            $table->string('website')->nullable();
            $table->string('country')->nullable();
            $table->smallInteger('region')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('profile_picture_path', 2048)->nullable();
            $table->string('large_profile_picture_path', 2048)->nullable();
            $table->foreignIdFor(BranchType::class)->nullable()->constrained()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('branches');
    }
};
