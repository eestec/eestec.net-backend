<?php

declare(strict_types=1);

use App\Models\BoardPosition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('board_positions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        if (App::environment('production')) {
            BoardPosition::create([
                'name' => 'Chairperson',
                'description' => "Responsible for:\n- the operational work of the Board,\n- Board Meetings,\n- Congresses.",
            ]);
            BoardPosition::create([
                'name' => 'Treasurer',
                'description' => "Responsible for:\n- account keeping and financial reports,\n- the Board's access to the Association's bank accounts\n- maintaining and updating the legal documents of the Association.",
            ]);
            BoardPosition::create([
                'name' => 'Vice Chairperson for Internal Affairs',
                'description' => "Responsible for:\n- coordinating the work of Contact Persons,\n- the activation and motivation of internationally active members of branches,\n- the activation, education and motivation of all levels of EESTEC branches,\n- cooperations within the network of EESTEC.",

            ]);
            BoardPosition::create([
                'name' => 'Vice Chairperson for External Affairs',
                'description' => "Responsible for:\n- initiation of EESTEC branches,\n- contact with other NGOs,\n- contacts with universities,\n- company contacts and fundraising,\n- public relations.",
            ]);
            BoardPosition::create([
                'name' => 'Vice Chairperson for Administrative Affairs',
                'description' => "Responsible for:\n- digital and physical documentation preservation and transfer,\n- data collection and evaluation,\n- status reports,\n- ensuring internal compliance with internal regulations and legal documents,\n- quality, validity and effective conduct of EESTEC Events.",
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('board_position');
    }
};
