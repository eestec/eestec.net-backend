<?php

declare(strict_types=1);

use App\Models\EventType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('slug')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->datetime('application_deadline');
            $table->integer('max_participants');
            $table->integer('participation_fee');
            $table->string('location')->nullable();
            $table->text('description');
            $table->foreignIdFor(EventType::class)->nullable()->constrained()->nullOnDelete();
            $table->string('profile_picture_path', 2048)->nullable();
            $table->string('banner_path', 2048)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
