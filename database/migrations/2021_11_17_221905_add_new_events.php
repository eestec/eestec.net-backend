<?php

declare(strict_types=1);

use App\Actions\File\CSV;
use App\Actions\File\Image;
use App\Models\Branch;
use App\Models\Event;
use App\Models\EventType;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            CSV::process('assets/events/new_events_data.csv', function ($data) {
                // All images are .jpg, so we simplify the code a bit.
                $images = glob('assets/events/new_events/*');
                $img_names = Arr::map($images, fn ($image) => basename($image));

                $slug = SlugService::createSlug(Event::class, 'slug', $data[1]);
                $now = time();
                $new_img_location = 'public/images/events/profile_pictures/'.$slug.'-'.$now.'.jpg';
                $new_large_img_location = 'public/images/events/profile_pictures/'.$slug.'-'.$now.'_large'.'.jpg';

                $img_idx = array_search(str_replace(':', '', $data[1]).'.jpg', $img_names);

                if ($img_idx !== false) {
                    Storage::put($new_large_img_location, File::get($images[$img_idx]));
                    try {
                        Image::resize($new_large_img_location, $new_img_location);
                    } catch (Exception $e) {
                        dd('Exception: '.$e);
                    }
                }

                $event_type = EventType::firstWhere('name', $data[2]);
                $event = Event::create([
                    'name' => $data[1],
                    'start_date' => $data[3],
                    'end_date' => $data[4],
                    'application_deadline' => $data[5],
                    'max_participants' => $data[6],
                    'participation_fee' => $data[7],
                    'description' => $data[8],
                    'event_type_id' => $event_type->id,
                    'profile_picture_path' => $img_idx !== false ? Storage::url($new_img_location) : null,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
                $branch = Branch::firstWhere('name', $data[0]);
                $event->branches()->attach($branch);
                $event->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
