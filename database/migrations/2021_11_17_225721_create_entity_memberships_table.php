<?php

declare(strict_types=1);

use App\Models\Entity;
use App\Models\EntityRole;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entity_memberships', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(Entity::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(EntityRole::class)->constrained()->cascadeOnDelete();
            $table->timestamps();
            $table->unique(['user_id', 'entity_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_memberships');
    }
};
