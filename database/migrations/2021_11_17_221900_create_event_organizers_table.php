<?php

declare(strict_types=1);

use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event_organizers', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Event::class)->constrained()->cascadeOnDelete();
            $table->morphs('event_organizer');
            $table->timestamps();
            $table->unique(['event_id', 'event_organizer_id', 'event_organizer_type'], 'unique_organizer');
        });

        if (App::environment('production')) {
            $events_organizers = DB::table('events_event_organizing_committee')->get();
            foreach ($events_organizers as $ev_org) {
                $old_event = DB::table('events_event')->where('id', $ev_org->event_id)->first();
                $new_event = Event::where('name', trim($old_event->name))->first();
                $team = DB::table('teams_team')->where('id', '=', $ev_org->team_id)->first();
                if (
                    $team->category == 'lc' ||
                    $team->category == 'jlc' ||
                    $team->category == 'observer'
                ) {
                    // it's a branch
                    $branch = DB::table('branches')->where('name', '=', $team->name)->first();
                    $new_event->branches()->attach($branch->id);
                } elseif (
                    $team->category == 'team' ||
                    $team->category == 'body'
                ) {
                    // it's not a branch
                    $entity = DB::table('entities')->where('name', '=', $team->name)->first();
                    $new_event->entities()->attach($entity->id);
                } else {
                    continue;
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event_organizers');
    }
};
