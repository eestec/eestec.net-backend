<?php

declare(strict_types=1);

use App\Actions\File\CSV;
use App\Models\Branch;
use App\Models\BranchType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            CSV::process('assets/branches/branches_to_be_migrated.csv', function ($data) {
                $branch = Branch::firstWhere('slug', $data[0]);
                $branch_type = BranchType::firstWhere('name', $data[1]);
                $branch->branch_type()->associate($branch_type);
                $branch->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
