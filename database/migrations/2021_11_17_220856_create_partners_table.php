<?php

declare(strict_types=1);

use App\Models\Partner;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('description');
            $table->string('logo', 2048)->nullable();
            $table->timestamps();
        });

        if (App::environment('production')) {
            Storage::makeDirectory('public/images/partners');
            $files = glob('assets/images/partners/*');
            foreach ($files as $file) {
                Storage::put('public/images/partners/'.basename($file), File::get($file));
            }

            $partners = [
                [
                    'name' => 'Eurosender',
                    'description' => 'Eurosender is a modern digital platform that '.
                        'connects shippers and logistics companies worldwide. '.
                        'Their centralised solution caters to every logistics requirement, '.
                        'providing access to the best transporters at discounted rates. '.
                        'They empower businesses to expand their reach and take full control '.
                        'of their logistics processes.',
                ],
                [
                    'name' => 'EDUopinions',
                    'description' => 'EDUopinions is a student centered platform with real reviews from students to '.
                        'help other students make a well-informed decision about their future. They encourage '.
                        'students and alumni from schools all over the world to write about their experiences '.
                        'for the people following in their footsteps.\n'.
                        'It also constitutes a valuable tool for universities. ',
                ],
                [
                    'name' => 'Swisscare',
                    'description' => 'Swiss Care is an international health and travel insurance '.
                        'for expats, tourists, globetrotters, professionals and students. '.
                        'It provides an easy and quick service that combines relevant but useful benefits. ',
                ],
                [
                    'name' => 'IAESTE',
                    'description' => 'IAESTE, the International Association for the Exchange of Students '.
                        'for Technical Experience, is a non-profit association of national committees '.
                        'representing academic, industrial and student interests. They serve '.
                        '3500+ students, 3000 employers and 1000 academic institutions through '.
                        'career-focused professional internships abroad, social and intercultural '.
                        'reception programmes, international networking and other career and employer '.
                        'branding activities in more than 80 countries worldwide.',
                ],
                [
                    'name' => 'BEST',
                    'description' => 'BEST, Board of European Students of Technology is a non-profit and non-political '.
                        'organisation founded in 1989 that provides communication, co-operation and exchange '.
                        'possibilities for students all over Europe. 90 Local BEST Groups (LBGs) in 32 countries '.
                        'are creating a growing, well organised, powerful, young and innovative student network.\n'.
                        'BEST strives to help European students of technology to become more internationally minded, '.
                        'by reaching a better understanding of European cultures and developing capacities to work '.
                        'on an international basis.',

                ],
                [
                    'name' => 'ESTIEM',
                    'description' => 'ESTIEM is a non-profit organisation for Industrial Engineering and Management '.
                        'students who combine technological understanding with management skills. Founded in '.
                        '1990, their goal is to foster relations between IEM students and support their '.
                        'development. They strive for students to have a connection not only with other '.
                        'students all over Europe of the same field but also to other companies and universities, '.
                        'to ensure each student as a voice as well as the opportunity to make a difference. ',
                ],
                [
                    'name' => 'Metafox',
                    'description' => 'MetaFox is a coaching tool catered to facilitators, '.
                        'teachers and trainers of all sorts that wish to empower people. ',
                ],
                [
                    'name' => 'IFISO',
                    'description' => 'IFISO is the ‘Informal Forum for International Student Organisations’, '.
                        'a non-political and non-profit forum for various student-run organizations. '.
                        'Currently it consists of 27 student organizations, covering more than two '.
                        'million students all together in different fields of study.',
                ],
                [
                    'name' => 'Infineon',
                    'description' => 'Infineon is a German semiconductor manufacturer founded in 1999, when '.
                        'the semiconductor operations of the former parent company Siemens AG were spun off. '.
                        'Infineon has about 50,280 employees and is one of the ten largest semiconductor '.
                        'manufacturers worldwide.',
                ],

            ];

            foreach ($partners as $partner) {
                $slug = SlugService::createSlug(Partner::class, 'slug', $partner['name']);
                Partner::create([
                    'name' => $partner['name'],
                    'description' => $partner['description'],
                    'logo' => Storage::url('public/images/partners/'.$slug.'.png'),
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('partners');
    }
};
