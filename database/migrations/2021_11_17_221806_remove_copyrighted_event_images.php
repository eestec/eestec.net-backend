<?php

declare(strict_types=1);

use App\Actions\File\CSV;
use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            CSV::process('assets/events/event_images_to_be_removed.csv', function ($data) {
                $profilePicturePath = 'public/images/events/profile_pictures/';

                $event = Event::firstWhere('name', $data[0]);
                $filename = basename($event->profile_picture_path);
                if (Storage::exists($profilePicturePath.$filename)) {
                    Storage::delete($profilePicturePath.$filename);

                    // Delete the large image as well
                    ['filename' => $img_filename, 'extension' => $img_extension] = pathinfo($filename);
                    Storage::delete($profilePicturePath.$img_filename.'_large'.'.'.$img_extension);
                }

                $event->fill([
                    'profile_picture_path' => null,
                ])->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
