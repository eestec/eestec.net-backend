<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            $memberships = DB::table('news_membership')->get();

            $output = new ConsoleOutput;
            $progress = new ProgressBar($output, count($memberships));
            $progress->start();

            foreach ($memberships as $m) {
                $entity_or_branch = DB::table('teams_team')->where('id', $m->team_id)->first();
                $old_user = DB::table('account_eestecer')->where('id', $m->user_id)->first();
                $user = User::where('email', $old_user->email)->first();

                if (
                    $entity_or_branch->category == 'lc' ||
                    $entity_or_branch->category == 'jlc' ||
                    $entity_or_branch->category == 'observer'
                ) {
                    if ($m->board) {
                        $role = DB::table('branch_roles')->where('name', 'Board')->first();
                    } elseif ($m->alumni) {
                        $role = DB::table('branch_roles')->where('name', 'Alumni')->first();
                    } else {
                        $role = DB::table('branch_roles')->where('name', 'Member')->first();
                    }
                    $branch = DB::table('branches')->where('name', $entity_or_branch->name)->first();
                    DB::table('branch_memberships')->insert([
                        'user_id' => $user->id,
                        'branch_id' => $branch->id,
                        'branch_role_id' => $role->id,
                        'created_at' => $m->date_joined,
                        'updated_at' => Carbon\Carbon::now(),
                    ]);

                    // Verify user when we actually find a branch they belong to
                    $user->forceFill(['branch_verified_at' => now()])->save();
                } elseif (
                    $entity_or_branch->category == 'body' ||
                    $entity_or_branch->category == 'team'
                ) {
                    if ($m->board) {
                        $role = DB::table('entity_roles')->where('name', 'Board')->first();
                    } elseif ($m->alumni) {
                        $role = DB::table('entity_roles')->where('name', 'Alumni')->first();
                    } else {
                        $role = DB::table('entity_roles')->where('name', 'Member')->first();
                    }
                    $entity = DB::table('entities')->where('name', $entity_or_branch->name)->first();
                    DB::table('entity_memberships')->insert([
                        'user_id' => $user->id,
                        'entity_id' => $entity->id,
                        'entity_role_id' => $role->id,
                        'created_at' => $m->date_joined,
                        'updated_at' => Carbon\Carbon::now(),
                    ]);
                }
                $progress->advance();
            }
            $progress->finish();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
