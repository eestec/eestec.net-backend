<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            $organizers = DB::table('events_event_organizers')->get();
            $organizer_role = DB::table('event_roles')->where('name', 'Organizer')->first();
            foreach ($organizers as $organizer) {
                $old_user = DB::table('account_eestecer')->where('id', $organizer->eestecer_id)->first();
                $new_user = DB::table('users')->where('email', $old_user->email)->first();
                $old_event = DB::table('events_event')->where('id', $organizer->event_id)->first();
                $new_event = DB::table('events')->where('slug', $old_event->slug)->first();
                DB::table('event_memberships')->insert([
                    'user_id' => $new_user->id,
                    'event_id' => $new_event->id,
                    'event_role_id' => $organizer_role->id,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ]);
            }

            $participants = DB::table('events_participation')->get();
            $participant_role = DB::table('event_roles')->where('name', 'Participant')->get()->first();

            $output = new ConsoleOutput;
            $progress = new ProgressBar($output, count($participants));
            $progress->start();

            foreach ($participants as $pax) {
                $old_user = DB::table('account_eestecer')->where('id', $pax->participant_id)->first();
                $new_user = DB::table('users')->where('email', $old_user->email)->first();
                $old_event = DB::table('events_event')->where('id', $pax->target_id)->first();
                $new_event = DB::table('events')->where('slug', $old_event->slug)->first();
                $exists = DB::table('event_memberships')->where([
                    ['user_id', $new_user->id],
                    ['event_id', $new_event->id],
                    ['event_role_id', $participant_role->id],
                ])->first();
                if ($exists) {
                    $progress->advance();

                    continue;
                }

                DB::table('event_memberships')->insert([
                    'user_id' => $new_user->id,
                    'event_id' => $new_event->id,
                    'event_role_id' => $participant_role->id,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ]);
                $progress->advance();
            }
            $progress->finish();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event_memberships');
    }
};
