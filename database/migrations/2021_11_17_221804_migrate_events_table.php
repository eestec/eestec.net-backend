<?php

declare(strict_types=1);

use App\Actions\File\Image;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            Storage::makeDirectory('public/images/events/profile_pictures');
            Storage::makeDirectory('public/images/events/banners');

            $events = DB::table('events_event')->get();

            $output = new ConsoleOutput;
            $progress = new ProgressBar($output, count($events));
            $progress->start();

            foreach ($events as $event) {
                if (
                    $event->category == 'recruitment' ||
                    $event->category == 'training' ||
                    $event->category == 'project'
                ) {
                    // Remove recruitment, projects & trainings so that we can easier manipulate other tables later
                    DB::table('events_event_organizing_committee')->where('event_id', '=', $event->id)->delete();
                    DB::table('events_event_organizers')->where('event_id', '=', $event->id)->delete();
                    DB::table('events_application')->where('target_id', '=', $event->id)->delete();
                    DB::table('events_participation')->where('target_id', '=', $event->id)->delete();
                    DB::table('events_eventimage')->where('property_id', '=', $event->id)->delete();
                    DB::table('events_event')->delete($event->id);
                } else {
                    if ($event->category == 'imw') {
                        $type = DB::table('event_types')->where('name', 'International Motivational Weekend')->first();
                    } elseif ($event->category == 'operational') {
                        $type = DB::table('event_types')->where('name', 'Operational Event')->first();
                    } else {
                        $type = DB::table('event_types')->where('name', ucfirst($event->category))->get()->first();
                    }

                    $max_pax = $event->max_participants;
                    if ($max_pax == '') {
                        $max_pax = 1000;
                    }

                    $app_ddl = $event->deadline;
                    if ($app_ddl == '') {
                        $app_ddl = $event->start_date;
                    }

                    // Consistent timestamps
                    $now = time();
                    $new_img_location = 'public/images/events/profile_pictures/'.$event->slug.'-'.$now.'.'.File::extension($event->thumbnail);
                    $new_large_img_location = 'public/images/events/profile_pictures/'.$event->slug.'-'.$now.'_large'.'.'.File::extension($event->thumbnail);

                    $exists = Storage::exists('public/images/'.$event->thumbnail);
                    if ($exists) {
                        Storage::copy('public/images/'.$event->thumbnail, $new_large_img_location);
                        try {
                            Image::resize('public/images/'.$event->thumbnail, $new_img_location);
                        } catch (Exception $e) {
                            dd('Exception: '.$e);
                        }
                    }

                    DB::table('events')->insert([
                        'name' => trim($event->name),
                        'slug' => $event->slug,
                        'start_date' => $event->start_date,
                        'end_date' => $event->end_date,
                        'application_deadline' => $app_ddl,
                        'max_participants' => $max_pax,
                        'participation_fee' => $event->participation_fee,
                        'location' => $event->location,
                        'description' => strip_tags($event->description),
                        'event_type_id' => $type->id,
                        'profile_picture_path' => $exists ? Storage::url($new_img_location) : null,
                        'created_at' => Carbon\Carbon::now(),
                        'updated_at' => Carbon\Carbon::now(),
                    ]);
                }
                $progress->advance();
            }
            $progress->finish();

            Storage::deleteDirectory('public/images/event_thumbnails');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
