<?php

declare(strict_types=1);

use App\Actions\File\CSV;
use App\Models\Branch;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            CSV::process('assets/branches/branch_country_region.csv', function ($data) {
                $branch = Branch::firstWhere('name', $data[1]);
                $branch->fill([
                    'country' => $data[0],
                    'region' => $data[2] !== '' ? $data[2] : null,
                ])->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
