<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('branch_roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('permissions');
            $table->timestamps();
        });

        if (App::environment('production')) {
            DB::table('branch_roles')->insert([
                [
                    'name' => 'Board',
                    'permissions' => 'Administrator',
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ],
                [
                    'name' => 'Member',
                    'permissions' => 'User',
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ],
                [
                    'name' => 'Alumni',
                    'permissions' => 'User',
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('branch_roles');
    }
};
