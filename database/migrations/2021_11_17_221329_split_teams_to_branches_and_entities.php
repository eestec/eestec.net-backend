<?php

declare(strict_types=1);

use App\Actions\File\Image;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (App::environment('production')) {
            $teams = DB::table('teams_team')->get();
            Storage::makeDirectory('public/images/branches/profile_pictures');
            Storage::makeDirectory('public/images/entities/profile_pictures');

            foreach ($teams as $team) {
                if (
                    $team->category == 'lc' ||
                    $team->category == 'jlc' ||
                    $team->category == 'observer'
                ) {
                    $now = time();
                    $new_img_location = 'public/images/branches/profile_pictures/'.$team->slug.'-'.$now.'.'.File::extension($team->thumbnail);
                    $new_large_img_location = 'public/images/branches/profile_pictures/'.$team->slug.'-'.$now.'_large.'.File::extension($team->thumbnail);

                    $exists = $team->thumbnail != '' && $team->thumbnail != null && Storage::exists('public/images/'.$team->thumbnail);
                    if ($exists) {
                        // New images are all jpg.
                        $new_image_exists = file_exists('assets/branches/new_pictures/'.$team->slug.'.jpg');
                        if ($new_image_exists) {
                            // Move the image from assets
                            Storage::put($new_img_location, File::get('assets/branches/new_pictures/'.$team->slug.'.jpg'));
                        } else {
                            Storage::copy('public/images/'.$team->thumbnail, $new_img_location);
                        }
                        // Create the large image (might upscale)
                        Image::resize($new_img_location, $new_large_img_location, 2000, 2);

                        // Create the small image (might downscale)
                        Image::resize($new_img_location, $new_img_location, 500, 2);
                    }

                    // Skip creating LC Waikiki
                    if ($team->slug == 'waikiki') {
                        continue;
                    }

                    if ($team->category == 'lc') {
                        $branch_type = DB::table('branch_types')->where('name', 'Local Committee')->first();
                    } elseif ($team->category == 'jlc') {
                        $branch_type = DB::table('branch_types')->where('name', 'Junior Local Committee')->first();
                    } else {
                        $branch_type = DB::table('branch_types')->where('name', 'Observer')->first();
                    }

                    DB::table('branches')->insert([
                        'name' => $team->name,
                        'slug' => $team->slug,
                        'email' => $team->slug.'@eestec.net',
                        'founded_in' => $team->founded,
                        'website' => $team->website,
                        'facebook' => $team->facebook,
                        'address' => $team->address,
                        'description' => strip_tags($team->description),
                        'profile_picture_path' => $exists ? Storage::url($new_img_location) : null,
                        'large_profile_picture_path' => $exists ? Storage::url($new_large_img_location) : null,
                        'branch_type_id' => $branch_type->id,
                        'created_at' => Carbon\Carbon::now(),
                        'updated_at' => Carbon\Carbon::now(),
                    ]);
                } elseif (
                    $team->category == 'team' ||
                    $team->category == 'body'
                ) {
                    if ($team->category == 'team') {
                        $entity_type = DB::table('entity_types')->where('name', 'Team')->first();
                    } elseif ($team->category == 'body') {
                        $entity_type = DB::table('entity_types')->where('name', 'Body')->first();
                    }
                    $new_img_location = 'public/images/entities/profile_pictures/'.$team->slug.'-'.time().'.'.File::extension($team->thumbnail);

                    $exists = $team->thumbnail != '' && $team->thumbnail != null && Storage::exists('public/images/'.$team->thumbnail);
                    if ($exists) {
                        Storage::copy('public/images/'.$team->thumbnail, $new_img_location);
                    }

                    DB::table('entities')->insert([
                        'name' => $team->name,
                        'slug' => $team->slug,
                        'description' => strip_tags($team->description),
                        'profile_picture_path' => $exists ? Storage::url($new_img_location) : null,
                        'entity_type_id' => $entity_type->id,
                        'created_at' => Carbon\Carbon::now(),
                        'updated_at' => Carbon\Carbon::now(),
                    ]);
                } elseif ($team->category == 'department') {
                    // DO NOTHING
                } else {
                    dd($team->category);
                }
            }
            Storage::deleteDirectory('public/images/memberthumbs');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
