<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Storage::makeDirectory('public/images/logo');
        $files = glob('assets/images/logo/*');
        foreach ($files as $file) {
            Storage::put('public/images/logo/'.basename($file), File::get($file));
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
