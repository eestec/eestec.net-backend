<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Branch;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchMembershipFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $branch = Branch::all()->random();
        $role = BranchRole::all()->random();

        return [
            'user_id' => $user,
            'branch_id' => $branch,
            'branch_role_id' => $role,
        ];
    }
}
