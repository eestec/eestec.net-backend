<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Entity;
use App\Models\EntityRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntityMembershipFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $entity = Entity::all()->random();
        $role = EntityRole::all()->random();

        return [
            'user_id' => $user,
            'entity_id' => $entity,
            'entity_role_id' => $role,
        ];
    }
}
