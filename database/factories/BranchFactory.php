<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Branch;
use App\Models\BranchType;
use Illuminate\Database\Eloquent\Factories\Factory;

class BranchFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Branch::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->city(),
            'email' => $this->faker->safeEmail(),
            'founded_in' => $this->faker->unixTime($max = 'now'),
            'website' => 'https://eestec.net/',
            'country' => $this->faker->country(),
            'region' => $this->faker->numberBetween(1, 5),
            'facebook' => $this->faker->url(),
            'instagram' => $this->faker->url(),
            'linkedin' => $this->faker->url(),
            'address' => $this->faker->address(),
            'description' => $this->faker->text(),
            'profile_picture_path' => $this->faker->imageUrl(500, 500, 'cats'),
            'large_profile_picture_path' => $this->faker->imageUrl(2000, 2000, 'cats'),
            'branch_type_id' => BranchType::all()->random(),
        ];
    }
}
