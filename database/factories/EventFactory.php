<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Event;
use App\Models\EventType;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $end_date = $this->faker->date();

        return [
            'name' => $this->faker->words(3, true),
            'end_date' => $end_date,
            'start_date' => $this->faker->date('Y-m-d', $end_date),
            'application_deadline' => $this->faker->dateTime(), //This could become more elaborate
            'max_participants' => $this->faker->numberBetween(10, 16),
            'participation_fee' => $this->faker->numberBetween(0, 100),
            'location' => $this->faker->city(),
            'description' => $this->faker->text(300),
            'profile_picture_path' => $this->faker->imageUrl(800, 800, 'dogs'),
            'banner_path' => $this->faker->imageUrl(1328, 332, 'dogs'),
            'event_type_id' => EventType::all()->random(),

        ];
    }
}
