<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Entity;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EntityApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $entity = Entity::all()->random();

        return [
            'user_id' => $user,
            'entity_id' => $entity,
            'motivational_letter' => $this->faker->paragraph(),
            'acceptance' => $this->faker->boolean(),
        ];
    }
}
