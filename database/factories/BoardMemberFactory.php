<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\BoardMember;
use App\Models\BoardPosition;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Board>
 */
class BoardMemberFactory extends Factory
{
    protected $model = BoardMember::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $year = $this->faker->year();

        return [
            'user_id' => User::all()->random()->id,
            'order' => random_int(0, 5),
            'mandate_start' => Carbon::createFromDate($year, 8, 1),
            'mandate_end' => Carbon::createFromDate($year + 1, 7, 31),
            'mandate' => (string) $year.'/'.(substr((string) ($year + 1), -2)),
            'board_position_id' => BoardPosition::all()->random(),
        ];
    }
}
