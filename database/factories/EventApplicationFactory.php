<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Event;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $event = Event::all()->random();

        return [
            'user_id' => $user,
            'event_id' => $event,
            'motivational_letter' => $this->faker->paragraph(),
            'acceptance' => $this->faker->boolean(),
        ];
    }
}
