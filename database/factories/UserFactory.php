<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'branch_verified_at' => now(),
            'last_password_update' => now(),
            'description' => $this->faker->text(),
            'profile_photo_path' => $this->faker->imageUrl(800, 800, 'cats'),
            'banner_path' => $this->faker->imageUrl(1328, 332, 'dogs'),
            'password' => static::$password ??= Hash::make('password'), // password
            'remember_token' => Str::random(10),
            'role_id' => Role::where('name', 'User')->first(),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
            'branch_verified_at' => null,
            'last_password_update' => null,
        ]);
    }
}
