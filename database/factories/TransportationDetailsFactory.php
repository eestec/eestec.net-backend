<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Event;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransportationDetailsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $departure = $this->faker->dateTime();
        $arrival = $this->faker->dateTime($departure);
        $user = User::all()->random();
        $event = Event::all()->random();

        return [
            'user_id' => $user,
            'event_id' => $event,
            'arrival' => $arrival,
            'departure' => $departure,
            'transportation_type' => $this->faker->randomElement(['airplane', 'bus', 'train']),
            'comment' => $this->faker->paragraph(),
            'arrival_location' => $this->faker->address(),
            'departure_location' => $this->faker->address(),
        ];
    }
}
