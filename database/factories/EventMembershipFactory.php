<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Event;
use App\Models\EventRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventMembershipFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $branch = Event::all()->random();
        $role = EventRole::all()->random();

        return [
            'user_id' => $user,
            'event_id' => $branch,
            'event_role_id' => $role,
        ];
    }
}
