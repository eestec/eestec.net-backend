<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Entity;
use App\Models\EntityType;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Entity::factory()->count(1)->state(new Sequence(
            // get() returns an array with one element, so we need to get the first
            fn ($sequence) => ['entity_type_id' => EntityType::where('name', 'BoA')->first()],
        ))
            ->create();

        Entity::factory()
            ->count(20)
            ->state(new Sequence(
                fn ($sequence) => ['entity_type_id' => EntityType::where('name', '<>', 'BoA')->get()->random()],
            ))
            ->create();
    }
}
