<?php

declare(strict_types=1);

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            ContactSeeder::class,
            PartnerSeeder::class, // First partners, as they have no relationships
            RoleSeeder::class, // Then user roles
            UserSeeder::class, // Then users, as they are needed for almost all other relationships
            EntityTypeSeeder::class, // Then starting with entity types
            EntityRoleSeeder::class,
            EntitySeeder::class, // And entities follow (they require users)
            EntityMembershipSeeder::class,
            EntityApplicationSeeder::class,
            BranchTypeSeeder::class,
            BranchRoleSeeder::class,
            BranchSeeder::class, // Branches can be swapped with entities, doesn't matter
            BranchMembershipSeeder::class,
            BranchApplicationSeeder::class,
            EventRoleSeeder::class,
            EventTypeSeeder::class,
            EventSeeder::class,
            EventOrganizersSeeder::class,
            EventMembershipSeeder::class,
            EventApplicationSeeder::class,
            TransportationDetailsSeeder::class,
            BoardPositionSeeder::class,
            BoardMemberSeeder::class,
        ]);
    }
}
