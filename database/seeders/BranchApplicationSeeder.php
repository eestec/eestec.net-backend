<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\BranchApplication;
use App\Models\User;
use Illuminate\Database\Seeder;

class BranchApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branches = Branch::all();
        $all_users = User::all();

        // Add applications to test branch
        $test_branch = Branch::where('name', 'Test Branch')->first();
        $users = $all_users->random(10);
        foreach ($users as $user) {
            if ($user->belongsToBranch($test_branch->id)) {
                continue;
            }
            BranchApplication::factory()->create([
                'branch_id' => $test_branch->id,
                'user_id' => $user->id,
            ]);
        }
        $test_branch->save();

        // Add applications to test LC
        $test_branch = Branch::where('name', 'Test LC')->first();
        $users = $all_users->random(10);
        foreach ($users as $user) {
            if ($user->belongsToBranch($test_branch->id)) {
                continue;
            }
            BranchApplication::factory()->create([
                'branch_id' => $test_branch->id,
                'user_id' => $user->id,
            ]);
        }
        $test_branch->save();

        foreach ($branches as $branch) {
            $users = $all_users->random(rand(1, 5));
            foreach ($users as $user) {
                if ($user->belongsToBranch($branch->id)) {
                    continue;
                }
                if ($user->appliedToBranch($branch->id)) {
                    continue;
                }
                BranchApplication::factory()->create([
                    'branch_id' => $branch,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
