<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Event;
use App\Models\TransportationDetails;
use App\Models\User;
use Illuminate\Database\Seeder;

class TransportationDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $events = Event::all();
        $all_users = User::all();
        foreach ($events as $event) {
            $users = $all_users->random(rand(1, 10));
            foreach ($users as $user) {
                TransportationDetails::factory()->create([
                    'event_id' => $event,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
