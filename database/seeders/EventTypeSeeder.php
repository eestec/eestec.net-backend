<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\EventType;
use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $event_types = [
            [
                'name' => 'Workshop',
            ],
            [
                'name' => 'Exchange',
            ],
            [
                'name' => 'Exclusive Workshop',
            ],
            [
                'name' => 'Operational Event',
            ],
            [
                'name' => 'Advanced Skills Workshop',
            ],
            [
                'name' => 'Soft Skills Workshop',
            ],
            [
                'name' => 'Project Event',
            ],
            [
                'name' => 'Live Board Meeting',
            ],
            [
                'name' => 'International Motivational Weekend',
            ],
            [
                'name' => 'Congress',
            ],
        ];

        foreach ($event_types as $event_type) {
            EventType::create($event_type);
        }
    }
}
