<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $contacts = [
            [
                'name' => 'new observer',
                'to' => 'vc-ea@eestec.net',
                'text' => 'Interested in founding an Observer?',
                'link' => 'Contact our Vice Chairperson for External Affairs',
                'to_text' => 'VC External Affairs',
            ],
            [
                'name' => 'partnership',
                'to' => 'cr-board@eestec.net',
                'cc' => 'vc-ea@eestec.net',
                'text' => 'Interested in a partnership?',
                'link' => 'Contact the Corporate Relations team',
                'to_text' => 'Corporate Relations Board',
            ],
            [
                'name' => 'technical',
                'to' => 'support@eestec.net',
                'cc' => 'it-board@eestec.net',
                'text' => 'Technical questions or problems with this page?',
                'link' => 'Contact support',
                'to_text' => 'EESTEC.net Support',
            ],
            [
                'name' => 'gdpr and privacy',
                'to' => 'legal-experts-leader@eestec.net',
                'cc' => 'treasurer@eestec.net',
                'text' => 'GDPR requests and privacy information?',
                'link' => 'Contact our legal department',
                'to_text' => 'Legal experts leader',
            ],
        ];

        foreach ($contacts as $contact) {
            Contact::create($contact);
        }
    }
}
