<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\BranchType;
use Illuminate\Database\Seeder;

class BranchTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branch_types = [
            ['name' => 'Observer'],
            ['name' => 'Junior Local Committee'],
            ['name' => 'Local Committee'],
            ['name' => 'Terminated'],
        ];

        foreach ($branch_types as $branch_type) {
            BranchType::create($branch_type);
        }
    }
}
