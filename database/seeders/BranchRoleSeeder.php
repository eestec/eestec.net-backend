<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\BranchRole;
use Illuminate\Database\Seeder;

class BranchRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            [
                'name' => 'Board',
                'permissions' => 'Administrator',
            ],
            [
                'name' => 'Member',
                'permissions' => 'User',
            ],
            [
                'name' => 'Alumni',
                'permissions' => 'User',
            ],
        ];

        foreach ($roles as $role) {
            BranchRole::create($role);
        }
    }
}
