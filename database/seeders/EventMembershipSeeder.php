<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Event;
use App\Models\EventMembership;
use App\Models\EventRole;
use App\Models\User;
use Illuminate\Database\Seeder;

class EventMembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $events = Event::all();
        $all_users = User::with('event_memberships')->get();

        // Make our test user organizer of the test event
        $test_event = Event::where('name', 'Test Event')->first();
        $test_user = User::where('email', 'admin@eestec.net')->first();
        EventMembership::factory()->create([
            'event_id' => $test_event->id,
            'user_id' => $test_user->id,
            'event_role_id' => EventRole::organizerId(),
        ]);

        foreach ($events as $event) {
            $users = $all_users->random(rand(1, 10));
            foreach ($users as $user) {
                $user_event_ids = $user->event_memberships->map(function ($event) {
                    return $event->id;
                });

                if (! $user_event_ids->isEmpty() && $user_event_ids->contains($event->id)) {
                    continue;
                }
                EventMembership::factory()->create([
                    'event_id' => $event,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
