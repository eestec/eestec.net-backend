<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Event;
use App\Models\EventApplication;
use App\Models\User;
use Illuminate\Database\Seeder;

class EventApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $events = Event::all();
        $all_users = User::all();

        foreach ($events as $event) {
            $users = $all_users->random(rand(0, 5));
            foreach ($users as $user) {
                EventApplication::factory()->create([
                    'event_id' => $event,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
