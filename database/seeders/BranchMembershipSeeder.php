<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Database\Seeder;

class BranchMembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $branches = Branch::all();
        $all_users = User::all();

        // Make our test user board of the test branch
        $test_branch = Branch::where('name', 'Test Branch')->first();
        $test_lc = Branch::where('name', 'Test LC')->first();

        $botty = User::where('email', 'admin@eestec.net')->first();
        BranchMembership::factory()->create([
            'branch_id' => $test_branch->id,
            'user_id' => $botty->id,
            'branch_role_id' => BranchRole::boardId(),
        ]);

        $lc_admin = User::where('email', 'lc-admin@eestec.net')->first();
        BranchMembership::factory()->create([
            'branch_id' => $test_lc->id,
            'user_id' => $lc_admin->id,
            'branch_role_id' => BranchRole::boardId(),
        ]);

        $user = User::firstWhere('email', 'user@eestec.net');
        BranchMembership::factory()->create([
            'branch_id' => $test_lc->id,
            'user_id' => $user->id,
            'branch_role_id' => BranchRole::memberId(),
        ]);

        // Add members to the test branch
        $users = $all_users->random(10);
        foreach ($users as $user) {
            if (
                $user->belongsToBranch($test_branch->id) ||
                $user->belongsToBranch($test_lc->id)
            ) {
                continue;
            }
            BranchMembership::factory()->create([
                'branch_id' => $test_branch->id,
                'user_id' => $user->id,
                'branch_role_id' => BranchRole::memberId(),
            ]);
        }

        // Add members to the test LC
        $users = $all_users->random(10);
        foreach ($users as $user) {
            if (
                $user->belongsToBranch($test_branch->id) ||
                $user->belongsToBranch($test_lc->id)
            ) {
                continue;
            }
            BranchMembership::factory()->create([
                'branch_id' => $test_lc->id,
                'user_id' => $user->id,
                'branch_role_id' => BranchRole::memberId(),
            ]);
        }

        foreach ($branches as $branch) {
            $users = $all_users->random(rand(1, 30));
            foreach ($users as $user) {
                if ($user->belongsToBranch($branch->id)) {
                    continue;
                }
                BranchMembership::factory()->create([
                    'branch_id' => $branch,
                    'user_id' => $user,
                ]);
            }
            $branch->save();
        }
    }
}
