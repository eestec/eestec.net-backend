<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\EventRole;
use Illuminate\Database\Seeder;

class EventRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            [
                'name' => 'Organizer',
                'permissions' => 'Administrator',
            ],
            [
                'name' => 'Helper',
                'permissions' => 'User',
            ],
            [
                'name' => 'Participant',
                'permissions' => 'User',
            ],
        ];

        foreach ($roles as $role) {
            EventRole::create($role);
        }
    }
}
