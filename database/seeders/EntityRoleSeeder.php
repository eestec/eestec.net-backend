<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\EntityRole;
use Illuminate\Database\Seeder;

class EntityRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = [
            [
                'name' => 'Leader',
                'permissions' => 'Administrator',
            ],
            [
                'name' => 'Coordinator',
                'permissions' => 'Administrator',
            ],
            [
                'name' => 'Member',
                'permissions' => 'User',
            ],
        ];

        foreach ($roles as $role) {
            EntityRole::create($role);
        }
    }
}
