<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // The superuser we have to test things
        User::factory()->create([
            'first_name' => 'Botty',
            'last_name' => 'McBotface',
            'email' => 'admin@eestec.net',
            'role_id' => Role::firstWhere('name', 'Administrator'),
        ]);

        // The branch admin we have to test things
        User::factory()->create([
            'first_name' => 'LC',
            'last_name' => 'Admin',
            'email' => 'lc-admin@eestec.net',
            'role_id' => Role::firstWhere('name', 'User'),
        ]);

        // The non-superuser we have to test things
        User::factory()->create(([
            'email' => 'user@eestec.net',
            'first_name' => 'Test',
            'last_name' => 'User',
            'role_id' => Role::firstWhere('name', 'User'),
        ]));

        User::factory()->count(5)->create();
        User::factory()->count(100)->unverified()->create();
    }
}
