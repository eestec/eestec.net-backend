<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Entity;
use App\Models\EntityMembership;
use App\Models\User;
use Illuminate\Database\Seeder;

class EntityMembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $entities = Entity::all();
        $all_users = User::all();

        foreach ($entities as $entity) {
            $users = $all_users->random(rand(1, 5));
            foreach ($users as $user) {
                EntityMembership::factory()->create([
                    'entity_id' => $entity,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
