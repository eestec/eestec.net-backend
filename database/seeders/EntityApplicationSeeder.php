<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Entity;
use App\Models\EntityApplication;
use App\Models\User;
use Illuminate\Database\Seeder;

class EntityApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $entities = Entity::all();
        $all_users = User::all();

        foreach ($entities as $entity) {
            $users = $all_users->random(rand(0, 5));
            foreach ($users as $user) {
                EntityApplication::factory()->create([
                    'entity_id' => $entity,
                    'user_id' => $user,
                ]);
            }
        }
    }
}
