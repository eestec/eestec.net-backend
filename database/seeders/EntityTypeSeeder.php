<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\EntityType;
use Illuminate\Database\Seeder;

class EntityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $entity_types = [
            [
                'name' => 'Team',
            ],
            [
                'name' => 'Project',
            ],
            [
                'name' => 'Board of the Association',
            ],
            [
                'name' => 'Supervisory Board',
            ],
        ];

        foreach ($entity_types as $entity_type) {
            EntityType::create($entity_type);
        }
    }
}
