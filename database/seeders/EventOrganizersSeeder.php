<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\Entity;
use App\Models\Event;
use Illuminate\Database\Seeder;

class EventOrganizersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $all_branches = Branch::all();
        $all_entities = Entity::all();
        foreach (Event::all() as $event) {
            $branches = $all_branches->random(rand(1, 2));

            if ($event->name == 'Test Event') {
                $branches = $all_branches->filter(fn ($b) => $b->slug == 'test-branch');
            }

            foreach ($branches as $branch) {
                $event->branches()->attach($branch->id);
            }
            $entityNr = rand(0, 4);
            if ($entityNr != 0) {
                $entities = $all_entities->random($entityNr);
                foreach ($entities as $entity) {
                    $event->entities()->attach($entity->id);
                }
            }
        }
    }
}
