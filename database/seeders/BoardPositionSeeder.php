<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\BoardPosition;
use Illuminate\Database\Seeder;

class BoardPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $positions = [
            [
                'name' => 'Chairperson',
                'description' => "Responsible for:\n- the operational work of the Board,\n- Board Meetings,\n- Congresses.",
            ],
            [
                'name' => 'Treasurer',
                'description' => "Responsible for:\n- account keeping and financial reports,\n- the Board's access to the Association's bank accounts\n- maintaining and updating the legal documents of the Association.",
            ],
            [
                'name' => 'Vice Chairperson for External Affairs',
                'description' => "Responsible for:\n- initiation of EESTEC branches,\n- contact with other NGOs,\n- contacts with universities,\n- company contacts and fundraising,\n- public relations.",
            ],
            [
                'name' => 'Vice Chairperson for Internal Affairs',
                'description' => "Responsible for:\n- coordinating the work of Contact Persons,\n- the activation and motivation of internationally active members of branches,\n- the activation, education and motivation of all levels of EESTEC branches,\n- cooperations within the network of EESTEC.",

            ],
            [
                'name' => 'Vice Chairperson for Administrative Affairs',
                'description' => "Responsible for:\n- digital and physical documentation preservation and transfer,\n- data collection and evaluation,\n- status reports,\n- ensuring internal compliance with internal regulations and legal documents,\n- quality, validity and effective conduct of EESTEC Events.",
            ],

        ];

        foreach ($positions as $position) {
            BoardPosition::create($position);
        }
    }
}
