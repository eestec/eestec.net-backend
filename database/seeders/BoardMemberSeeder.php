<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\BoardMember;
use App\Models\User;
use Illuminate\Database\Seeder;

class BoardMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $bottie = User::where('email', 'admin@eestec.net')->first();
        BoardMember::factory()->create([
            'user_id' => $bottie->id,
        ]);

        BoardMember::factory()->count(4)->create();
    }
}
