<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Branch;
use App\Models\BranchType;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = BranchType::all();
        $observer_t = $types->first(fn ($value, $key) => $value->name == 'Observer');
        $jlc_t = $types->first(fn ($value, $key) => $value->name == 'Junior Local Committee');
        $lc_t = $types->first(fn ($value, $key) => $value->name == 'Local Committee');

        Branch::factory()->create(['name' => 'Test Branch']);
        Branch::factory()->create(['name' => 'Test LC', 'branch_type_id' => BranchType::firstWhere('name', 'Local Committee')]);
        Branch::factory()->create(['name' => 'Test JLC', 'branch_type_id' => BranchType::firstWhere('name', 'Junior Local Committee')]);
        Branch::factory()->create(['name' => 'Test Observer', 'branch_type_id' => BranchType::firstWhere('name', 'Observer')]);

        Branch::factory()->count(10)->create();
    }
}
