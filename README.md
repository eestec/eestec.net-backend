<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Setting up the local environment

### [Development Containers](https://containers.dev/)

1. Install [Docker Desktop](https://docs.docker.com/desktop/)
1. Install [Visual Studio Code](https://code.visualstudio.com/download)
1. Install [Dev Containers (VS Code extension)](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
1. In upper right of this GitLab page click the blue button `Code` and then _Visual Studio Code (HTTPS)_
1. Copy `.env.example` to `.env`: `cp .env.example .env`
1. In VS Code: View -> Command Palette (Ctrl+Shift+P) -> type _Dev Containers: Reopen in Container_ and run it
    - If running this for the first time, it will take some time to build everything
1. Everthing should be running now, the backend is accessible at [http://localhost:8000](http://localhost:8000) and the pgAdmin at [http://localhost:5050](http://localhost:5050)

### Windows

There are many ways of [installing Laravel](https://laravel.com/docs/8.x/installation). Seid's personal preference is to install [XAMPP](https://www.apachefriends.org/index.html), [Composer](https://getcomposer.org/download/)

After installing Laravel & XAMPP, here are the steps to making it work locally:

-   Pull the project into your preferred folder
-   Open XAMPP and turn on the Apache server (to serve php myAdmin) and the MySQL database
-   Install Imagick php module ([Instructions](http://jichiduo.com/2022/03/16/Install-Imagick-Xampp-PHP8-1-Windows/))
-   In the project folder, copy the `.env.example` file and rename it to `.env`
-   In the `.env` file, change the `DB_DATABASE` attribute to the name of the local database name you intend to use
-   Open your phpmyadmin using XAMPP (Admin button in the MySQL row of the XAMPP control panel)
-   Create new database with the name used in the `DB_DATABASE` attribute of the `.env` file
-   Open the command prompt and go to the project folder
-   `composer install` - this will install all php dependencies defined in composer.json
-   `php artisan key:generate` - sets the APP_KEY value in your .env file. It's main use is as a seed for all cryptographic functions.
-   `php artisan migrate --seed` - creates the database tables and adds dummy data

Since it's Laravel, there will probably be some issues with the installation. Please write an issue in that case.

### Linux

-   Install the required packages. These include `php, imagemagick` and the required php modules (openssl, pdo, mbstring, tokenizer, xml). In Debian-based distros:

    ```bash
    sudo apt install php unzip openssl php-bcmath php-curl php-json php-mbstring php-mysql php-tokenizer php-xml php-zip imagemagick php-imagick php-gd
    ```

    Depending on the database you will want to use, you will have to install also the php drivers for the database

    For postgreSQL:

    ```bash
    sudo apt install php-pgsql
    ```

    For mySQL:

    ```bash
    sudo apt install php-mysql
    ```

-   Install composer (php package manager)

    ```bash
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    ```

    If you want to make composer global, you can run:

    ```bash
    mv composer.phar /usr/local/bin/composer
    ```

-   Start the database

    For postgreSQL:

    ```bash
    docker compose up -d postgres pgadmin
    ```

    For mySQL:

    ```bash
    docker compose up -d mysql
    ```

## How to run the application

-   Open the command prompt and go to the project folder `php artisan serve`
-   Since there is no frontend in Laravel, there is not much to see,
    hitting some endpoints with Postman on `http://localhost:8000` should
    indicate if Laravel is running.
-   The default admin user has email `admin@eestec.net` and password `password`.

## Schema

You can find the schema of the database on [GitLab Pages](https://eestec.gitlab.io/eestec.net-backend/).

## Current version of Laravel in the project

laravel/laravel 11.1.4

```

```
