<?php

declare(strict_types=1);

use App\Console\Commands\DeleteBranchUnverifiedUsers;
use App\Console\Commands\DeleteEmailUnverifiedUsers;
use App\Console\Commands\DeleteNeverUpdatedPasswordUsers;
use App\Console\Commands\UpdateStatistics;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schedule;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

Schedule::command(DeleteEmailUnverifiedUsers::class)
    ->daily()
    ->at('04:00')
    ->appendOutputTo(base_path('storage/logs/scheduler.log'));

Schedule::command(DeleteBranchUnverifiedUsers::class)
    ->daily()
    ->at('04:05')
    ->appendOutputTo(base_path('storage/logs/scheduler.log'));

//Schedule::command(DeleteNeverUpdatedPasswordUsers::class, ['210'])
//     ->daily()
//     ->at('04:10')
//     ->appendOutputTo(base_path('storage/logs/scheduler.log'));

Schedule::command(UpdateStatistics::class)
    ->daily()
    ->at('4:15')
    ->appendOutputTo(base_path('storage/logs/scheduler.log'));

Schedule::command('backup:clean')
    ->daily()
    ->at('01:00')
    ->appendOutputTo(base_path('storage/logs/scheduler.log'));

Schedule::command('backup:run')
    ->daily()
    ->at('01:30')
    ->appendOutputTo(base_path('storage/logs/scheduler.log'));
