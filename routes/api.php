<?php

declare(strict_types=1);

use App\Http\Controllers\BoardController;
use App\Http\Controllers\BoardPositionController;
use App\Http\Controllers\Branch\BranchApplicationController;
use App\Http\Controllers\Branch\BranchController;
use App\Http\Controllers\Branch\BranchMembershipController;
use App\Http\Controllers\Branch\BranchRoleController;
use App\Http\Controllers\Branch\BranchTypeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\DeleteCurrentUserController;
use App\Http\Controllers\Event\EventApplicationController;
use App\Http\Controllers\Event\EventController;
use App\Http\Controllers\Event\EventCsvController;
use App\Http\Controllers\Event\EventMembershipController;
use App\Http\Controllers\Event\EventOrganizerController;
use App\Http\Controllers\Event\EventRoleController;
use App\Http\Controllers\Event\EventTypeController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\UserController;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Statistics
Route::get('/statistics', StatisticsController::class);

// Global roles
Route::get('/roles', [RoleController::class, 'index']);

// Branch roles
Route::get('branches/roles', [BranchRoleController::class, 'index']);
Route::get('branches/types', [BranchTypeController::class, 'index']);

// Event Types
Route::get('events/types', [EventTypeController::class, 'index'])->name('events.types');

// Event Roles
Route::get('events/roles', [EventRoleController::class, 'index'])->name('events.roles');

// Events
Route::get('events', [EventController::class, 'index']);
Route::get('events/{event}', [EventController::class, 'show'])->name('events.show');

// Branches
Route::get('/branches', [BranchController::class, 'index']);
Route::get('/branches/{branch}', [BranchController::class, 'show']);
Route::get('branches/{branch}/board', [BranchController::class, 'boardMembers'])->name('branches.board');
Route::get('branches/{branch}/events', [BranchController::class, 'events'])->name('branch.events');

// Contacts
Route::get('/contacts', [ContactController::class, 'index']);
Route::post('contacts/{contact}', ContactUsController::class);

// Partners
Route::get('/partners', [PartnerController::class, 'index']);

// Board
Route::get('/board', [BoardController::class, 'index']);

// Board positions
Route::get('/board-positions', [BoardPositionController::class, 'index']);
Route::get('/board-positions/{position}', [BoardPositionController::class, 'show']);

Route::middleware(['auth:sanctum'])->group(
    function () {
        // Users need to only be authenticated to see their own data.
        // TODO: Why do we return success true?
        Route::get('/user', function (Request $request) {
            return response()->json(['success' => true, 'data' => new UserResource($request->user()->with(['branch_memberships', 'branch_applications'])->find($request->user()->id))]);
        });
        Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');
        Route::match(['PUT', 'PATCH'], 'users/{user}', [UserController::class, 'update'])->name('users.update');
        Route::delete('/user', DeleteCurrentUserController::class);

        // Users need to be logged in to apply to branch.
        Route::post('/branches/{branch}/apply', [BranchApplicationController::class, 'apply']);
    }
);

Route::middleware(['auth:sanctum', 'email.verified', 'branch.verified'])->group(
    function () {
        // Partners
        Route::delete('partners/{partner}', [PartnerController::class, 'destroy'])->name('partners.destroy');

        // Users
        Route::get('users', [UserController::class, 'index']);
        Route::get('users/{user}/events-attended', [UserController::class, 'attendedEvents'])->name('users.attended-events');
        Route::get('users/{user}/events-organized', [UserController::class, 'organizedEvents'])->name('users.organized-events');
        Route::delete('users/{user}', [UserController::class, 'destroy']);
        Route::post('users/{user}/role', [RoleController::class, 'changeUserRole']);

        // Branches
        Route::post('branches', [BranchController::class, 'store'])->name('branches.store');
        Route::match(['PUT', 'PATCH'], 'branches/{branch}', [BranchController::class, 'update'])->name('branches.update');
        Route::post('branches/{branch}/type', [BranchTypeController::class, 'changeType']);
        Route::get('branches/{branch}/members', [BranchController::class, 'members'])->name('branches.members');

        Route::post('branches/{branch}/add/{user}', [BranchMembershipController::class, 'store']);
        Route::post('branches/{branch}/change-role/{user}', [BranchRoleController::class, 'changeRole'])->name('branches.change-role');
        Route::post('branches/{branch}/change-role', [BranchRoleController::class, 'massChangeRole']);
        Route::delete('branches/{branch}/remove', [BranchMembershipController::class, 'destroyMany']);
        Route::delete('branches/{branch}/remove/{user}', [BranchMembershipController::class, 'destroy']);
        Route::post('branches/{branch}/add-application/{user}', [BranchApplicationController::class, 'store']);
        Route::get('branches/{branch}/applications', [BranchApplicationController::class, 'getApplications'])->name('branches.applications');
        Route::post('branches/{branch}/applications/approval', [BranchApplicationController::class, 'massApproval']);
        Route::post('branches/{branch}/applications/{application}/approval', [BranchApplicationController::class, 'approval'])->name('branches.applications.approval');

        // Events
        Route::post('events', [EventController::class, 'store'])->name('events.store');

        Route::match(['PUT', 'PATCH'], 'events/{event}', [EventController::class, 'update'])->name('events.update');
        Route::delete('events/{event}', [EventController::class, 'destroy'])->name('events.destroy');

        Route::post('events/{event}/csv', [EventCsvController::class, 'upload']);

        Route::get('events/{event}/participants', [EventController::class, 'participants'])->name('events.participants');
        Route::get('events/{event}/organizers', [EventController::class, 'organizers'])->name('events.organizers');
        Route::get('events/{event}/helpers', [EventController::class, 'helpers'])->name('events.helpers');

        // Manage event users & roles
        Route::match(['PUT', 'PATCH'], 'events/{event}/change-role/{user}', [EventRoleController::class, 'changeEventRole'])->name('events.changeRole');
        Route::post('events/{event}/add-user/{user}', [EventMembershipController::class, 'addUser'])->name('events.addUser');
        Route::delete('events/{event}/remove-user/{user}', [EventMembershipController::class, 'destroy'])->name('events.deleteMember');

        Route::post('events/{event}/add-organizing-branch', [EventOrganizerController::class, 'addOrganizingBranch'])->name('events.addOrganizingBranch');

        // Event applications routes
        Route::post('events/{event}/apply', [EventApplicationController::class, 'store'])->name('events.apply');
        Route::post('events/{event}/applications', [EventApplicationController::class, 'addEventApplication'])->name('events.applications');
        Route::get('events/{event}/applications/{eventApplication}', [EventApplicationController::class, 'show'])->name('events.applications.show');
        Route::delete('events/applications/{eventApplication}', [EventApplicationController::class, 'destroy'])->name('events.applications.destroy');
        Route::get('events/{event}/applications', [EventApplicationController::class, 'index'])->name('events.applications.index');
        Route::match(['PUT', 'PATCH'], 'events/applications/{eventApplication}', [EventApplicationController::class, 'update'])->name('events.applications.update');
        Route::post('events/applications/{eventApplication}/approval', [EventApplicationController::class, 'approval'])->name('events.applications.approval');

        // Admin specific endpoints
        Route::get('admin', [UserController::class, 'admin'])->name('user.admin');

        // Board
        Route::post('/board', [BoardController::class, 'store']);
        Route::put('/board/{position}', [BoardController::class, 'update']);
        Route::delete('/board/{position}', [BoardController::class, 'destroy']);
        Route::put('/board-positions/{position}', [BoardPositionController::class, 'update']);

    }
);
