<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController;

/*
|--------------------------------------------------------------------------
| Sanctum Routes
|--------------------------------------------------------------------------
|
| This is a route we added on RouteServiceProvider to override the sanctum
| original route, since it was registered on the web middleware.
|
*/

Route::get(
    '/csrf-cookie',
    CsrfCookieController::class.'@show'
)->name('sanctum.csrf-cookie');
