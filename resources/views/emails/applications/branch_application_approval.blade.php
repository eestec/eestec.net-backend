@component('mail::message')
    Dear {{ $user->first_name }},

@if($acceptance)
<p>Your application to {{ $branch_type_name}} {{ $branch->name }} has been approved. You can now login and fully enjoy EESTEC.net.</p>
@else
<p>Your application to {{ $branch_type_name}} {{ $branch->name }} has been rejected.</p>
@if (!$user->branch_verified_at)
<p>Since you are not registered in any branch, your account will soon be deleted.</p>
<p>You can apply to a new branch, if you think this was a mistake.</p>
@endif
<p>If you have any questions, please contact us at <a href="mailto:support@eestec.net">support@eestec.net</a></p>
@endif
@endcomponent
