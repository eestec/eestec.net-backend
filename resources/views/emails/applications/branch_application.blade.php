@component('mail::message')
    <p class="centered">User {{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }}) applied to your branch.</p>
    <p class="centered">Visit your branch page to accept or reject this application.</p>
    @component('mail::button', ['url' => $view_url])
        See all applications
    @endcomponent
@endcomponent
