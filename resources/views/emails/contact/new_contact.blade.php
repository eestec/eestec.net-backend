@component('mail::message')
Dear {{ $contact->to_text }},

A message was sent from:

<p class="centered">{{ $sender }}</p>

<p>The message was:</p>

<p class="centered">{{ $content }}</p>
@endcomponent
