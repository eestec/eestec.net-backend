<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            <img src="{{ Storage::url('public/images/logo/EESTEC_red.png') }}" class="logo" alt="EESTEC Logo">
        </a>
    </td>
</tr>
