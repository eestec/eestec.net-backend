<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('ensures that unregistered visitors cannot search users', function () {
    $response = $this->getJson('/api/users');
    $response->assertUnauthorized();
});

it('ensures that registered people can search users', function () {
    $user = User::factory()->create();
    $response = $this->actingAs($user)->getJson('/api/users');
    $response->assertValid();
});

it('ensures that the search function works correctly', function () {
    $user = User::factory()->create();
    $response = $this->actingAs($user)->getJson('/api/users?[search]=test');
    $response->assertValid();
});
