<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('checks that the partners endpoint works', function () {

    $response = $this->get('/api/partners');

    $response->assertStatus(200);

    $response->assertJsonStructure(['*' => [
        'name',
        'slug',
        'description',
        'logo',
    ]]);
});

it('checks that a superuser can delete a partner', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');

    $response = $this->actingAs($user)->delete('/api/partners/partner-1');

    $response->assertStatus(204);
});

it('checks that a random user cannot delete a partner', function () {
    $user = User::firstWhere('slug', 'test-user');

    $response = $this->actingAs($user)->delete('/api/partners/partner-1');

    $response->assertStatus(403);
});
