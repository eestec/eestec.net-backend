<?php

declare(strict_types=1);

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;

uses(RefreshDatabase::class);

it('returns an array of events when requesting the active ones', function () {
    $response = $this->get('/api/events?kind=active');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});

it('returns an array of events when requesting the past ones', function () {
    $response = $this->get('/api/events?kind=past');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});

it('returns all events of branch', function () {
    $response = $this->get('/api/events');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});
