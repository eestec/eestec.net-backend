<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts that a user can delete themselves', function () {
    $user = User::factory()->create();
    $this->withHeader('origin', config('app.spa_url'));

    $response = $this->actingAs($user)->deleteJson('/api/user', ['password' => 'password']);
    $response->assertOk();
});

it('asserts that a user without pictures can delete themselves', function () {
    $user = User::factory()->create([
        'profile_photo_path' => null,
        'banner_path' => null,
    ]);
    $this->withHeader('origin', config('app.spa_url'));

    $response = $this->actingAs($user)->deleteJson('/api/user', ['password' => 'password']);
    $response->assertOk();
});
