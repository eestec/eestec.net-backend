<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\BranchApplication;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts that a global admin can see branch applications', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');

    $response = $this->actingAs($user)->getJson('api/branches/test-lc/applications');
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'id',
                'user_id',
                'created_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('asserts that a branch admin can see branch applications', function () {
    $user = User::firstWhere('email', 'lc-admin@eestec.net');

    $response = $this->actingAs($user)->getJson('api/branches/test-lc/applications');
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'id',
                'user_id',
                'created_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('asserts that a branch member cannot see branch applications', function () {
    $user = User::firstWhere('email', 'user@eestec.net');

    $response = $this->actingAs($user)->getJson('api/branches/test-lc/applications');
    $response->assertStatus(403);
});

it('asserts that a global admin can accept a member in a branch', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $application = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->first();

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/'.$application->id.'/approval', [
        'acceptance' => true,
    ]);
    $response->assertOk();
});

it('asserts that a local admin can accept a member in a branch', function () {
    $user = User::with('branch_memberships')->firstWhere('email', 'lc-admin@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $application = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->first();

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/'.$application->id.'/approval', [
        'acceptance' => true,
    ]);
    $response->assertOk();
});

it('asserts that a branch member cannot accept a member in a branch', function () {
    $user = User::with('branch_memberships')->firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $application = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->first();

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/'.$application->id.'/approval', [
        'acceptance' => true,
    ]);

    $response->assertStatus(403);
});

it('asserts that a global admin can bulk accept members in a branch', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $applications = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->limit(3)->get()->map(fn (BranchApplication $application) => $application->id);

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/approval', [
        'acceptance' => true,
        'applications' => $applications,
    ]);
    $response->assertOk();
});

it('asserts that a branch admin can bulk accept members in a branch', function () {
    $user = User::firstWhere('email', 'lc-admin@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $applications = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->limit(3)->get()->map(fn (BranchApplication $application) => $application->id);

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/approval', [
        'acceptance' => true,
        'applications' => $applications,
    ]);
    $response->assertOk();
});

it('asserts that a branch user cannot bulk accept members in a branch', function () {
    $user = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $applications = BranchApplication::where([
        'branch_id' => $branch->id,
    ])->limit(3)->get()->map(fn (BranchApplication $application) => $application->id);

    $response = $this->actingAs($user)->postJson('api/branches/test-lc/applications/approval', [
        'acceptance' => true,
        'applications' => $applications,
    ]);
    $response->assertStatus(403);
});
