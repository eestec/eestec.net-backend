<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\BranchMembership;
use App\Models\BranchRole;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts that roles are returned in GET request', function () {
    $response = $this->get('/api/branches/roles');

    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'id',
                'name',
                'permissions',
            ],
        ],
    ]);
});

it('asserts that a global admin can change a members role within the branch', function () {
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $user = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $board_role_id = BranchRole::boardId();

    $membership = BranchMembership::where([
        ['branch_id', '=', $branch->id],
        ['user_id', '!=', $user->id],
        ['branch_role_id', '!=', $board_role_id],
    ])->with('user')->first();

    $response = $this->actingAs($admin)->post('api/branches/'.$branch->slug.'/change-role/'.$membership->user->slug, [
        'branch_role_id' => $board_role_id,
    ]);

    $response->assertSessionHasNoErrors();

    $this->assertDatabaseHas('branch_memberships', [
        'user_id' => $membership->user->id,
        'branch_id' => $branch->id,
        'branch_role_id' => $board_role_id,
    ]);
});

it('asserts that a branch admin can change a members role within the branch', function () {
    $admin = User::firstWhere('email', 'lc-admin@eestec.net');
    $user = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $board_role_id = BranchRole::boardId();

    $membership = BranchMembership::where([
        ['branch_id', '=', $branch->id],
        ['user_id', '!=', $user->id],
        ['branch_role_id', '!=', $board_role_id],
    ])->with('user')->first();

    $response = $this->actingAs($admin)->post('api/branches/'.$branch->slug.'/change-role/'.$membership->user->slug, [
        'branch_role_id' => $board_role_id,
    ]);

    $response->assertSessionHasNoErrors();

    $this->assertDatabaseHas('branch_memberships', [
        'user_id' => $membership->user->id,
        'branch_id' => $branch->id,
        'branch_role_id' => $board_role_id,
    ]);
});

it('asserts that a branch member cannot change a members role within the branch', function () {
    $user = User::firstWhere('slug', 'test-user');
    $branch = Branch::firstWhere('slug', 'test-branch');
    $board_role_id = BranchRole::boardId();

    $membership = BranchMembership::where([
        ['branch_id', '=', $branch->id],
        ['user_id', '!=', $user->id],
        ['branch_role_id', '!=', $board_role_id],
    ])->with('user')->first();

    $url = 'api/branches/'.$branch->slug.'/change-role/'.$membership->user->slug;

    $response = $this->actingAs($user)->post($url, [
        'branch_role_id' => $board_role_id,
    ]);
    $response->assertStatus(403);
});

it('asserts that a branch admin can bulk change branch roles', function () {
    $admin = User::firstWhere('email', 'lc-admin@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $board_role_id = BranchRole::boardId();

    $users_to_change = $branch->activeMembers()
        ->whereNot('email', 'lc-admin@eestec.net')
        ->limit(5)->get()->map(function (User $u) {
            return $u->slug;
        });

    $response = $this->actingAs($admin)->postJson('/api/branches/test-lc/change-role',
        [
            'branch_role_id' => $board_role_id,
            'users' => $users_to_change,
        ]);

    $response->assertOk();

    $this->assertDatabaseHas('branch_memberships', [
        'branch_role_id' => $board_role_id,
        'user_id' => User::firstWhere('slug', $users_to_change->first())->id,
        'branch_id' => $branch->id,
    ]);
});

it('asserts that a global admin can bulk change branch roles', function () {
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $board_role_id = BranchRole::boardId();

    $users_to_change = $branch->activeMembers()
        ->whereNot('slug', 'botty-mcbotface')
        ->limit(5)->get()->map(function (User $u) {
            return $u->slug;
        });

    $response = $this->actingAs($admin)->postJson('/api/branches/test-lc/change-role',
        [
            'branch_role_id' => $board_role_id,
            'users' => $users_to_change,
        ]);

    $response->assertOk();

    $this->assertDatabaseHas('branch_memberships', [
        'branch_role_id' => $board_role_id,
        'user_id' => User::firstWhere('slug', $users_to_change->first())->id,
        'branch_id' => $branch->id,
    ]);
});

it('asserts that a member cannot bulk change branch roles', function () {
    $user = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $board_role_id = BranchRole::boardId();

    $users_to_change = $branch->activeMembers()
        ->whereNot('email', 'user@eestec.net')
        ->whereNot('branch_role_id', $board_role_id)
        ->limit(5)->get()
        ->map(function (User $u) {
            return $u->slug;
        });

    $response = $this->actingAs($user)->postJson('/api/branches/test-lc/change-role',
        [
            'branch_role_id' => $board_role_id,
            'users' => $users_to_change,
        ]);

    $response->assertStatus(403);
    $this->assertDatabaseMissing('branch_memberships', [
        'branch_role_id' => $board_role_id,
        'user_id' => User::firstWhere('slug', $users_to_change->first())->id,
        'branch_id' => $branch->id,
    ]);
});
