<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts that a global admin can remove a single branch membership', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $lc_user = User::firstwhere('email', 'lc-admin@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $response = $this->actingAs($user)->deleteJson('/api/branches/test-lc/remove/'.$lc_user->slug);
    $response->assertOk();
    $this->assertDatabaseMissing('branch_memberships', [
        'user_id' => $lc_user->id,
        'branch_id' => $branch->id,
    ]);
});

it('asserts that a branch admin can remove a single branch membership', function () {
    $board = User::firstWhere('email', 'lc-admin@eestec.net');
    $member = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $response = $this->actingAs($board)->deleteJson('/api/branches/test-lc/remove/'.$member->slug);
    $response->assertOk();
    $this->assertDatabaseMissing('branch_memberships', [
        'user_id' => $member->id,
        'branch_id' => $branch->id,
    ]);
});

it('asserts that a normal member cannot remove a single branch membership', function () {
    $board = User::firstWhere('email', 'lc-admin@eestec.net');
    $member = User::firstWhere('email', 'user@eestec.net');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $response = $this->actingAs($member)->deleteJson('/api/branches/test-lc/remove/'.$board->slug);
    $response->assertStatus(403);

    $this->assertDatabaseHas('branch_memberships', [
        'user_id' => $member->id,
        'branch_id' => $branch->id,
    ]);
});

it('asserts that a global admin can bulk remove branch memberships', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $test_branch = Branch::where('name', 'Test Branch')->first();

    $users_to_delete = $test_branch->activeMembers()
        ->whereNot('slug', 'botty-mcbotface')
        ->limit(5)->get()->map(function (User $u) {
            return $u->slug;
        });
    $response = $this->actingAs($user)->deleteJson('/api/branches/test-branch/remove?slugs='.$users_to_delete->join(','));
    $response->assertOk();

    $this->assertDatabaseMissing('branch_memberships', [
        'user_id' => User::firstWhere('slug', $users_to_delete->first())->id,
        'branch_id' => $test_branch->id,
    ]);
});

it('asserts that a branch admin can bulk remove branch memberships', function () {
    $board = User::firstWhere('email', 'lc-admin@eestec.net');
    $test_branch = Branch::where('name', 'Test LC')->first();

    $users_to_delete = $test_branch->activeMembers()
        ->whereNot('email', 'lc-admin@eestec.net')
        ->limit(5)->get()->map(function (User $u) {
            return $u->slug;
        });

    $response = $this->actingAs($board)->deleteJson('/api/branches/test-lc/remove?slugs='.$users_to_delete->join(','));
    $response->assertOk();

    $this->assertDatabaseMissing('branch_memberships', [
        'user_id' => User::firstWhere('slug', $users_to_delete->first())->id,
        'branch_id' => $test_branch->id,
    ]);
});

it('asserts that a normal member cannot bulk remove branch memberships', function () {
    $member = User::firstWhere('email', 'user@eestec.net');
    $test_branch = Branch::where('name', 'Test LC')->first();

    $users_to_delete = $test_branch->activeMembers()
        ->whereNot('email', 'user@eestec.net')
        ->limit(5)->get()->map(function (User $u) {
            return $u->slug;
        });

    $response = $this->actingAs($member)->deleteJson('/api/branches/test-lc/remove?slugs='.$users_to_delete->join(','));
    $response->assertStatus(403);

    $this->assertDatabaseHas('branch_memberships', [
        'user_id' => User::firstWhere('slug', $users_to_delete->first())->id,
        'branch_id' => $test_branch->id,
    ]);
});
