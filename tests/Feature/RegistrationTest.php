<?php

declare(strict_types=1);

use App\Mail\NewMemberApplicationToBranch;
use App\Models\Branch;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

uses(RefreshDatabase::class);

it('can register a user', function () {
    $response = $this->post('auth/register', [
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'test@example.com',
        'password' => 'EESTEC_password',
        'password_confirmation' => 'EESTEC_password',
        'terms' => true,
        'branch' => Branch::first()->name,
    ]);
    $this->assertAuthenticated();
});

it('can not register a new user with the same email', function () {
    $user = User::factory()->create();

    $response = $this->post('auth/register', [
        'last_name' => 'User',
        'email' => $user->email,
        'password' => 'password',
        'password_confirmation' => 'password',
        'terms' => true,
        'branch' => Branch::first()->name,
    ]);
    $response->assertInvalid('email');
});

it('can not register a user with a wrong branch', function () {
    $response = $this->post('auth/register', [
        'last_name' => 'User',
        'email' => 'test@example.com',
        'password' => 'password',
        'password_confirmation' => 'password',
        'terms' => true,
        'branch' => 'Athina',
    ]);
    $response->assertInvalid('branch');
});

it('is invalid', function (array $data, array $errors) {
    $response = $this->post('auth/register', [
        'last_name' => 'User',
        'email' => 'test@example.com',
        'password' => 'password',
        'branch' => Branch::first()->name,
        array_values($data),
    ]);
    $response->assertInvalid($errors);
})->with([
    'first name > 255 characters' => [
        [
            'first_name' => str_repeat('e', 256),
            'password_confirmation' => 'password13',
            'terms' => true,
        ],
        ['password'],
    ],
    'password != password_confirmation' => [
        [
            'first_name' => 'Test',
            'password_confirmation' => 'password13',
            'terms' => true,
        ],
        ['password'],
    ],
    'password < 8 characters' => [
        [
            'first_name' => 'Test',
            'password_confirmation' => 'pass',
            'terms' => true,
        ],
        ['password'],
    ],
    'terms not accepted' => [
        [
            'first_name' => 'Test',
            'password_confirmation' => 'password',
            'terms' => false,
        ],
        ['terms'],
    ],
]);

it('tests the branch receives an application request', function () {
    Mail::fake();

    $this->post('auth/register', [
        'first_name' => 'Test',
        'last_name' => 'User',
        'email' => 'test@example.com',
        'password' => 'EESTEC_password',
        'password_confirmation' => 'EESTEC_password',
        'terms' => true,
        'branch' => Branch::first()->name,
    ]);

    Mail::assertQueued(NewMemberApplicationToBranch::class);
});
