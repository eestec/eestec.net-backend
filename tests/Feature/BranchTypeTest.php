<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\BranchType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts that types are returned in GET request', function () {
    $response = $this->get('/api/branches/types');

    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'id',
                'name',
            ],
        ],
    ]);
});

it('asserts that a global admin can change a branch\'s type', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $branch = Branch::firstWhere('slug', 'test-jlc');
    $branchType = BranchType::firstWhere('name', 'Observer');

    $response = $this->actingAs($user)->post('api/branches/'.$branch->slug.'/type/', [
        'branch_type' => $branchType->name,
    ]);

    $response->assertSessionHasNoErrors();

    $this->assertDatabaseHas('branches', [
        'id' => $branch->id,
        'branch_type_id' => $branchType->id,
    ]);
});

it('asserts that a branch admin cannot change their branch\'s type', function () {
    $user = User::firstWhere('slug', 'lc-admin');
    $branch = Branch::firstWhere('slug', 'test-lc');
    $branchType = BranchType::firstWhere('name', 'Observer');

    $response = $this->actingAs($user)->post('api/branches/'.$branch->slug.'/type', [
        'branch_type' => $branchType->name,
    ]);

    $response->assertStatus(403);
});

it('asserts that a user cannot change a branch\'s type', function () {
    $user = User::firstWhere('slug', 'lc-admin');
    $branch = Branch::firstWhere('slug', 'test-jlc');
    $branchType = BranchType::firstWhere('name', 'Observer');

    $response = $this->actingAs($user)->post('api/branches/'.$branch->slug.'/type', [
        'branch_type' => $branchType->name,
    ]);

    $response->assertStatus(403);
});
