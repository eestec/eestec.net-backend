<?php

declare(strict_types=1);

use App\Actions\Branch\CreateNewBranch;
use App\Models\Branch;
use App\Models\BranchType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;

use function Pest\Faker\fake;

uses(RefreshDatabase::class);

it('tests that the branch create action works', function () {
    Event::fake();
    Storage::fake('public/images/branches/profile-pictures');
    $file = UploadedFile::fake()->image('avatar.jpg');

    $creator = new CreateNewBranch;

    $input = [
        'name' => fake()->city(),
        'email' => fake()->safeEmail(),
        'founded_in' => fake()->year(),
        'website' => fake()->url(),
        'profile_picture_path' => $file,
        'description' => fake()->text(),
        'country' => fake()->country(),
        'region' => fake()->numberBetween(1, 5),
        'facebook' => fake()->url(),
        'instagram' => fake()->url(),
        'linkedin' => fake()->url(),
        'address' => fake()->address(),
        'branch_type_id' => BranchType::firstWhere('name', 'Observer')->id,
    ];

    $branch = $creator->create($input);
    expect($branch)->toBeInstanceOf(Branch::class);
});
