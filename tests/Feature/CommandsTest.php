<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

beforeEach(
    function () {
        return User::factory()->unverified()->count(10)->create([
            'created_at' => now()->subDays(15),
            'updated_at' => now()->subDays(15),
        ]);
    }
);

it('tests if delete email unverified users command works', function () {
    $this->artisan('eestecnet:delete-email-unverified-users')
        ->expectsOutput('Deleted 10 email unverified users.')
        ->assertExitCode(0);
});

it('tests if delete email unverified users command with arguments works', function () {
    $this->artisan('eestecnet:delete-email-unverified-users 10')
        ->expectsOutput('Deleted 10 email unverified users.')
        ->assertExitCode(0);

    $this->artisan('eestecnet:delete-email-unverified-users 10')
        ->expectsOutput('Deleted 0 email unverified users.')
        ->assertExitCode(0);
});

it('tests if delete branch unverified users command works', function () {
    $this->artisan('eestecnet:delete-branch-unverified-users')
        ->expectsOutput('Deleted 0 branch unverified users.')
        ->assertExitCode(0);

    $this->artisan('eestecnet:delete-branch-unverified-users 7')
        ->expectsOutput('Deleted 10 branch unverified users.')
        ->assertExitCode(0);
});
