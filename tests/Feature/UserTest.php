<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;

uses(RefreshDatabase::class);

it("asserts that not logged in people don't have access to profiles", function () {
    $response = $this->getJson('/api/users/botty-mcbotface');
    $response->assertJson([
        'message' => 'Unauthenticated.',
    ]);
});

it('asserts that logged in people can see only partial profiles', function () {
    $user = User::factory()->create();
    $bot = User::where('slug', 'botty-mcbotface')->first();
    $response = $this->actingAs($user)->get('/api/users/botty-mcbotface');
    $response->assertJson(fn (AssertableJson $json) => $json->where('first_name', $bot->first_name)
        ->where('last_name', $bot->last_name)
        ->where('slug', $bot->slug)
        ->where('description', $bot->description)
        ->where('profile_photo_path', Storage::url($bot->profile_photo_path))
        ->where('banner_path', Storage::url($bot->banner_path))
        ->has('email_verified_at')
        ->has('branch_verified_at')
        ->etc()
    );
});

it('asserts that a user can see all the data for themselves', function () {
    $user = User::where('slug', 'botty-mcbotface')->first();
    $response = $this->actingAs($user)->get('/api/users/'.$user->slug);
    $response->assertJson(fn (AssertableJson $json) => $json
        ->where('id', $user->id)
        ->where('first_name', $user->first_name)
        ->where('last_name', $user->last_name)
        ->where('slug', $user->slug)
        ->where('email', $user->email)
        ->where('profile_photo_path', Storage::url($user->profile_photo_path))
        ->where('banner_path', Storage::url($user->banner_path))
        ->where('role_id', $user->role_id)
        ->etc()
    );
});

it('asserts that an admin user can see all the data of another user', function () {
    $admin = User::where('slug', 'botty-mcbotface')->first();
    $user = User::factory()->create();
    $response = $this->actingAs($admin)->get('/api/users/'.$user->slug);
    $response->assertJson(fn (AssertableJson $json) => $json
        ->where('id', $user->id)
        ->where('first_name', $user->first_name)
        ->where('last_name', $user->last_name)
        ->where('slug', $user->slug)
        ->where('email', $user->email)
        ->where('description', $user->description)
        ->where('profile_photo_path', Storage::url($user->profile_photo_path))
        ->where('banner_path', Storage::url($user->banner_path))
        ->where('role_id', $user->role_id)
        ->etc()
    );
});

it('returns paginated attended events of a single user', function () {
    $admin = User::where('slug', 'botty-mcbotface')->get()->first();
    $response = $this->actingAs($admin)->get('/api/users/botty-mcbotface/events-attended');

    $response->assertJson(fn (AssertableJson $json) => $json->has('data')->etc());
});

it('returns paginated organized events of a single user', function () {
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $response = $this->actingAs($admin)->get('/api/users/botty-mcbotface/events-organized');

    $response->assertJson(fn (AssertableJson $json) => $json->has('data')->etc());
});
