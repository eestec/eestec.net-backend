<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

uses(RefreshDatabase::class);

it('tests reset password link can be requested', function () {
    Notification::fake();

    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => $user->email,
    ]);

    Notification::assertSentTo($user, ResetPassword::class);
});

it('tests reset password link can not be requested with wrong email', function () {
    Notification::fake();

    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => 'test@test.com',
    ]);

    $response->assertInvalid(['email']);
});

it('tests password can be reset with valid token', function () {
    Notification::fake();

    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => $user->email,
    ]);

    Notification::assertSentTo($user, ResetPassword::class, function ($notification) use ($user) {
        $response = $this->post('auth/reset-password', [
            'token' => $notification->token,
            'email' => $user->email,
            'password' => 'EESTEC_password',
            'password_confirmation' => 'EESTEC_password',
        ]);

        $response->assertSessionHasNoErrors();

        return true;
    });
});

it('tests password can not be reset with invalid token', function () {
    Notification::fake();
    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => $user->email,
    ]);

    Notification::assertSentTo($user, ResetPassword::class, function ($notification) use ($user) {
        $response = $this->post('auth/reset-password', [
            'token' => sha1('wrong-token'),
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        // If the password reset token is wrong, the error is shown on the email.
        $response->assertInvalid(['email']);

        return true;
    });
});

it(/**
 * @throws Exception
 */ 'tests password can not be reset if confirmation does not match', function () {
    Notification::fake();

    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => $user->email,
    ]);

    Notification::assertSentTo($user, ResetPassword::class, function ($notification) use ($user) {
        $response = $this->post('auth/reset-password', [
            'token' => $notification->token,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password1',
        ]);

        $response->assertInvalid(['password']);

        return true;
    });
});

it('tests password can not be reset with different email', function () {
    Notification::fake();
    $user = User::factory()->create();

    $response = $this->post('auth/forgot-password', [
        'email' => $user->email,
    ]);

    Notification::assertSentTo($user, ResetPassword::class, function ($notification) {
        $response = $this->post('auth/reset-password', [
            'token' => $notification->token,
            'email' => 'test@test.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $response->assertInvalid(['email']);

        return true;
    });
});
