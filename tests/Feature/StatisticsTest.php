<?php

declare(strict_types=1);

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

uses(RefreshDatabase::class);

it('asserts the statistics endpoint is working', function () {
    $response = $this->get('api/statistics');

    $response->assertJsonStructure([
        'countries',
        'universities',
        'users',
    ]);
});

it('asserts that statistics command is working', function () {
    $this->artisan('eestecnet:update-statistics')
        ->assertExitCode(0);

    $this->assertTrue(Cache::has('branches'));
    $this->assertTrue(Cache::has('countries'));
    $this->assertTrue(Cache::has('users'));
});
