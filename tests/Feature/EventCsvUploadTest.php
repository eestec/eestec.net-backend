<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;

uses(RefreshDatabase::class);

it('asserts that a file with valid users does not return errors', function () {
    $user = User::factory()->create();
    $user_name = $user->first_name.' '.$user->last_name;
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $file = UploadedFile::fake()->createWithContent('test.csv', $user_name);

    $response = $this->actingAs($admin)->postJson('/api/events/test-event/csv', [
        'file' => $file,
    ]);
    $response->assertJson([$user_name => 'User added to event']);
});

it('asserts that a wrong user is not added to the event', function () {
    $user_name = 'Potato King';
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $file = UploadedFile::fake()->createWithContent('test.csv', $user_name);

    $response = $this->actingAs($admin)->postJson('/api/events/test-event/csv', [
        'file' => $file,
    ]);
    $response->assertJson([$user_name => 'User not found. Slug: potato-king']);
});

it('asserts that a user cannot be added to the event twice', function () {
    $user = User::factory()->create();
    $user_name = $user->first_name.' '.$user->last_name;
    $admin = User::firstWhere('slug', 'botty-mcbotface');
    $file = UploadedFile::fake()->createWithContent('test.csv', $user_name);

    $this->actingAs($admin)->postJson('/api/events/test-event/csv', [
        'file' => $file,
    ]);

    $response = $this->actingAs($admin)->postJson('/api/events/test-event/csv', [
        'file' => $file,
    ]);

    $response->assertJson([$user_name => 'The user already has this role in the event']);
});
