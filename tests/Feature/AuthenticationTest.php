<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('asserts users can login with a POST request', function () {
    $user = User::factory()->create();

    $response = $this->post('auth/login', [
        'email' => $user->email,
        'password' => 'password',
    ]);

    $this->assertAuthenticated();
});

it('asserts users can not authenticate with an invalid password', function () {
    $user = User::factory()->create();

    $this->post('auth/login', [
        'email' => $user->email,
        'password' => 'wrong-password',
    ]);

    $this->assertGuest();
});
