<?php

declare(strict_types=1);

use App\Models\Branch;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;

uses(RefreshDatabase::class);

it('asserts board members are returned on get request', function () {
    $response = $this->get('/api/branches/test-branch/board');

    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'branch_id',
                'user_id',
                'branch_role_id',
                'created_at',
                'updated_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('asserts members are not accessible by unauthorized users', function () {
    $response = $this->withHeaders([
        'Accept' => 'application/json',
    ])->get('/api/branches/test-branch/members');
    $response->assertStatus(401);
});

it('asserts members are returned on get request', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $response = $this->actingAs($user)->get('/api/branches/test-branch/members');
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'branch_id',
                'user_id',
                'branch_role_id',
                'created_at',
                'updated_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('asserts active members are returned on get request', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $response = $this->actingAs($user)->get('/api/branches/test-branch/members?kind=active');
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'branch_id',
                'user_id',
                'branch_role_id',
                'created_at',
                'updated_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('asserts alumni members are returned on get request', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $response = $this->actingAs($user)->get('/api/branches/test-branch/members?kind=alumni');
    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'branch_id',
                'user_id',
                'branch_role_id',
                'created_at',
                'updated_at',
                'user' => [
                    'first_name',
                    'last_name',
                    'slug',
                    'profile_photo_path',
                ],
            ],
        ],
    ]);
});

it('returns an array of events in two separate categories for the specific branch', function () {
    $response = $this->get('/api/branches/test-branch/events');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->has('links')
        ->has('meta')
        ->etc());
});

it('asserts that a global admin can change branch info', function () {
    $user = User::firstWhere('slug', 'botty-mcbotface');
    $branch = Branch::firstWhere('slug', 'test-jlc');

    $response = $this->actingAs($user)->put('api/branches/'.$branch->slug, [
        'region' => '8',
        'country' => 'Swaziland',
        'description' => 'Test!',
    ]);

    $response->assertOk();
    $response->assertSessionHasNoErrors();
});

it('asserts that a branch admin cannot change all branch info', function () {
    $user = User::firstWhere('slug', 'lc-admin');
    $branch = Branch::firstWhere('slug', 'test-lc');

    $response = $this->actingAs($user)->put('api/branches/'.$branch->slug, [
        'region' => '8',
        'country' => 'Swaziland',
        'description' => 'Test!',
    ]);

    $response->assertStatus(403);
});

it('returns an array of events when requesting the active ones', function () {
    $response = $this->get('/api/branches/test-branch/events?kind=active');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});

it('returns an array of events when requesting the past ones', function () {
    $response = $this->get('/api/branches/test-branch/events?kind=past');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});

it('returns all events of branch', function () {
    $response = $this->get('/api/branches/test-branch/events');

    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('data')
        ->etc());
});

it('asserts correct branch structure is returned, if exists', function () {
    $response = $this->get('/api/branches/test-branch');
    $response->assertStatus(200);
    $response->assertJson(fn (AssertableJson $json) => $json
        ->has('can_edit')
        ->has('branch')
        ->first(fn (AssertableJson $json) => $json->has('id')
            ->has('name')
            ->has('slug')
            ->has('email')
            ->has('founded_in')
            ->has('website')
            ->has('country')
            ->has('region')
            ->has('facebook')
            ->has('instagram')
            ->has('linkedin')
            ->has('address')
            ->has('member_no')
            ->has('description')
            ->has('profile_picture_path')
            ->has('large_profile_picture_path')
            ->has('branch_type')
        )
    );
});

it('asserts 404 is returned when branch doesn\'t exist', function () {
    $response = $this->get('/api/branches/test-test');
    $response->assertStatus(404);
});
