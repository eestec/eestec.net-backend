<?php

declare(strict_types=1);

use App\Mail\ContactMail;
use App\Models\Contact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

uses(RefreshDatabase::class);

it('asserts that the contacts JSON has correct form', function () {
    $response = $this->get('/api/contacts');

    $response->assertJsonStructure([
        'data' => [
            '*' => [
                'slug',
                'text',
                'link',
                'to-text',
            ],
        ],
    ]);
});

it('asserts that sending mail to a contact works', function () {
    Mail::fake();

    $contact = Contact::first();
    $response = $this->post('/api/contacts/'.$contact->slug, [
        'content' => 'Test',
        'sender' => 'admin@eestec.net',
    ]);

    Mail::assertQueued(ContactMail::class);
});
